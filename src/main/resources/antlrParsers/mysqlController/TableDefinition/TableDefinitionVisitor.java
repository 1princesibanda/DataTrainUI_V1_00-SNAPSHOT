// Generated from ..\..\prince\ETL_Wizard\V1_00\DatatrainUI\src\main\resources\antlrGrammars\mysqlController\TableDefinition.g4 by ANTLR 4.7.2
import org.antlr.v4.runtime.tree.ParseTreeVisitor;

/**
 * This interface defines a complete generic visitor for a parse tree produced
 * by {@link TableDefinitionParser}.
 *
 * @param <T> The return type of the visit operation. Use {@link Void} for
 * operations with no return type.
 */
public interface TableDefinitionVisitor<T> extends ParseTreeVisitor<T> {
	/**
	 * Visit a parse tree produced by {@link TableDefinitionParser#prog}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitProg(TableDefinitionParser.ProgContext ctx);
	/**
	 * Visit a parse tree produced by {@link TableDefinitionParser#tableDefinition}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitTableDefinition(TableDefinitionParser.TableDefinitionContext ctx);
	/**
	 * Visit a parse tree produced by {@link TableDefinitionParser#columnNameList}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitColumnNameList(TableDefinitionParser.ColumnNameListContext ctx);
	/**
	 * Visit a parse tree produced by {@link TableDefinitionParser#columnDefinition}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitColumnDefinition(TableDefinitionParser.ColumnDefinitionContext ctx);
	/**
	 * Visit a parse tree produced by {@link TableDefinitionParser#columnDefinitionList}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitColumnDefinitionList(TableDefinitionParser.ColumnDefinitionListContext ctx);
	/**
	 * Visit a parse tree produced by {@link TableDefinitionParser#sqlDataType}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitSqlDataType(TableDefinitionParser.SqlDataTypeContext ctx);
	/**
	 * Visit a parse tree produced by {@link TableDefinitionParser#directType}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitDirectType(TableDefinitionParser.DirectTypeContext ctx);
	/**
	 * Visit a parse tree produced by {@link TableDefinitionParser#precisionType}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitPrecisionType(TableDefinitionParser.PrecisionTypeContext ctx);
	/**
	 * Visit a parse tree produced by {@link TableDefinitionParser#tableName}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitTableName(TableDefinitionParser.TableNameContext ctx);
}