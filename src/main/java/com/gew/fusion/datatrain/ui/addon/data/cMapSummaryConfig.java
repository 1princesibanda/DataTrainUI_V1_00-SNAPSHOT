/*
 * Copyright (c) 2019 GEW Technologies (GEW). All rights reserved.
 */
package com.gew.fusion.datatrain.ui.addon.data;

import com.gew.ifs.ext.datatrain.core.transformation.cMap;
import com.gew.ifs.ext.datatrain.lib.config.cMapConfig;
import com.gew.persistence.IPersistenceStore;
import com.gew.persistence.ITransaction;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.w3c.dom.Element;

/**
 * This will contain the config for the map
 * @author 0400553
 */
public class cMapSummaryConfig
{
  private final String mapName ;
  private String fieldType;
  private final cMap map;
  
  private static final String SELECT_COUNT= "select count(*) as count ";
  private String sourceCountQuery;
  private final cMapConfig mapConfigHandler;

  /**
   * Config the summary item requires to calculate its values
   * 
   * @param p_mapName The map name
   * @param map The map as per the config
   * @param mapConfigHandler Config handler the load the config from the map.
   */
  public cMapSummaryConfig(String p_mapName, cMap map, cMapConfig mapConfigHandler)
  {
    this.mapName = p_mapName;
    this.map = map;
    this.mapConfigHandler = mapConfigHandler;
    
    try
    {
      this.initSourceQuery();
    }
    catch (Exception ex)
    {
      Logger.getLogger(cMapSummaryConfig.class.getName()).log(Level.SEVERE, null, ex);
    }
  }
  
  /**
   * This will create a map item will not recalculate from the ETL MAP.
   * @param p_mapName 
   */
  public cMapSummaryConfig(String p_mapName)
  {
    this(p_mapName, null, null);
  }
  
  
  /**
   * Generate the select query. This is specific to oracle.
   * TODO - Needs to be abstracted for other data sources.
   */
  private void initSourceQuery()
  {
    //Build the select query;
    Element oSqlElement = mapConfigHandler.getChildElement(map.getInputElement(), "Sql");
    //this is to ensure we use the original case sensitive query,
    final String sqlQuery = oSqlElement.getTextContent().trim();
    final String sqlQueryLowerCase = sqlQuery.toLowerCase();
    
    int typePos = sqlQueryLowerCase.indexOf("as type");
    fieldType = sqlQuery.substring(0,typePos);
    fieldType = fieldType.substring(fieldType.lastIndexOf(",")+1).replace("'", "").trim();
    
    
    int whereIndex = sqlQueryLowerCase.lastIndexOf("where");
    //Remove the dateTime limits from the where clause
    String whereClause = sqlQuery.substring(whereIndex);
    
    whereClause = whereClause.substring(0, whereClause.indexOf("((")).trim();
    if(whereClause.toLowerCase().endsWith("and"))
      whereClause =whereClause.substring(0,whereClause.toLowerCase().lastIndexOf("and"));
    //This means there there is no valid where clause
    else
      whereClause="";
    
    String fromClause = sqlQuery.substring(sqlQueryLowerCase.indexOf("from"), whereIndex);
    
    sourceCountQuery = SELECT_COUNT + " "+  fromClause + " " + whereClause;
  }

  /**
   * @return The map name
   */
  public String getMapName()
  {
    return mapName;
  }
  
  /**
   * Load the count from the source db
   * 
   * @param sourceConnection The source db connection
   * @return The count
   */
  public long loadSourceCount(Connection sourceConnection) throws Exception
  {
    if(sourceConnection==null)
      return 0;

    long count=0;
    PreparedStatement statement=null;
    try
    {
      statement = sourceConnection.prepareStatement(sourceCountQuery);
      ResultSet resultSet = statement.executeQuery();
      if(resultSet!=null)
      {
        while(resultSet.next())
          count = resultSet.getLong("count");
        resultSet.close();
      }
      
    }
    finally
    {
      if(statement!=null)
      try 
      {
          statement.close();
      }
      catch (SQLException ex) {
        Logger.getLogger(cMapSummaryConfig.class.getName()).log(Level.SEVERE, null, ex);
      }
    }
    return count;
  }
  
  /**
   * Load the count from the destination DB
   * @param destConn The destination DB connection
   * @return The count
   */
  public long loadDestinationCount(IPersistenceStore destConn) throws Exception
  {
    long count = 0;
    try (ITransaction tx = destConn.getTransaction())
    {
      count = destConn.getContainerModelManager().loadCountForType(tx,fieldType);
    }
    return count;
  }
  
  
}
