/**
 * Copyright (c) 2019 GEW Technologies (GEW). All rights reserved.
 */
package com.gew.fusion.datatrain.ui.addon.view.importing;

import com.gew.fusion.datatrain.ui.addon.cFXDataTrainUIManager;
import com.gew.fusion.datatrain.ui.addon.cFXDataTrainUIManager.EViews;
import com.gew.fusion.datatrain.ui.addon.view.cAbstractView;
import com.gew.lib.ui.fx.securefilechooser.cFXSecureFileChooser;
import java.io.File;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Tooltip;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.stage.FileChooser;

/**
 *
 * @author Philip M. Trenwith
 */
public class cImportControls extends cAbstractView
{
  private Button m_oButtonImportFusionData;
  private Button m_oButtonImportExternalData;
  private final cFXSecureFileChooser m_oFXSecureFileChooser;
    
  public cImportControls(cFXDataTrainUIManager oManager, int iDefaultWidth, int iDefaultHeight)
  {
    super(oManager, iDefaultWidth, iDefaultHeight);
    m_oFXSecureFileChooser = new cFXSecureFileChooser(m_oDataTrainUIManager.getConfig().getFileChooserConfig());
  }


  @Override
  public void reload()
  {
    if (m_oResourceBundle != null)
    {
      m_oButtonImportFusionData.setText(m_oResourceBundle.getString("button_import_fusion_data"));
      m_oButtonImportFusionData.setTooltip(new Tooltip(m_oResourceBundle.getString("button_import_fusion_data_tooltip")));
      
      m_oButtonImportExternalData.setText(m_oResourceBundle.getString("button_import_external_data"));
      m_oButtonImportExternalData.setTooltip(new Tooltip(m_oResourceBundle.getString("button_import_external_data_tooltip")));
    }
  }

  @Override
  protected BorderPane setupPane(Scene oScene)
  {
    m_oParent.prefHeightProperty().bind(oScene.heightProperty());
    m_oParent.prefWidthProperty().bind(oScene.widthProperty());
       
    m_oButtonImportFusionData = new Button("Import Fusion Data");
    m_oButtonImportFusionData.setTooltip(new Tooltip("Import fusion data exported by the DataTrain"));
    m_oButtonImportFusionData.setOnAction((event) -> 
    {
      // Choose file to import. 
      Thread oFileLoadingThread = new Thread(() -> 
      {
        m_oFXSecureFileChooser.getExtensionFilters().clear();
        m_oFXSecureFileChooser.getExtensionFilters().add(new FileChooser.ExtensionFilter("Fusion Data", "*.fd"));
        File oSelectedFile = m_oFXSecureFileChooser.showOpenDialog(null);
        if (oSelectedFile != null)
        {
          m_oDataTrainUIManager.deserializeFusionDataFromFile(oSelectedFile);
        }
      });
      oFileLoadingThread.setName("DataTrainUI-m_oButtonImportFusionData-Thread");
      oFileLoadingThread.setDaemon(true);
      oFileLoadingThread.start();
    });
    
    m_oButtonImportExternalData = new Button("Import External Documents");
    m_oButtonImportExternalData.setTooltip(new Tooltip("Import various documents from external sources"));
    m_oButtonImportExternalData.setOnAction((event) -> 
    {
      // Choose file to import. 
      Thread oFileLoadingThread = new Thread(() -> 
      {
        m_oFXSecureFileChooser.getExtensionFilters().clear();
        File oSelectedFile = m_oFXSecureFileChooser.showOpenDialog(null);
        if (oSelectedFile != null)
        {
          m_oDataTrainUIManager.ingestExternalFile(oSelectedFile);
        }
      });
      oFileLoadingThread.setName("DataTrainUI-m_oButtonImportExternalData-Thread");
      oFileLoadingThread.setDaemon(true);
      oFileLoadingThread.start();
    });
    
    HBox oCenterPane = new HBox(20);
    oCenterPane.setPadding(new Insets(5));
    oCenterPane.getChildren().addAll(m_oButtonImportFusionData, m_oButtonImportExternalData);
    oCenterPane.setAlignment(Pos.TOP_LEFT);
    
    m_oParent.setCenter(oCenterPane);
    
    return m_oParent;
  }

  @Override
  public Node getNode()
  {
    return m_oParent;
  }

  @Override
  public String getViewName()
  {
    return EViews.IMPORT_VIEW.niceName();
  }
  
}
