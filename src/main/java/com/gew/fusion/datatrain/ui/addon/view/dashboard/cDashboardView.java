/**
 * Copyright (c) 2019 GEW Technologies (GEW). All rights reserved.
 */
package com.gew.fusion.datatrain.ui.addon.view.dashboard;

import com.gew.fusion.datatrain.ui.addon.cFXDataTrainUIManager;
import com.gew.fusion.datatrain.ui.addon.cFXDataTrainUIManager.EViews;
import com.gew.fusion.datatrain.ui.addon.view.cAbstractView;
import static com.gew.fusion.datatrain.ui.addon.view.cAbstractView.g_oSIMPLE_DATE;
import com.gew.fusion.datatrain.ui.addon.view.dashboard.charts.cFXChart;
import com.gew.ifs.ext.datatrain.core.transformation.feedback.cPostFeedbackToObservers;
import com.gew.ifs.ext.datatrain.core.transformation.feedback.cSyncProgress1;
import com.gew.ifs.ext.datatrain.lib.chats.cChartMetadata;
import com.gew.ifs.ext.datatrain.lib.statistics.cNumberOfStatistics;
import com.gew.ifs.ext.datatrain.lib.statistics.cStatisticsWrapper;
import com.gew.persistence.datatypes.IContainer;
import com.gew.ui.util.fxwidgets.control.IWidgetControl;
import com.gew.ui.util.fxwidgets.control.cWidgetFactory;
import com.gew.util.cThrowable;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Observable;
import java.util.Observer;
import java.util.Set;
import java.util.TreeMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadFactory;
import javafx.application.Platform;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.SnapshotParameters;
import javafx.scene.chart.Chart;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.Label;
import javafx.scene.control.MenuItem;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.control.TableColumn;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.transform.Transform;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Philip M. Trenwith
 */
public class cDashboardView extends cAbstractView implements Observer
{

  private static final Logger g_oLog = LoggerFactory.getLogger(cDashboardView.class);
  private static final int m_iCOLUMNS = 3;

  private Label m_oLabelNoContentPlaceholder;
  //private TableView<IStatisticsWrapper> m_oTableStatistics;
  private TableColumn m_oColPropertyName;
  private TableColumn m_oColPropertyType;
  private TableColumn m_oColPropertyCount;

  private HashMap<Object, cStatisticsWrapper> m_oProperties = new HashMap();
  private Tab m_oPropertiesTab;
  private Tab m_oSummaryTab;
  private TabPane m_oChartsTabPane;
  private HashMap<String, Tab> m_oChartsTabsMap;
  private VBox m_oSummaryPaneContent;
  private final ExecutorService m_oChartsExecutorService;
  private final String m_sVOLUME = "Volume";
  private final String m_sFREQUENCY = "Frequency";
  private final String m_sBANDWIDTH = "Bandwidth";
  private final String m_sMODULATION = "Modulation";
  private final String m_sPROTOCOL = "Protocol";
  private final String m_sAZIMUTH = "Azimuth";
  private final String m_sELEVATION = "Elevation";
  private final String m_sDETECTION_STARTTIME = "Detection Start Time";
  private final String m_sDETECTION_STOPTIME = "Detection Stop Time";
  private final String m_sSOURCE_DESTINATION_COUNT = "Source Destination Count Overview";

  private Label m_oLabelFrom;
  private Label m_oLabelTo;
  private Button m_oButtonGenerateStatistics;
  private IWidgetControl<?> m_oDateTimeControlFrom;
  private IWidgetControl<?> m_oDateTimeControlTo;
  private LocalDateTime m_oDateTimeFrom;
  private LocalDateTime m_oDateTimeTo;
  private CheckBox m_oCheckBoxPreviousDay;

  private Set<IContainer> m_oFusionDataSet;
  private long m_lStartTimestamp = 0;
  private long m_lStopTimestamp = 0;

  private ContextMenu m_oContextMenu;
  private MenuItem m_oMenuItemScreenCapture;
  private MenuItem m_oMenuItemSendToReport;
  /**
   * When the ContextMenu shows this is the pane that was clicked.
   */
  private volatile BorderPane m_oCurrentPane;
  private cSourceDestinationViewManager sourceDestinationController;

  public cDashboardView(cFXDataTrainUIManager oManager, int iDefaultWidth, int iDefaultHeight)
  {
    super(oManager, iDefaultWidth, iDefaultHeight);

    ThreadFactory tFactory = (Runnable runnable) ->
    {
      Thread thread = new Thread(runnable);
      thread.setName("Charts-ExecutorService-Thread");
      thread.setDaemon(true);
      thread.setPriority(3);
      return thread;
    };
    m_oChartsExecutorService = Executors.newFixedThreadPool(4, tFactory);

    cPostFeedbackToObservers.instance().addObserver(this);
  }

  @Override
  public void reload()
  {
    if (m_oResourceBundle != null)
    {
      m_oColPropertyName.setText(m_oResourceBundle.getString("label_column_name"));
      m_oColPropertyType.setText(m_oResourceBundle.getString("label_column_type"));
      m_oColPropertyCount.setText(m_oResourceBundle.getString("label_column_count"));
      m_oButtonGenerateStatistics.setText(m_oResourceBundle.getString("button_generate_statistics"));
      m_oLabelNoContentPlaceholder.setText(m_oResourceBundle.getString("table_no_content_placeholder"));
      m_oPropertiesTab.setText(m_oResourceBundle.getString("tab_properties"));
      m_oSummaryTab.setText(m_oResourceBundle.getString("tab_summary"));
      m_oLabelFrom.setText(m_oResourceBundle.getString("label_from_export_view"));
      m_oLabelTo.setText(m_oResourceBundle.getString("label_to_export_view"));
      m_oCheckBoxPreviousDay.setText(m_oResourceBundle.getString("label_previous_day"));
      m_oMenuItemScreenCapture.setText(m_oResourceBundle.getString("screen_capture"));
      m_oMenuItemSendToReport.setText(m_oResourceBundle.getString("send_to_report"));
    }
  }

  @Override
  protected BorderPane setupPane(Scene oScene)
  {
    m_oParent.prefHeightProperty().bind(oScene.heightProperty());
    m_oParent.prefWidthProperty().bind(oScene.widthProperty());

    m_oLabelFrom = new Label("From");
    m_oLabelTo = new Label("To");

    Node oDateTimeControlFrom = null;
    Node oDateTimeControlTo = null;
    try
    {
      cWidgetFactory oFXWidgetFactory = cWidgetFactory.getInstance();
      m_oDateTimeControlFrom = oFXWidgetFactory.getControlFor("Start Date", LocalDateTime.class);
      m_oDateTimeControlTo = oFXWidgetFactory.getControlFor("End Date", LocalDateTime.class);
      m_oDateTimeControlFrom.reset();
      m_oDateTimeControlTo.reset();

      oDateTimeControlFrom = m_oDateTimeControlFrom.getWidget();
      oDateTimeControlTo = m_oDateTimeControlTo.getWidget();
    }
    catch (Exception ex)
    {
      g_oLog.error("Exception: " + ex.getMessage());
      g_oLog.error(cThrowable.getStackTrace(ex));
    }

    m_oCheckBoxPreviousDay = new CheckBox("Previous Day");
    m_oCheckBoxPreviousDay.setOnAction((oEvent) ->
    {
      if (m_oCheckBoxPreviousDay.isSelected())
      {
        // Use the Calendar class to subtract one day
        Date oNow = new Date(System.currentTimeMillis());
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(oNow);
        calendar.add(Calendar.DAY_OF_YEAR, -1);
        calendar.add(Calendar.HOUR, -1 * oNow.getHours());
        calendar.add(Calendar.MINUTE, -1 * oNow.getMinutes());
        calendar.add(Calendar.SECOND, -1 * oNow.getSeconds());
        Date oStartOfPreviousDay = calendar.getTime();
        calendar.add(Calendar.HOUR, 24);
        Date oEndOfPreviousDay = calendar.getTime();

        m_lStartTimestamp = oStartOfPreviousDay.getTime();
        m_lStopTimestamp = oEndOfPreviousDay.getTime();

        m_oDateTimeControlFrom.setDisabled(true);
        m_oDateTimeControlTo.setDisabled(true);
      }
      else
      {
        m_oDateTimeControlFrom.setDisabled(false);
        m_oDateTimeControlTo.setDisabled(false);
      }
    });

    // Generate Stats button.
    m_oButtonGenerateStatistics = new Button("Generate Statistics", m_oIconLoader.getIcon("refresh.png", 16));
    m_oButtonGenerateStatistics.setOnAction((oEvent) ->
    {
      //m_oTableStatistics.getItems().clear();
      m_oButtonGenerateStatistics.setDisable(true);
      new Thread(() ->
      {
        m_oDateTimeFrom = (LocalDateTime) m_oDateTimeControlFrom.getWidgetValueProperty().getValue();
        m_oDateTimeTo = (LocalDateTime) m_oDateTimeControlTo.getWidgetValueProperty().getValue();
        boolean bPreviousDay = m_oCheckBoxPreviousDay.isSelected();
        boolean bTimesEntered = (m_oDateTimeFrom != null && m_oDateTimeTo != null);
        if (bTimesEntered)
        {
          String fromTime = g_oSIMPLE_DATE.format(m_oDateTimeFrom.toEpochSecond(ZoneOffset.UTC) * 1000);
          String toTime = g_oSIMPLE_DATE.format(m_oDateTimeTo.toEpochSecond(ZoneOffset.UTC) * 1000);

          m_lStartTimestamp = m_oDateTimeFrom.toEpochSecond(ZoneOffset.UTC) * 1000;
          m_lStopTimestamp = (m_oDateTimeTo.toEpochSecond(ZoneOffset.UTC) * 1000) + 999;
        }

        if (bPreviousDay || bTimesEntered)
        {
          m_oFusionDataSet = m_oDataTrainUIManager.loadContainers(m_lStartTimestamp, m_lStopTimestamp);
          m_oFusionDataSet.stream().forEach((container) ->
          {
            Set<String> oPropertyKeys = container.getAdditionalPropertyKeys();
            oPropertyKeys.stream().forEach((key) ->
            {
              Object oProperty = container.getAdditionalProperty(key);
              Class<?> aClass = oProperty.getClass();
              String sClassName = aClass.getName();

              addProperty(key, sClassName, oProperty, 1);
            });
          });
          m_oButtonGenerateStatistics.setDisable(false);

          buildCharts();
        }
        else
        {
          Platform.runLater(() ->
          {
            Alert alert = new Alert(Alert.AlertType.ERROR, "Specify both start and end times.", ButtonType.OK);
            alert.setTitle("Error");
            alert.showAndWait();
            m_oButtonGenerateStatistics.setDisable(false);
          });
        }
      }).start();
    });

    HBox oPaneButtons = new HBox(5.0d);
    oPaneButtons.setAlignment(Pos.CENTER_LEFT);
    oPaneButtons.setPadding(new Insets(5.0d));
    oPaneButtons.getChildren().addAll(m_oLabelFrom, oDateTimeControlFrom, m_oLabelTo, oDateTimeControlTo,
            m_oCheckBoxPreviousDay, m_oButtonGenerateStatistics);
    m_oParent.setTop(oPaneButtons);

    VBox oPropertiesPaneContent = new VBox(5.0d);

    m_oLabelNoContentPlaceholder = new Label("No Content");

//    m_oTableStatistics = new TableView<>();
//    m_oTableStatistics.prefHeightProperty().bind(oPropertiesPaneContent.heightProperty());
//    m_oTableStatistics.prefWidthProperty().bind(oPropertiesPaneContent.widthProperty());
//    m_oTableStatistics.setEditable(false);
//    m_oTableStatistics.setPlaceholder(m_oLabelNoContentPlaceholder);
//    
//    m_oColPropertyName = new TableColumn("Name");
//    m_oColPropertyName.setCellValueFactory(cTableCellFactory.propertyNameValueFactory());
//    m_oColPropertyName.setCellFactory((o) -> cTableCellFactory.stringCellFactory());
//    m_oColPropertyName.setComparator(cUtil.g_oSTRING_COMPARATOR);
//    m_oColPropertyName.prefWidthProperty().bind(m_oTableStatistics.widthProperty().divide(m_iCOLUMNS));
//
//    m_oColPropertyType = new TableColumn("Type");
//    m_oColPropertyType.setCellValueFactory(cTableCellFactory.propertyTypeValueFactory());
//    m_oColPropertyType.setCellFactory((o) -> cTableCellFactory.stringCellFactory());
//    m_oColPropertyType.setComparator(cUtil.g_oSTRING_COMPARATOR);
//    m_oColPropertyType.prefWidthProperty().bind(m_oTableStatistics.widthProperty().divide(m_iCOLUMNS));
//
//    m_oColPropertyCount = new TableColumn("Count");
//    m_oColPropertyCount.setCellValueFactory(cTableCellFactory.propertyCountValueFactory());
//    m_oColPropertyCount.setCellFactory((o) -> cTableCellFactory.propertyCountCellFactory());
//    m_oColPropertyCount.setComparator(cUtil.g_oLONG_COMPARATOR);
//    m_oColPropertyCount.prefWidthProperty().bind(m_oTableStatistics.widthProperty().divide(m_iCOLUMNS));
//    
//    m_oTableStatistics.getColumns().addAll(m_oColPropertyName, m_oColPropertyType, m_oColPropertyCount);
//    m_oTableStatistics.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
//    m_oTableStatistics.getSortOrder().addAll(m_oColPropertyName, m_oColPropertyType, m_oColPropertyCount);
//    
//    
//    oPropertiesPaneContent.getChildren().addAll(m_oTableStatistics);
//
//    ScrollPane oPropertiesPane = new ScrollPane(oPropertiesPaneContent);
//    oPropertiesPane.prefHeightProperty().bind(oScene.heightProperty());
//    oPropertiesPane.prefWidthProperty().bind(oScene.widthProperty());
//    oPropertiesPaneContent.prefHeightProperty().bind(oPropertiesPane.heightProperty().subtract(5.0d));
//    oPropertiesPaneContent.prefWidthProperty().bind(oPropertiesPane.widthProperty().subtract(5.0d));
//
//    m_oPropertiesTab = new Tab("Properties");
//    m_oPropertiesTab.setClosable(false);
//    m_oPropertiesTab.setContent(oPropertiesPane);
//    oTabPane.getTabs().add(m_oPropertiesTab);
    TabPane oTabPane = new TabPane();

    m_oSummaryPaneContent = new VBox(5.0d);
    ScrollPane oChartsPane = new ScrollPane(m_oSummaryPaneContent);
    oChartsPane.prefHeightProperty().bind(oScene.heightProperty());
    oChartsPane.prefWidthProperty().bind(oScene.widthProperty());
    m_oSummaryPaneContent.prefHeightProperty().bind(oChartsPane.heightProperty().subtract(5.0d));
    m_oSummaryPaneContent.prefWidthProperty().bind(oChartsPane.widthProperty().subtract(5.0d));

    m_oSummaryTab = new Tab("Summary");
    m_oSummaryTab.setClosable(false);
    m_oSummaryTab.setContent(m_oSummaryPaneContent);
    oTabPane.getTabs().add(m_oSummaryTab);

    initSummaryTabs();
    m_oParent.setCenter(oTabPane);

    createContextMenu();
    initSourceDestinationView();
    return m_oParent;
  }
  
  
  private void initSourceDestinationView()
  {
    sourceDestinationController = new cSourceDestinationViewManager(m_oDataTrainUIManager);
    m_oChartsTabsMap.get(m_sSOURCE_DESTINATION_COUNT).setContent(sourceDestinationController.getView().getNode());
  }
  
  private void createContextMenu()
  {
    // create a menu 
    m_oContextMenu = new ContextMenu();

    // create menuitems 
    m_oMenuItemScreenCapture = new MenuItem("Screen Capture");
    m_oMenuItemSendToReport = new MenuItem("Send To Report");

    // create action handlers
    m_oMenuItemScreenCapture.setOnAction((event) ->
    {
      m_oDataTrainUIManager.getScreenCapture().capture(m_oCurrentPane);
    });
    m_oMenuItemSendToReport.setOnAction((event) ->
    {
      m_oDataTrainUIManager.getScreenCapture().capture(m_oCurrentPane);
    });

    // add menu items to menu 
    //m_oContextMenu.getItems().add(m_oMenuItemScreenCapture);
    m_oContextMenu.getItems().add(m_oMenuItemSendToReport);
    
  }

  private void initSummaryTabs()
  {
    m_oChartsTabsMap = new HashMap();
    m_oChartsTabPane = new TabPane();

    //TODO: Charts from config.
    createTabForChart(m_sSOURCE_DESTINATION_COUNT);
    createTabForChart(m_sVOLUME);
    createTabForChart(m_sFREQUENCY);
    createTabForChart(m_sBANDWIDTH);
    createTabForChart(m_sMODULATION);
    createTabForChart(m_sPROTOCOL);
    createTabForChart(m_sAZIMUTH);
    createTabForChart(m_sELEVATION);
    createTabForChart(m_sDETECTION_STARTTIME);
    createTabForChart(m_sDETECTION_STOPTIME);
    

    m_oSummaryPaneContent.getChildren().addAll(m_oChartsTabPane);
  }

  public void buildCharts()
  {
    buildVolumeChart("Time", "Volume", m_sVOLUME);

    buildPropertyChart("centre_frequency_hz", "Frequency Map", "Occurrences", "Frequencies", m_sFREQUENCY, true);
    buildPropertyChart("bandwidth_hz", "Bandwidth Map", "Occurrences", "Bandwidth", m_sBANDWIDTH, true);
    buildPropertyChart("modulation", "Modulation Map", "Occurrences", "Modulation", m_sMODULATION);
    buildPropertyChart("protocol", "Protocol Map", "Occurrences", "Protocols", m_sPROTOCOL);
    buildPropertyChart("azimuth_deg", "Azimuth Map", "Occurrences", "Azimuth", m_sAZIMUTH);
    buildPropertyChart("elevation_deg", "Elevation Map", "Occurrences", "Elevation", m_sELEVATION);
    buildPropertyChart("detection_start_timestamp_ms", "Start Map", "Occurrences", "Start Time", m_sDETECTION_STARTTIME, true);
    buildPropertyChart("detection_stop_timestamp_ms", "Start Map", "Occurrences", "Stop Time", m_sDETECTION_STOPTIME, true);
  }

  public void buildPropertyChart(String sPropertyName, String sChartLabel, String sXAxisLabel, String sYAxisLabel, String sTabName)
  {
    buildPropertyChart(sPropertyName, sChartLabel, sXAxisLabel, sYAxisLabel, sTabName, false);
  }
  
  /**
   * 
   * @param sPropertyName
   * @param sChartLabel
   * @param sXAxisLabel
   * @param sYAxisLabel
   * @param sTabName
   * @param bGeneralize - too many data points in the graph makes it too slow to draw, so reduce the data points to a max 100
   */
  public void buildPropertyChart(String sPropertyName, String sChartLabel, String sXAxisLabel, String sYAxisLabel, String sTabName, 
          boolean bGeneralize)
  {
    m_oChartsExecutorService.execute(() ->
    {
      try
      {
        //DecimalFormat oDecimal_Format_Hz = new DecimalFormat("###,###,###,### Hz");
        cFXChart oFXChart = new cFXChart();
        cNumberOfStatistics oStats = new cNumberOfStatistics();
        cStatisticsWrapper oStatsWrapper = m_oProperties.get(sPropertyName);
        final cChartMetadata oChartMetadata = new cChartMetadata(sChartLabel);
        if (oStatsWrapper != null)
        {
          HashMap<Object, Long> oValues = oStatsWrapper.getValues();
          TreeMap<Object, Long> oSortedValues = new TreeMap();
          try
          {
            oSortedValues = new TreeMap(oValues);
          }
          catch (Exception ex)
          {
            g_oLog.warn("Warning: Have to iterate values manually for property: '" + sPropertyName + "' because: " + ex.getMessage());
            Iterator<Object> oIterator = oValues.keySet().iterator();
            if (oIterator != null)
            {
              while (oIterator.hasNext())
              {
                Object key = oIterator.next();
                Long oLong = oValues.get(key);

                if (ex.getMessage().contains("java.lang.String cannot be cast to java.lang.Double"))
                {
                  double dDouble = Double.parseDouble(key + "");
                  dDouble = (double) Math.round(dDouble * 1000d) / 1000d;
                  oSortedValues.put(dDouble, oLong);
                }
                else if (ex.getMessage().contains("java.lang.Double cannot be cast to java.lang.String"))
                {
                  double dDouble = Double.parseDouble(key + "");
                  dDouble = (double) Math.round(dDouble * 1000d) / 1000d;
                  oSortedValues.put(dDouble, oLong);
                }
                else
                {
                  oSortedValues.put(key + "", oLong);
                }
              }
            }
          }
          Long lBiggestItem = oSortedValues.get(oSortedValues.descendingKeySet().first());
          
          //TODO fix this after SAGAR
          if (false)//(bGeneralize)
          {
            try
            {
              // move the items contained in the tree to other points to only have 100 point not more
              Set<Object> oKeys = oValues.keySet();
              Iterator<Object> oIterator = oKeys.iterator();
              Object nextObj = oIterator.next();
              // only move the points if thex-axis is Long or Int values
              boolean bIterate = ((nextObj instanceof Long) || (nextObj instanceof Integer));
              TreeMap<Object, Long> oTempSortedValues = new TreeMap();
              
              if (bIterate)
              {
                int iDesiredXAxisItems = 100;
                // get the biggest item on the x-axis
                long iHighestXAxisItem = lBiggestItem;
                // get the distance between the x-axis items
                double dSpanBetweenItems = iHighestXAxisItem/iDesiredXAxisItems;
                do 
                {
                  long lDataPoint = (Long) nextObj;
                  long lItemsAtDataPoint = oValues.get(lDataPoint);
                  // take the actual data point and devide it by the distance to get an adjusted point.
                  long lAjustedValue = Long.parseLong((lDataPoint/dSpanBetweenItems)+"");
                  // get the other items already at the adjusted point and add the new ones.
                  Long lOtherPoints = oTempSortedValues.get(lAjustedValue);
                  if (lOtherPoints != null)
                  {
                    lItemsAtDataPoint += lOtherPoints;
                  }
                  oTempSortedValues.put(lAjustedValue, lItemsAtDataPoint);
                }
                while (oIterator.hasNext());
              }
              oSortedValues = oTempSortedValues;
            }
            catch (Exception ex)
            {
              g_oLog.error("Error generalizing data points: " + ex.getMessage(), ex); 
            }
          }

          final TreeMap<Object, Long> oFinalSortedValues = oSortedValues;
          oStats.setObjects(oFinalSortedValues);

          // determine lowest and higest value
          oFinalSortedValues.keySet().forEach((key) ->
          {
            oChartMetadata.setXAxisLastItem(key);
            if (oChartMetadata.getXAxisFirstItem() == null)
            {
              oChartMetadata.setXAxisFirstItem(key);
            }

            Long lValue = oFinalSortedValues.get(key);
            if (lValue > oStats.getHighestValue())
            {
              oStats.setHighestValue(lValue);
            }
          });

          oChartMetadata.setXAxisLabel(sXAxisLabel);
          oChartMetadata.setYAxisLabel(sYAxisLabel);
          oChartMetadata.setXAxisRangeLabel("x-axis range: " + oChartMetadata.getXAxisFirstItem() + " - " + oChartMetadata.getXAxisLastItem());
          oChartMetadata.setYAxisRangeLabel("y-axis range: " + oStats.getLowestValue() + " - " + oStats.getHighestValue());

          Chart oChart = oFXChart.getChart(cFXChart.ChartType.BAR_CHART, oChartMetadata, oStats);

          Platform.runLater(() ->
          {
            SnapshotParameters oSnapshotParameters = new SnapshotParameters();
            oSnapshotParameters.setTransform(Transform.scale(5, 5));

            BorderPane oChartPane = new BorderPane();
            oChartPane.setCenter(oChart);

            oChartPane.addEventHandler(MouseEvent.MOUSE_PRESSED, event ->
            {
              if (event.isSecondaryButtonDown())
              {
                m_oCurrentPane = oChartPane;
                m_oContextMenu.show(oChartPane, event.getScreenX(), event.getScreenY());
              }
              else
              {
                m_oContextMenu.hide();
              }
            });
            
            Tab oTab = m_oChartsTabsMap.get(sTabName);
            oTab.setContent(oChartPane);
            
          });
        }
      }
      catch (Exception ex)
      {
        g_oLog.error("Exception building chart: '" + sPropertyName + "' Error: " + ex.getMessage(), ex);
      }
    });

  }

  public void buildVolumeChart(String sXAxisLabel, String sYAxisLabel, String sTabName)
  {
    m_oChartsExecutorService.execute(() ->
    {
      try
      {
        cFXChart oFXChart = new cFXChart();
        cNumberOfStatistics oCreatedStats = new cNumberOfStatistics();
        oCreatedStats.setName("Created Time");
        cNumberOfStatistics oModifiedStats = new cNumberOfStatistics();
        oModifiedStats.setName("Modified Time");
        cChartMetadata oChartMetadata = new cChartMetadata(sTabName);

        refactorDataSet(oCreatedStats, oModifiedStats, oChartMetadata);

        final Map<Object, Long> oCreatedKeys = oCreatedStats.getTypeKeyValueStatistics();
        oCreatedKeys.keySet().forEach((key) ->
        {
          oChartMetadata.setXAxisLastItem(key);
          if (oChartMetadata.getXAxisFirstItem() == null)
          {
            oChartMetadata.setXAxisFirstItem(key);
          }

          Long lValue = oCreatedKeys.get(key);
          if (lValue > oCreatedStats.getHighestValue())
          {
            oCreatedStats.setHighestValue(lValue);
          }
        });

        final Map<Object, Long> oModifiedKeys = oModifiedStats.getTypeKeyValueStatistics();
        oModifiedKeys.keySet().forEach((key) ->
        {
          oChartMetadata.setXAxisLastItem(key);
          if (oChartMetadata.getXAxisFirstItem() == null)
          {
            oChartMetadata.setXAxisFirstItem(key);
          }

          Long lValue = oModifiedKeys.get(key);
          if (lValue > oModifiedStats.getHighestValue())
          {
            oModifiedStats.setHighestValue(lValue);
          }
        });

        oChartMetadata.setXAxisLabel(sXAxisLabel);
        oChartMetadata.setYAxisLabel(sYAxisLabel);
        oChartMetadata.setXAxisRangeLabel("x-axis range: " + oChartMetadata.getXAxisFirstItem() + " - " + oChartMetadata.getXAxisLastItem());
        long lLowest = Math.min(oCreatedStats.getLowestValue(), oModifiedStats.getLowestValue());
        long lHighest = Math.max(oCreatedStats.getHighestValue(), oModifiedStats.getHighestValue());
        oChartMetadata.setYAxisRangeLabel("y-axis range: " + lLowest + " - " + lHighest);

        Chart oChart = oFXChart.getChart(cFXChart.ChartType.LINE_CHART, oChartMetadata, oCreatedStats, oModifiedStats);

        BorderPane oChartPane = new BorderPane();
        oChartPane.setCenter(oChart);
        Platform.runLater(() ->
        {
          oChartPane.addEventHandler(MouseEvent.MOUSE_PRESSED, event ->
          {
            if (event.isSecondaryButtonDown())
            {
              m_oCurrentPane = oChartPane;
              m_oContextMenu.show(oChartPane, event.getScreenX(), event.getScreenY());
            }
            else
            {
              m_oContextMenu.hide();
            }
          });

          Tab oTab = m_oChartsTabsMap.get(sTabName);
          oTab.setContent(oChartPane);
          
        });
      }
      catch (Exception ex)
      {
        g_oLog.error("Exception building volume chart. Error: " + ex.getMessage(), ex);
      }
    });
  }

  public void createTabForChart(String sTitle)
  {
    Tab oTab = new Tab(sTitle);
    oTab.setClosable(false);
    m_oChartsTabsMap.put(sTitle, oTab);

    m_oChartsTabPane.getTabs().addAll(oTab);
  }

  /**
   * For the use in the volume graph, set the timestamps of the containers in the dataset to the nearest hour or day.
   */
  private void refactorDataSet(cNumberOfStatistics oCreatedStats, cNumberOfStatistics oModifiedStats, cChartMetadata oChartMetadata)
  {
    boolean bRoundToHour = false;
    if (numberOfHoursBetweenTimestamps() < 50)
    {
      bRoundToHour = true;
    }

    TreeMap<Object, Long> oCreatedTimeStamps = new TreeMap();
    TreeMap<Object, Long> oModifiedTimeStamps = new TreeMap();
    Iterator<IContainer> oIterator = m_oFusionDataSet.iterator();
    while (oIterator.hasNext())
    {
      IContainer oObj = oIterator.next();
      Instant oInstantDateCreated = oObj.getDateCreated();
      Instant oInstantDateModified = oObj.getDateModified();

      long lDateCreated;
      long lDateModified;
      if (bRoundToHour)
      {
        lDateCreated = roundInstantToNearestHour(oInstantDateCreated);
        lDateModified = roundInstantToNearestHour(oInstantDateModified);
      }
      else
      {
        lDateCreated = roundInstantToNearestDay(oInstantDateCreated);
        lDateModified = roundInstantToNearestDay(oInstantDateModified);
      }

      String sDateCreated = g_oSIMPLE_DATE.format(lDateCreated);
      String sDateModified = g_oSIMPLE_DATE.format(lDateModified);

      Long lCreatedCount = oCreatedTimeStamps.get(sDateCreated);
      if (lCreatedCount == null)
      {
        oCreatedTimeStamps.put(sDateCreated, 1L);
      }
      else
      {
        oCreatedTimeStamps.put(sDateCreated, lCreatedCount + 1);
      }

      Long lModifiedCount = oModifiedTimeStamps.get(sDateModified);
      if (lModifiedCount == null)
      {
        oModifiedTimeStamps.put(sDateModified, 1L);
      }
      else
      {
        oModifiedTimeStamps.put(sDateModified, lModifiedCount + 1);
      }

    }

    oCreatedStats.setObjects(oCreatedTimeStamps);
    oModifiedStats.setObjects(oModifiedTimeStamps);
  }

  public int numberOfHoursBetweenTimestamps()
  {
    long lMilliseconds = m_lStopTimestamp - m_lStartTimestamp;
    int iReturn = (int) (lMilliseconds / 1000 / 60 / 60);
    if (iReturn == 0)
    {
      iReturn = 1;
    }
    return iReturn;
  }

  public long roundInstantToNearestDay(Instant oInstant)
  {
    Date d = new Date(oInstant.getEpochSecond() * 1000);
    Calendar c = GregorianCalendar.getInstance();
    c.setTime(d);

    c.set(Calendar.HOUR_OF_DAY, 12);
    c.set(Calendar.MINUTE, 0);
    c.set(Calendar.SECOND, 0);

    return c.getTimeInMillis();
  }

  public long roundInstantToNearestHour(Instant oInstant)
  {
    Date d = new Date(oInstant.getEpochSecond() * 1000);
    Calendar c = GregorianCalendar.getInstance();
    c.setTime(d);

    if (c.get(Calendar.MINUTE) >= 30)
    {
      c.add(Calendar.HOUR, 1);
    }

    c.set(Calendar.MINUTE, 0);
    c.set(Calendar.SECOND, 0);

    return c.getTimeInMillis();
  }

  @Override
  public Node getNode()
  {
    return m_oParent;
  }

  @Override
  public String getViewName()
  {
    return EViews.DASHBOARD_VIEW.name();
  }

  public cStatisticsWrapper addProperty(String sPropertyName, String sPropertyType, Object oPropertyValue, long lCount)
  {
    cStatisticsWrapper oWrapper = m_oProperties.get(sPropertyName);
    if (oWrapper != null)
    {
      Long value = oWrapper.getCount().getValue();
      oWrapper.getCount().setValue(value + lCount);
    }
    else
    {
      oWrapper = new cStatisticsWrapper(sPropertyName, sPropertyType, lCount);
      m_oProperties.put(sPropertyName, oWrapper);
      //m_oTableStatistics.getItems().add(oWrapper);
    }

    oWrapper.addValue(oPropertyValue);

    return oWrapper;
  }

  @Override
  public void update(Observable o, Object obj)
  {
    if (obj instanceof cSyncProgress1)
    {
      cSyncProgress1 oSyncProgress = (cSyncProgress1) obj;
    }
  }
}
