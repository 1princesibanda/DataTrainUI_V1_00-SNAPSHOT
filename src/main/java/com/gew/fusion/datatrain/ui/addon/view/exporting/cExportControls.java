/**
 * Copyright (c) 2019 GEW Technologies (GEW). All rights reserved.
 */
package com.gew.fusion.datatrain.ui.addon.view.exporting;

import com.gew.fusion.datatrain.ui.addon.cFXDataTrainUIAddon;
import com.gew.fusion.datatrain.ui.addon.cFXDataTrainUIManager;
import com.gew.fusion.datatrain.ui.addon.cFXDataTrainUIManager.EViews;
import com.gew.fusion.datatrain.ui.addon.view.cAbstractView;
import com.gew.lib.ui.fx.securefilechooser.cFXSecureFileChooser;
import com.gew.ui.util.fxwidgets.control.IWidgetControl;
import com.gew.ui.util.fxwidgets.control.cWidgetFactory;
import com.gew.util.cThrowable;
import java.io.File;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Label;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.stage.FileChooser.ExtensionFilter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Philip M. Trenwith
 */
public class cExportControls extends cAbstractView
{
  private Label m_oLabelFrom;
  private Label m_oLabelTo;
  private Button m_oButtonExport;
  private IWidgetControl<?> m_oDateTimeControlFrom;
  private IWidgetControl<?> m_oDateTimeControlTo;
  private LocalDateTime m_oDateTimeFrom = null;
  private LocalDateTime m_oDateTimeTo = null;
  private static final Logger m_oLOG = LoggerFactory.getLogger(cFXDataTrainUIAddon.class);
  
  private final cFXSecureFileChooser m_oFXSecureFileChooser;
  
 
  public cExportControls(cFXDataTrainUIManager oManager, int iDefaultWidth, int iDefaultHeight)
  {
    super(oManager, iDefaultWidth, iDefaultHeight);
    m_oFXSecureFileChooser = new cFXSecureFileChooser(m_oDataTrainUIManager.getConfig().getFileChooserConfig());
    m_oFXSecureFileChooser.getExtensionFilters().add(new ExtensionFilter("Fusion Data", "*.fd"));
  }


  @Override
  public void reload()
  {
    if (m_oResourceBundle != null)
    {
      m_oLabelFrom.setText(m_oResourceBundle.getString("label_from_export_view"));
      m_oLabelTo.setText(m_oResourceBundle.getString("label_to_export_view"));
      m_oButtonExport.setText(m_oResourceBundle.getString("button_export"));
    }
  }

  @Override
  protected BorderPane setupPane(Scene oScene)
  {
    m_oParent.prefHeightProperty().bind(oScene.heightProperty());
    m_oParent.prefWidthProperty().bind(oScene.widthProperty());
    
    m_oLabelFrom = new Label("From");
    m_oLabelTo = new Label("To");
    
    Node oDateTimeControlFrom = null;
    Node oDateTimeControlTo = null;
    try
    {
      cWidgetFactory oFXWidgetFactory = cWidgetFactory.getInstance();
      m_oDateTimeControlFrom = oFXWidgetFactory.getControlFor("Start Date", LocalDateTime.class);
      m_oDateTimeControlTo = oFXWidgetFactory.getControlFor("End Date", LocalDateTime.class);
      m_oDateTimeControlFrom.reset();
      m_oDateTimeControlTo.reset();
      
      oDateTimeControlFrom = m_oDateTimeControlFrom.getWidget();
      oDateTimeControlTo = m_oDateTimeControlTo.getWidget();
    }
    catch (Exception ex)
    {
      m_oLOG.error("Exception: " + ex.getMessage());
      m_oLOG.error(cThrowable.getStackTrace(ex));
    }
    
    m_oButtonExport = new Button("Export Data");
    m_oButtonExport.setOnAction((event) -> 
    {
      m_oDateTimeFrom = (LocalDateTime) m_oDateTimeControlFrom.getWidgetValueProperty().getValue();
      m_oDateTimeTo = (LocalDateTime) m_oDateTimeControlTo.getWidgetValueProperty().getValue();
      if (m_oDateTimeFrom != null && m_oDateTimeTo != null)
      {
        String fromTime = g_oSIMPLE_DATE.format(m_oDateTimeFrom.toEpochSecond(ZoneOffset.UTC)*1000);
        String toTime = g_oSIMPLE_DATE.format(m_oDateTimeTo.toEpochSecond(ZoneOffset.UTC)*1000);

        long lStartTime = m_oDateTimeFrom.toEpochSecond(ZoneOffset.UTC)*1000;
        long lEndTime = (m_oDateTimeTo.toEpochSecond(ZoneOffset.UTC)*1000)+999;

        // Choose location to save. 
        Thread oFileSavingThread = new Thread(() -> 
        {
          m_oFXSecureFileChooser.setInitialFileName("data_" + lStartTime + "_" + lEndTime + ".fd");
          File oSelectedFile = m_oFXSecureFileChooser.showSaveDialog(null);
          if (oSelectedFile != null)
          {
            if (!oSelectedFile.getAbsolutePath().endsWith(".fd"))
            {
              oSelectedFile = new File(oSelectedFile.getAbsolutePath()+".fd");
            }
            m_oDataTrainUIManager.serializeFusionDataToFile(oSelectedFile, lStartTime, lEndTime);
          }
        });
        oFileSavingThread.setName("DataTrainUI-Save-Dialog-Thread");
        oFileSavingThread.setDaemon(true);
        oFileSavingThread.start();
      }
      else
      {
        Alert alert = new Alert(AlertType.ERROR, "Specify both start and end times.", ButtonType.OK);
        alert.setTitle("Error");
        alert.showAndWait();
      }
    });
    
    HBox oCenterPane = new HBox(20);
    oCenterPane.setPadding(new Insets(5));
    oCenterPane.getChildren().addAll(m_oLabelFrom, oDateTimeControlFrom, m_oLabelTo, oDateTimeControlTo, m_oButtonExport);
    oCenterPane.setAlignment(Pos.TOP_LEFT);
    
    m_oParent.setCenter(oCenterPane);
    
    return m_oParent;
  }

  @Override
  public Node getNode()
  {
    return m_oParent;
  }

  @Override
  public String getViewName()
  {
    return EViews.EXPORT_VIEW.niceName();
  }
  
}
