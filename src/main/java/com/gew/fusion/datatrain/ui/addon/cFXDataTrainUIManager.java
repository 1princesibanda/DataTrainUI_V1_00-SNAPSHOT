/**
 * Copyright (c) 2019 GEW Technologies (GEW). All rights reserved.
 */
package com.gew.fusion.datatrain.ui.addon;

import com.gew.ifs.ext.datatrain.lib.files.EFileOperationStatus;
import com.gew.ifs.ext.datatrain.lib.files.cFileOperationFeedbackWrapper;
import com.gew.fusion.datatrain.ui.addon.view.cAbstractView;
import com.gew.fusion.datatrain.ui.addon.view.cScreenCapture;
import com.gew.fusion.datatrain.ui.addon.view.dashboard.cDashboardView;
import com.gew.fusion.datatrain.ui.addon.view.exporting.cExportHistoryView;
import com.gew.fusion.datatrain.ui.addon.view.exporting.cExportControls;
import com.gew.fusion.datatrain.ui.addon.view.importing.cImportHistoryView;
import com.gew.fusion.datatrain.ui.addon.view.importing.cImportControls;
import com.gew.fusion.datatrain.ui.addon.view.wizard.cWizardView;
import com.gew.ifs.ext.datatrain.core.transformation.connector.datatrain.files.cFileOperationManager;
import com.gew.lib.multilingual.cBundleManager;
import com.gew.lib.ui.fx.securefilechooser.icons.cIconLoader;
import java.io.File;
import java.util.HashMap;
import java.util.Map;
import javafx.collections.ObservableList;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.gew.ifs.ext.datatrain.lib.files.IFileOperationFeedbackWrapper;
import com.gew.lib.context.interfaces.IContext;
import com.gew.persistence.datatypes.IContainer;
import java.util.Set;
import java.util.concurrent.CancellationException;

/**
 *
 * @author Philip M. Trenwith
 */
public class cFXDataTrainUIManager
{
  /**
   * Logger instance.
   */
  private static final Logger m_oLOG = LoggerFactory.getLogger(cFXDataTrainUIManager.class);
  
  /**
   * Map of views. Enum is used as a key for easy lookups.
   */
  private Map<EViews, cAbstractView> m_oViews;
  
  private cIconLoader m_oIconLoader;
  private cFXDataTrainUIAddonConfiguration m_oConfig;
  private IContext m_oContext;
  private final cBundleManager m_oResourceManager;
  private cFileOperationManager m_oFileOperationManager = new cFileOperationManager();
  /**
   * Screen Capture
   */
  private cScreenCapture m_oScreenCapture; 
  
  /**
   * The view provided by the manager.
   */
  public enum EViews
  {
    EXPORT_VIEW("datatrain_export_controls", "DATATRAIN_EXPORT_CONTROLS"),
    IMPORT_VIEW("datatrain_import_controls", "DATATRAIN_IMPORT_CONTROLS"),
    EXPORT_HISTORY_VIEW("datatrain_export_history_view", "DATATRAIN_EXPORT_HISTORY_VIEW"),
    IMPORT_HISTORY_VIEW("datatrain_import_history_view", "DATATRAIN_IMPORT_HISTORY_VIEW"),
    WIZARD_VIEW("datatrain_wizard_view", "DATATRAIN_WIZARD_VIEW"),
    DASHBOARD_VIEW("datatrain_dashboard_view", "DATATRAIN_DASHBOARD_VIEW");

    private final String m_sView;
    private final String m_sNiceName;

    /**
     * @return the component name of the view, as used in the Addon/layout configuration.
     */
    public String view()
    {
      return m_sView;
    }

    /**
     * @return the nice name of the view.
     */
    public String niceName()
    {
      return m_sNiceName;
    }

    /**
     * Private constructor.
     */
    private EViews(String sView, String sNiceName)
    {
      this.m_sView = sView;
      this.m_sNiceName = sNiceName;
    }
  }
  
  public cFXDataTrainUIManager(cBundleManager oResourceBundle, IContext oContext, cFXDataTrainUIAddonConfiguration oConfig)
  {
    this.m_oResourceManager = oResourceBundle;
    this.m_oContext = oContext;
    this.m_oConfig = oConfig;
    this.m_oIconLoader = new cIconLoader(oConfig.getFileChooserConfig().getIconPacks());
    
    m_oScreenCapture = new cScreenCapture(oContext);
    
    this.m_oViews = new HashMap<>(6);
    this.m_oViews.put(EViews.DASHBOARD_VIEW, new cDashboardView(this, 600, 600));
    this.m_oViews.put(EViews.EXPORT_VIEW, new cExportControls(this, 600, 600));
    this.m_oViews.put(EViews.EXPORT_HISTORY_VIEW, new cExportHistoryView(this, 600, 600));
    this.m_oViews.put(EViews.IMPORT_VIEW, new cImportControls(this, 600, 600));
    this.m_oViews.put(EViews.IMPORT_HISTORY_VIEW, new cImportHistoryView(this, 600, 600));
    this.m_oViews.put(EViews.WIZARD_VIEW, new cWizardView(this, 600, 600));
  }

  public cBundleManager getResourceBundle()
  {
    return m_oResourceManager;
  }
  
  public IContext getContext()
  {
    return m_oContext;
  }
  
  public cScreenCapture getScreenCapture()
  {
    return m_oScreenCapture;
  }

  public cIconLoader getIconLoader()
  {
    return m_oIconLoader;
  }

  public void reloadViews()
  {
    m_oLOG.info("Reloading views ...");
    m_oViews.values().forEach((oView) -> oView.reload());
  }
  
  /**
   * Get a view for the specified type.
   *
   * @param oView the type of view.
   *
   * @return the view.
   */
  public cAbstractView getView(EViews oView)
  {
    return m_oViews.get(oView);
  }
  
  public cFXDataTrainUIAddonConfiguration getConfig()
  {
    return m_oConfig;
  }
  
  public void cancel(ObservableList<IFileOperationFeedbackWrapper> oItems)
  {
    oItems.forEach((oItem) -> 
    {
      String sFilename = oItem.getFileName().get();
      m_oFileOperationManager.cancel(sFilename, oItem);
    });
  }
  
  /**
   * Useful when data from the database within a time span has to be saved to file
   * 
   * @param oFile The file to save the data
   * @param lStartTime The start time
   * @param lEndTime  The end time
   */
  public void serializeFusionDataToFile(File oFile, long lStartTime, long lEndTime)
  {
    cExportHistoryView oView = (cExportHistoryView) m_oViews.get(EViews.EXPORT_HISTORY_VIEW);
    cFileOperationFeedbackWrapper oWrapper = oView.addExport(oFile, lStartTime, lEndTime);
    oWrapper.getStatus().set(EFileOperationStatus.BUSY);
    try
    {
      boolean bSuccess = m_oFileOperationManager.serializeFusionDataToFile(oFile, lStartTime, lEndTime, oWrapper);
      if (bSuccess) 
      {
        m_oLOG.debug("Data successfully extracted to file: " + oFile.getAbsolutePath());
        oWrapper.getProgress().set(100);
        oWrapper.getStatus().set(EFileOperationStatus.SUCCESS);
      }
      else 
      { 
        m_oLOG.debug("Extraction of data to file failed: " + oFile.getAbsolutePath());
        oWrapper.getStatus().set(EFileOperationStatus.FAILURE); 
      }
    }
    catch (CancellationException ex)
    {
      m_oLOG.debug("Extraction of data to file cancelled: " + oFile.getAbsolutePath());
      oWrapper.getSize().set(0L);
      oWrapper.getProgress().set(0);
      oWrapper.getStatus().set(EFileOperationStatus.CANCELLED); 
    }
    oView.invalidateSelectionBindings();
  }
  
  /**
   * Load fusion data that was saved to file and save it to the database
   * @param oFile The data file
   */
  public void deserializeFusionDataFromFile(File oFile)
  {
    cImportHistoryView oView = (cImportHistoryView) m_oViews.get(EViews.IMPORT_HISTORY_VIEW);
    cFileOperationFeedbackWrapper oWrapper = oView.addImport(oFile);
    oWrapper.getStatus().set(EFileOperationStatus.BUSY);
    try
    {
      boolean bSuccess = m_oFileOperationManager.deserializeFusionDataFromFile(oFile, oWrapper);
      if (bSuccess)
      {
        m_oLOG.debug("Data successfully imported from file: " + oFile.getAbsolutePath());
        oWrapper.getStatus().set(EFileOperationStatus.SUCCESS);
        oWrapper.getProgress().set(100);
      }
      else 
      { 
        m_oLOG.debug("Import of data from file failed: " + oFile.getAbsolutePath());
        oWrapper.getStatus().set(EFileOperationStatus.FAILURE); 
      }
    }
    catch (CancellationException ex)
    {
      m_oLOG.debug("Import of data from file cancelled: " + oFile.getAbsolutePath());
      oWrapper.getProgress().set(0);
      oWrapper.getStatus().set(EFileOperationStatus.CANCELLED); 
    }
    oView.invalidateSelectionBindings();
  }
  
  /**
   * Save an non-fusion file to the database
   * @param oFile The file to save
   */
  public void ingestExternalFile(File oFile)
  {
    cImportHistoryView oView = (cImportHistoryView) m_oViews.get(EViews.IMPORT_HISTORY_VIEW);
    cFileOperationFeedbackWrapper oWrapper = oView.addImport(oFile);
    try
    {
      boolean bSuccess = m_oFileOperationManager.ingestExternalFile(oFile, oWrapper);
      if (bSuccess)
      {
        m_oLOG.debug("Ingesting of data from file succeeded: " + oFile.getAbsolutePath());
        oWrapper.getStatus().set(EFileOperationStatus.SUCCESS);
        oWrapper.getProgress().set(100);
      }
      else 
      { 
        m_oLOG.debug("Ingesting of data from file failed: " + oFile.getAbsolutePath());
        oWrapper.getStatus().set(EFileOperationStatus.FAILURE); 
      }
    }
    catch (CancellationException ex)
    {
      m_oLOG.debug("Ingesting of data from file cancelled: " + oFile.getAbsolutePath());
      oWrapper.getProgress().set(0);
      oWrapper.getStatus().set(EFileOperationStatus.CANCELLED); 
    }
    oView.invalidateSelectionBindings();
  }
  
  public Set<IContainer> loadContainers(long lStartTime, long lEndTime)
  {
    Set<IContainer> oFusionData = null;
    try
    {
      cFileOperationManager oManager = new cFileOperationManager();
      oFusionData = oManager.loadFusionData(lStartTime, lEndTime);
    }
    catch (Exception ex)
    {
      m_oLOG.debug("Exception loading containers.");
    }
    return oFusionData;
  }
}
