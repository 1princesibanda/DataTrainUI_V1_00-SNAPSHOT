/**
 * Copyright (c) 2017 GEW Technologies (GEW). All rights reserved.
 */
package com.gew.fusion.datatrain.ui.addon.view;

import com.gew.fusion.datatrain.ui.addon.cFXDataTrainUIManager;
import com.gew.lib.multilingual.cBundleManager;
import com.gew.lib.ui.fx.securefilechooser.icons.cIconLoader;
import com.gew.lib.ui.view.interfaces.IViewableNode;
import java.awt.Dimension;
import java.text.SimpleDateFormat;
import javafx.embed.swing.JFXPanel;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.layout.BorderPane;

/**
 * Abstract View.
 *
 * Base implementation for views.
 *
 * @author Philip M. Tremwith
 */
public abstract class cAbstractView implements IViewableNode
{
  public static SimpleDateFormat g_oSIMPLE_DATE = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
  /**
   * The parent pane
   */
  public BorderPane m_oParent = new BorderPane();
  /**
   * The JFX panel (not needed if the parent is FX).
   */
  private JFXPanel m_oPanel;
  /**
   * The manager instance.
   */
  protected final cFXDataTrainUIManager m_oDataTrainUIManager;
  /**
   * Resource bundle manager.
   */
  protected cBundleManager m_oResourceBundle;
  /**
   * Icon loader.
   */
  protected cIconLoader m_oIconLoader;
  /**
   * The default width.
   */
  protected final int m_iDefaultWidth;
  /**
   * The default height.
   */
  protected final int m_iDefaultHeight;


  /**
   * Constructor.
   *
   * @param oManager       the DIEUI manager.
   * @param iDefaultWidth  the default width.
   * @param iDefaultHeight the default height.
   */
  public cAbstractView(cFXDataTrainUIManager oManager, int iDefaultWidth, int iDefaultHeight)
  {
    this.m_oDataTrainUIManager = oManager;
    this.m_iDefaultWidth = iDefaultWidth;
    this.m_iDefaultHeight = iDefaultHeight;
    if (oManager != null)
    {
      this.m_oResourceBundle = oManager.getResourceBundle();
      this.m_oIconLoader = oManager.getIconLoader();
      initUI();
    }
  }


  /**
   * Initializes the view.
   */
  private void initUI()
  {
    final Scene oScene = new Scene(new Group());
    Group oSceneRoot = (Group) oScene.getRoot();

    m_oParent = setupPane(oScene);
    oSceneRoot.getChildren().add(m_oParent);
    m_oPanel = new JFXPanel();
    m_oPanel.setPreferredSize(new Dimension(m_iDefaultWidth, m_iDefaultHeight));
    m_oPanel.setScene(oScene);
  }


  /**
   * @return the Java FX panel.
   */
  public JFXPanel getPanel()
  {
    return m_oPanel;
  }


  /**
   * Reloads the view. Good place to perform changes for locale changes.
   */
  public abstract void reload();


  /**
   * Setup the view.
   *
   * @param oScene the parent scene for the view.
   *
   * @return the pane containing the view.
   */
  protected abstract BorderPane setupPane(Scene oScene);

}
