/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gew.fusion.datatrain.ui.addon.view.wizard.Controller.mysqlcontroller;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;

/**
 * A class created for use by the cMySQLController controller. This controller will read sql statements to read/write/create data and create Table and Column objects defined here when parsed in
 * config. Use ANTLR to read the input SQL to pull the strings for table and column names and column name types.
 *
 * @author 0400626
 */
public class Table implements ITableCreator
{
	//If table exists, then it always has at least one column and one primary key the column itself
	//If table exists, the name may not be null nor may it be only whitespace, ie an identifier is a valid table name

	private String name;
	Set<Column> columnSet;
	Set<String> primaryKeySet;  //assert this is a subset of columnSet

	public Table(String name, Set<Column> columnSet, Set<String> pkSet)
	{
		this.name = name;
		this.columnSet = columnSet;
		this.primaryKeySet = pkSet;
	}

	public void addColumn(Column column)
	{
		this.columnSet.add(column);
	}

	public void removeColumn(Column column)
	{
		this.columnSet.remove(column);
	}

	public String getName()
	{
		return this.name;
	}

	public void setName(String name)
	{
		this.name = name;
	}

	public Set<String> getColumnNames()
	{
		Set<String> columnNameSet;
		String name;
		Column col;
		Iterator<Column> columnIter;
		
		columnIter = this.columnSet.iterator();
		
		columnNameSet = new HashSet<>();
		
		while(columnIter.hasNext())
		{
			col = columnIter.next();
			name = col.getName();
			columnNameSet.add(name);
		}
		
		return columnNameSet;
	}
	
	protected Column getColumnByName(String columnName) throws Exception
	{
		Column outColumn;
		Iterator<Column> iter;
		
		iter = this.columnSet.iterator();
		while(iter.hasNext())
		{
			outColumn = iter.next();
			if(outColumn.getName().equals(columnName))
				return outColumn;
		}
		
		throw new Exception("Table:getColumnByName(): Table "+this.name+" has no such column with name '"+columnName+"'.");
	}
	
	public void write(Map<String, List<String>> data) throws Exception
	{
		Iterator<String> keyIter;
		String columnName;
		Column column;
		List<String> columnData;

		keyIter = data.keySet().iterator();
		
		while (keyIter.hasNext())
		{
			columnName = keyIter.next();
			column = getColumnByName(columnName);
			columnData = column.getData();
			columnData.addAll(data.get(columnName));
		}
	}

	public Map<String, List<String>> read(List<String> columnNameList) throws Exception
	{
		Map<String, List<String>> outMap;
		Iterator<String> keyIter;
		String columnName;
		Column column;
		List<String> columnData;

		outMap = new HashMap<>();

		for (Iterator<String> iter = columnNameList.iterator(); iter.hasNext();)
		{
			outMap.put(iter.next(), new LinkedList<>());
		}

		keyIter = outMap.keySet().iterator();

		while (keyIter.hasNext())
		{
			columnName = keyIter.next();
			column = getColumnByName(columnName);
			columnData = column.getData();
			outMap.get(columnName).addAll(columnData);
		}

		assert outMap.keySet().containsAll(columnNameList) && columnNameList.containsAll(outMap.keySet()) : "The read data must map all input column names to lists containing the read data";
		return outMap;
	}

	public boolean equals(Object other)
	{
		if (other == null)
			return false;
		return (other instanceof Table) ? this.name.equals(((Table) other).name) && this.columnSet.equals(((Table) other).columnSet) : false;
	}

	public Set<Column> getColumns()
	{
		return this.columnSet;
	}

	public Set<Column> getAutoIncrementColumns()
	{
		Iterator<Column> columnsIter;
		Set<Column> autoIncrementColumnsSet;
		Column column;

		columnsIter = this.columnSet.iterator();
		autoIncrementColumnsSet = new HashSet<>();

		while (columnsIter.hasNext())
		{
			column = columnsIter.next();
			if (column.isAutoIncrement())
				autoIncrementColumnsSet.add(column);
		}
		return autoIncrementColumnsSet;
	}

	public Set<Column> getPrimaryKeyColumsSet()
	{
		Set<Column> primaryKeyColumnsSet;
		Iterator<Column> columnsSetIter;
		Column column;

		primaryKeyColumnsSet = new HashSet<>();
		columnsSetIter = this.columnSet.iterator();

		while (columnsSetIter.hasNext())
		{
			column = columnsSetIter.next();
			if (this.primaryKeySet.contains(column.getName()))
				primaryKeyColumnsSet.add(column);
		}

		assert primaryKeyColumnsSet != null : "in the very least getPrimaryKeyColumsSet() returns an object with no columns, but never null.";

		return primaryKeyColumnsSet;
	}

	public Set<String> getPrimaryKeyColumNamesSet()
	{
		return this.primaryKeySet;
	}

	@Override
	public int hashCode()
	{
		int hash = 3;
		hash = 37 * hash + Objects.hashCode(this.name);
		hash = 37 * hash + Objects.hashCode(this.columnSet);
		return hash;
	}

	@Override
	public String toString()
	{
		return this.name;
	}

}
