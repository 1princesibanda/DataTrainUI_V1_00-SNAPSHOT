/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gew.fusion.datatrain.ui.addon.view.wizard.com.gew.datatrain.ui;

import static com.gew.fusion.datatrain.ui.addon.view.wizard.com.gew.datatrain.ui.IDataTrainView.runningMap;
import com.gew.fusion.datatrain.ui.addon.view.wizard.com.gew.datatrain.ui.cView3.Properties2;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Objects;
import java.util.Set;

/**
 *
 * @author 0400626
 */
public class OutputMap
{

	private String mapName;
	private String filename;
	private String userQuery;
	private String schema;
	private String host;
	private String password;
	private String userName;
	private Set<Properties2> propertiesSet;

	public OutputMap(String host, String schema, String password, String mapName, String userName, String filename, String userQuery, Set<Properties2> propertiesSet)
	{
		this.mapName = mapName;
		this.filename = filename;
		this.userQuery = userQuery;
		this.propertiesSet = propertiesSet;
		this.schema = schema;
		this.host = host;
		this.password = password;
		this.userName = userName;
	}

	public OutputMap(String name, String filename, String userName, String userQuery)
	{
		this.mapName = name;
		this.filename = filename;
		this.userQuery = userQuery;
		this.userName = userName;
		this.propertiesSet = new HashSet<>();
	}

	public OutputMap()
	{
		this.mapName = this.filename = this.userQuery = this.host = this.password = this.userName = null;
		this.propertiesSet = new HashSet<>();
	}

	public OutputMap(OutputMap cloned) throws Exception
	{
		if (cloned == null)
			throw new Exception("Null is illegal constructor argument.");

		this.mapName = new String(cloned.getName());
		this.filename = new String(cloned.getFilename());
		this.userQuery = new String(cloned.getUserQuery());
		this.propertiesSet = new HashSet<>(cloned.getProperties());
		this.schema = new String(cloned.getSchema());
		this.host = new String(cloned.getHost());
		this.password = new String(cloned.getPassword());
		this.userName = new String(cloned.getUserName());
	}

	public String getName()
	{
		return this.mapName;
	}

	public void setName(String name)
	{
		this.mapName = name;
	}

	public String getFilename()
	{
		return this.filename;
	}

	public void setFilename(String filename)
	{
		this.filename = filename;
	}

	public Set<Properties2> getProperties()
	{
		return this.propertiesSet;
	}

	public void setProperties(Set<Properties2> props)
	{
		this.propertiesSet = props;
	}

	public String getHost()
	{
		return this.host;
	}

	public void setHost(String host)
	{
		this.host = host;
	}

	public String getSchema()
	{
		return this.schema;
	}

	public void setSchema(String schema)
	{
		this.schema = schema;
	}

	public void setPassword(String password)
	{
		this.password = password;
	}

	public String getPassword()
	{
		return this.password;
	}

	public String getUserName()
	{
		return this.userName;
	}

	public void setUserName(String uname)
	{
		this.userName = uname;
	}

	public void setUserQuery(String str)
	{
		this.userQuery = str;
	}

	public String getUserQuery()
	{
		return this.userQuery;
	}

	public static OutputMap clone(OutputMap clonee)
	{
		OutputMap m = new OutputMap();
		m.setName(runningMap.getName());
		m.setFilename(runningMap.getFilename());
		m.setHost(runningMap.getHost());
		m.setPassword(runningMap.getPassword());
		m.setProperties(runningMap.getProperties());
		m.setSchema(runningMap.getSchema());
		m.setUserName(runningMap.getUserName());
		m.setUserQuery(runningMap.getUserQuery());

		return m;
	}

	@Override
	public boolean equals(Object other)
	{
		if (!(other instanceof OutputMap))
			return false;
		return ((OutputMap) other).getName().equals(this.getName());
	}

	@Override
	public int hashCode()
	{
		int hash = 5;
		hash = 17 * hash + Objects.hashCode(this.mapName);
		return hash;
	}

	public Properties2 getPropertyByName(String propertyName) throws Exception
	{
		Iterator<Properties2> iter = this.propertiesSet.iterator();
		Properties2 prop;
		String name;

		while (iter.hasNext())
		{
			prop = iter.next();
			name = prop.getColumnName();
			if (name.equals(propertyName))
				return prop;
		}
		throw new Exception("No Properties2 with columnName '" + propertyName + "'");

	}
}
