/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gew.fusion.datatrain.ui.addon.view.wizard.Controller.textfilecontroller;

import java.util.List;
import java.util.Set;

/**
 *
 * @author 0400626
 */
public interface ITextFileController
{
	public String getDataItem(String path);
	
	public List<String> getData(String path);
	
	public void insertData(String path, String data);
	
	public void updateData(String path);
}
