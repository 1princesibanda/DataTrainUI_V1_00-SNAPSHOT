/**
 * Copyright (c) 2019 GEW Technologies (GEW). All rights reserved.
 */
package com.gew.fusion.datatrain.ui.addon;

import com.gew.fusion.datatrain.ui.addon.cFXDataTrainUIManager.EViews;
import com.gew.fusion.datatrain.ui.addon.view.cAbstractView;
import com.gew.lib.addon.cAddon;
import com.gew.lib.addon.cAddonMetaData;
import com.gew.lib.addon.interfaces.IAddonViewNodeList;
import com.gew.lib.configuration.IConfigurationManager;
import com.gew.lib.configuration.cConfigurationManagerFactory;
import com.gew.lib.configuration.exceptions.cConfigurationException;
import com.gew.lib.context.interfaces.IContext;
import com.gew.lib.multilingual.cBundleManager;
import com.gew.lib.multilingual.cLocaleChangeManager;
import com.gew.lib.multilingual.event.ILocaleChangeListener;
import com.gew.lib.multilingual.event.cLocaleChangeEvent;
import com.gew.lib.ui.view.cViewableNode;
import java.io.IOException;
import java.util.Optional;
import javafx.application.Platform;
import javafx.embed.swing.JFXPanel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Philip M. Trenwith
 */
public class cFXDataTrainUIAddon extends cAddon<Object> implements IAddonViewNodeList, ILocaleChangeListener
{
  static
  {
    // Ensure FX initialised.
    new JFXPanel();
  }
  /**
   * Logger instance.
   */
  private static final Logger m_oLOG = LoggerFactory.getLogger(cFXDataTrainUIAddon.class);
  /**
   * Context.
   */
  private final IContext m_oContext;
  /**
   * Resource bundle.
   */
  private cBundleManager m_oResourceBundle;
  /**
   * The configuration manager instance.
   */
  private IConfigurationManager m_oConfigManager;
  /**
   * The viewable components.
   */
  private cViewableNode[] m_oViewableNodes;
  /**
   * The manager instance.
   */
  private cFXDataTrainUIManager m_oManager;
  
          
  /**
   * @param oAddonMetaData
   * @param oContext 
   */
  public cFXDataTrainUIAddon(cAddonMetaData oAddonMetaData, IContext oContext)
  {
    super(oAddonMetaData);
    this.m_oContext = oContext;
    
    try
    {
      this.m_oResourceBundle = new cBundleManager(cFXDataTrainUIAddon.class);
      this.m_oResourceBundle.initialiseBundle(cFXDataTrainUIAddon.class);
      localeChanged(null);
    } 
    catch (Exception ex)
    {
      m_oLOG.debug("Exception initializing Resource Bundle: " + ex.getMessage(), ex);
    }
    
    try
    {
      this.m_oConfigManager = cConfigurationManagerFactory.getInstance().getManager();
    } 
    catch (Exception ex)
    {
      m_oLOG.debug("Exception initializing Resource Bundle: " + ex.getMessage(), ex);
    }
    cLocaleChangeManager.getLocaleChangeManager().addLocaleChangeListener(this);
  }
  
  

  @Override
  public void initAddon()
  {
    cFXDataTrainUIAddonConfiguration oConfig = getConfiguration();
    this.m_oManager = new cFXDataTrainUIManager(m_oResourceBundle, m_oContext, oConfig);
    
    this.m_oViewableNodes = new cViewableNode[EViews.values().length];
    for (int i = 0; i < EViews.values().length; i++)
    {
      EViews eView = EViews.values()[i];
      cAbstractView oView = m_oManager.getView(eView);
      m_oViewableNodes[i] = new cViewableNode(eView.view(), (oView!=null) ? oView.getNode() : null);
    }
  }
  
  /**
   * Get the configuration instance for the UI.
   */
  private cFXDataTrainUIAddonConfiguration getConfiguration()
  {
    try
    {
      m_oConfigManager.loadConfiguration(cFXDataTrainUIAddonConfiguration.class, Optional.empty(), false);
      return m_oConfigManager.getConfiguration(cFXDataTrainUIAddonConfiguration.class, Optional.empty());
    }
    catch (cConfigurationException | IOException | IllegalArgumentException e)
    {
      m_oLOG.error("Unable to retrieve configuration for the DIEUI.", e);
      return null;
    }
  }

  
  @Override
  public cViewableNode[] getViewableNodes()
  {
    return m_oViewableNodes;
  }

  
  @Override
  public void localeChanged(cLocaleChangeEvent lce)
  {
    m_oResourceBundle.initialiseBundle(cFXDataTrainUIAddon.class);
    if (m_oManager != null)
    {
      Platform.runLater(() -> m_oManager.reloadViews());
    }
  }

  @Override
  public Object getPublishedLocalizedModel()
  {
    return null;
  }
}
