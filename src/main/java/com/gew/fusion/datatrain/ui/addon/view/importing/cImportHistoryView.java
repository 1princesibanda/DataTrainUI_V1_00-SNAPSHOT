/**
 * Copyright (c) 2019 GEW Technologies (GEW). All rights reserved.
 */
package com.gew.fusion.datatrain.ui.addon.view.importing;

import com.gew.fusion.datatrain.ui.addon.cFXDataTrainUIManager;
import com.gew.ifs.ext.datatrain.lib.files.EFileOperationStatus;
import com.gew.ifs.ext.datatrain.lib.files.cFileOperationFeedbackWrapper;
import com.gew.fusion.datatrain.ui.addon.view.factories.cTableCellFactory;
import com.gew.fusion.datatrain.ui.addon.data.cUtil;
import com.gew.fusion.datatrain.ui.addon.view.cAbstractView;
import java.io.File;
import javafx.beans.binding.Bindings;
import javafx.beans.binding.BooleanBinding;
import javafx.collections.ObservableList;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import com.gew.ifs.ext.datatrain.lib.files.IFileOperationFeedbackWrapper;

/**
 *
 * @author Philip M. Trenwith
 */
public final class cImportHistoryView extends cAbstractView
{
  
  private static final int m_iCOLUMNS = 6;
  private TableView<IFileOperationFeedbackWrapper> m_oTableHistory;
  private TableColumn m_oColName;
  private TableColumn m_oColStatus;
  private TableColumn m_oColProgress;
  private TableColumn m_oColSize;
  private TableColumn m_oColStartTime;
  private TableColumn m_oColEndTime;
  private Button m_oButtonCancel;
  private Button m_oButtonDelete;
  private Button m_oButtonRefresh;
  private Label m_oLabelNoContentPlaceholder;
  private ObservableList<IFileOperationFeedbackWrapper> m_oSelection;
  private BooleanBinding m_oSelectedAreBusy;
  private BooleanBinding m_oSelectedAreDone;

  public cImportHistoryView(cFXDataTrainUIManager oManager, int iDefaultWidth, int iDefaultHeight)
  {
    super(oManager, iDefaultWidth, iDefaultHeight);
  }


  @Override
  public void reload()
  {
    if (m_oResourceBundle != null)
    {
      m_oColName.setText(m_oResourceBundle.getString("label_column_name"));
      m_oColStatus.setText(m_oResourceBundle.getString("label_column_status"));
      m_oColProgress.setText(m_oResourceBundle.getString("label_column_progress"));
      m_oColSize.setText(m_oResourceBundle.getString("label_column_size"));
      m_oColStartTime.setText(m_oResourceBundle.getString("label_column_start_time"));
      m_oColEndTime.setText(m_oResourceBundle.getString("label_column_end_time"));
      m_oButtonCancel.setText(m_oResourceBundle.getString("button_cancel"));
      m_oButtonDelete.setText(m_oResourceBundle.getString("button_delete"));
      m_oButtonRefresh.setText(m_oResourceBundle.getString("button_refresh"));
      m_oLabelNoContentPlaceholder.setText(m_oResourceBundle.getString("table_no_content_placeholder"));
    }
  }


  @Override
  protected BorderPane setupPane(Scene oScene)
  {
    m_oParent.prefHeightProperty().bind(oScene.heightProperty());
    m_oParent.prefWidthProperty().bind(oScene.widthProperty());

    VBox oPaneContent = new VBox(5.0d);
    
    m_oLabelNoContentPlaceholder = new Label("No Content");

    m_oTableHistory = new TableView<>();
    m_oTableHistory.prefHeightProperty().bind(oPaneContent.heightProperty());
    m_oTableHistory.prefWidthProperty().bind(oPaneContent.widthProperty());
    m_oTableHistory.setEditable(false);
    m_oTableHistory.setPlaceholder(m_oLabelNoContentPlaceholder);

    m_oColName = new TableColumn("Name");
    m_oColName.setCellValueFactory(cTableCellFactory.fileNameValueFactory());
    m_oColName.setCellFactory((o) -> cTableCellFactory.stringCellFactory());
    m_oColName.setComparator(cUtil.g_oSTRING_COMPARATOR);
    m_oColName.prefWidthProperty().bind(m_oTableHistory.widthProperty().divide(m_iCOLUMNS));

    m_oColStatus = new TableColumn("Status");
    m_oColStatus.setCellValueFactory(cTableCellFactory.statusValueFactory());
    m_oColStatus.setCellFactory((o) -> cTableCellFactory.statusCellFactory());
    m_oColStatus.setComparator(cUtil.g_oSTATUS_COMPARATOR);
    m_oColStatus.prefWidthProperty().bind(m_oTableHistory.widthProperty().divide(m_iCOLUMNS));

    m_oColProgress = new TableColumn("Progress");
    m_oColProgress.setCellValueFactory(cTableCellFactory.progressValueFactory());
    m_oColProgress.setCellFactory((o) -> cTableCellFactory.progressCellFactory());
    m_oColProgress.setComparator(cUtil.g_oINTEGER_COMPARATOR);
    m_oColProgress.prefWidthProperty().bind(m_oTableHistory.widthProperty().divide(m_iCOLUMNS));

    m_oColSize = new TableColumn("Size");
    m_oColSize.setCellValueFactory(cTableCellFactory.sizeValueFactory());
    m_oColSize.setCellFactory((o) -> cTableCellFactory.sizeCellFactory());
    m_oColSize.setComparator(cUtil.g_oSTRING_COMPARATOR);
    m_oColSize.prefWidthProperty().bind(m_oTableHistory.widthProperty().divide(m_iCOLUMNS));

    m_oColStartTime = new TableColumn("Start Time");
    m_oColStartTime.setCellValueFactory(cTableCellFactory.startTimeValueFactory());
    m_oColStartTime.setCellFactory((o) -> cTableCellFactory.timeCellFactory());
    m_oColStartTime.setComparator(cUtil.g_oTIME_COMPARATOR);
    m_oColStartTime.prefWidthProperty().bind(m_oTableHistory.widthProperty().divide(m_iCOLUMNS));

    m_oColEndTime = new TableColumn("End Time");
    m_oColEndTime.setCellValueFactory(cTableCellFactory.endTimeValueFactory());
    m_oColEndTime.setCellFactory((o) -> cTableCellFactory.timeCellFactory());
    m_oColEndTime.setComparator(cUtil.g_oTIME_COMPARATOR);
    m_oColEndTime.prefWidthProperty().bind(m_oTableHistory.widthProperty().divide(m_iCOLUMNS));

    m_oTableHistory.getColumns().addAll(m_oColName, m_oColSize, m_oColStatus, m_oColProgress, m_oColStartTime, m_oColEndTime);
    m_oTableHistory.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
    m_oTableHistory.getSortOrder().addAll(m_oColName, m_oColSize, m_oColStatus, m_oColProgress, m_oColStartTime, m_oColEndTime);

    m_oSelection = m_oTableHistory.getSelectionModel().getSelectedItems();

    // A boolean binding taking into account all selected items are busy, i.e. not (success or failure).
    m_oSelectedAreBusy = Bindings.createBooleanBinding(() ->
    {
      return m_oSelection.size() > 0 && m_oSelection.stream().allMatch((o) -> !(o.getStatus().getValue().equals(EFileOperationStatus.SUCCESS)
                                                                          || o.getStatus().getValue().equals(EFileOperationStatus.FAILURE)
                                                                          || o.getStatus().getValue().equals(EFileOperationStatus.CANCELLED)));
    }, m_oSelection);

    // A boolean binding taking into account all selected items are done, i.e. (success or failure).
    m_oSelectedAreDone = Bindings.createBooleanBinding(() ->
    {
      return m_oSelection.size() > 0 && m_oSelection.stream().allMatch((o) -> (o.getStatus().getValue().equals(EFileOperationStatus.SUCCESS)
                                                                          || o.getStatus().getValue().equals(EFileOperationStatus.FAILURE)
                                                                          || o.getStatus().getValue().equals(EFileOperationStatus.CANCELLED)));
    }, m_oSelection);

     // Buttons.
    HBox oPaneButtons = new HBox(5.0d);
    oPaneButtons.setAlignment(Pos.CENTER_RIGHT);
    oPaneButtons.setPadding(new Insets(5.0d));

    // TODO: Cancel/Delete/Refresh buttons.
    m_oButtonCancel = new Button("Cancel", m_oIconLoader.getIcon("gear_stop.png", 16));
    m_oButtonCancel.setOnAction((oEvent) ->
    {
      m_oDataTrainUIManager.cancel(m_oTableHistory.getSelectionModel().getSelectedItems());
    });
    
    m_oButtonCancel.disableProperty().bind(m_oSelectedAreBusy.not());

    // Delete button.
    m_oButtonDelete = new Button("Delete", m_oIconLoader.getIcon("delete.png", 16));
    m_oButtonDelete.setOnAction((oEvent) ->
    {
      ObservableList<IFileOperationFeedbackWrapper> selectedItems = m_oTableHistory.getSelectionModel().getSelectedItems();
      m_oTableHistory.getItems().removeAll(selectedItems);
    });
    
    m_oButtonDelete.disableProperty().bind(m_oSelectedAreDone.not());

    // Refresh button.
//    m_oButtonRefresh = new Button(m_oResourceBundle.getString("label_refresh"),
//                                  m_oIconLoader.getIcon("refresh.png", 16));
//    m_oButtonRefresh.setOnAction((oEvent) ->
//    {
//      m_oManager.updateExports();
//    });

    oPaneButtons.getChildren().addAll(m_oButtonCancel, m_oButtonDelete);
    m_oParent.setBottom(oPaneButtons);
    oPaneContent.getChildren().addAll(m_oTableHistory);

    // TODO: Filtered and sorted exports view.
//    FilteredList<IExportImportStatus> oFilteredExports = new FilteredList<>(m_oManager.getExports(), (p) -> true);
//    oFilteredExports.setPredicate(oExportItem -> allowToShow(oExportItem));
//    
//    SortedList<IExportImportStatus> oSortedExports = new SortedList<>(oFilteredExports);
//    oSortedExports.comparatorProperty().bind(m_oTableHistory.comparatorProperty());
//
//    m_oTableHistory.setItems(oSortedExports);

    ScrollPane oScrollPane = new ScrollPane(oPaneContent);
    oScrollPane.prefHeightProperty().bind(oScene.heightProperty());
    oScrollPane.prefWidthProperty().bind(oScene.widthProperty());

    oPaneContent.prefHeightProperty().bind(oScrollPane.heightProperty().subtract(5.0d));
    oPaneContent.prefWidthProperty().bind(oScrollPane.widthProperty().subtract(5.0d));

    m_oParent.setCenter(oScrollPane);
    return m_oParent;
  }


  @Override
  public Node getNode()
  {
    return m_oParent;
  }

  @Override
  public String getViewName()
  {
    return cFXDataTrainUIManager.EViews.IMPORT_HISTORY_VIEW.niceName();
  }

  public cFileOperationFeedbackWrapper addImport(File oFile)
  {
    cFileOperationFeedbackWrapper oWrapper = new cFileOperationFeedbackWrapper(oFile, EFileOperationStatus.PENDING, 0, 0, false);
    m_oTableHistory.getItems().add(oWrapper);
    return oWrapper;
  }

  public void invalidateSelectionBindings()
  {
    m_oSelectedAreBusy.invalidate();
    m_oSelectedAreDone.invalidate();
  }
}
