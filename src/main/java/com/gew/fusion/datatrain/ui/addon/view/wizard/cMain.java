/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gew.fusion.datatrain.ui.addon.view.wizard;

import com.gew.fusion.datatrain.ui.addon.view.wizard.Controller.mysqlcontroller.IApplication;
import com.gew.fusion.datatrain.ui.addon.view.wizard.Controller.mysqlcontroller.IController;
import com.gew.fusion.datatrain.ui.addon.view.wizard.Controller.mysqlcontroller.IView;
import com.gew.fusion.datatrain.ui.addon.view.wizard.Controller.mysqlcontroller.cMySQLController;
import com.gew.fusion.datatrain.ui.addon.view.wizard.DataTrain.cDataTrain;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;
import com.gew.fusion.datatrain.ui.addon.view.wizard.com.gew.datatrain.ui.IDataTrainView;
import com.gew.fusion.datatrain.ui.addon.view.wizard.com.gew.datatrain.ui.cView1;
import com.gew.fusion.datatrain.ui.addon.view.wizard.com.gew.datatrain.ui.cView2;
import com.gew.fusion.datatrain.ui.addon.view.wizard.com.gew.datatrain.ui.cView3;
import com.gew.fusion.datatrain.ui.addon.view.wizard.com.gew.datatrain.ui.cView4;
import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedList;

/**
 *
 * @author 0400626
 */
public class cMain extends Application
{

	Stage stage;
	Pane pane;
	IDataTrainView firstView, secondView, thirdView, fourthView;
	IController mysqlController;
	Collection<IView> viewsList;
	Collection<IApplication> applicationsList;
	IApplication application;

	public cMain() throws Exception
	{
		this.mysqlController = cMySQLController.newMySQLController();

		this.viewsList = new LinkedList<>();
		this.applicationsList = new LinkedList<>();

		this.firstView = new cView1(this.mysqlController, null);
		this.secondView = new cView2(this.mysqlController, this.firstView, null);
		this.thirdView = new cView3(this.mysqlController, this.secondView, null);
		this.fourthView = new cView4(this.mysqlController);
		this.fourthView.setNextView(secondView);

		this.application = new cDataTrain();
	}

	@Override
	public void init() throws Exception
	{
		this.stage = null;

		this.viewsList.add(this.firstView);
		this.viewsList.add(this.secondView);
		this.viewsList.add(this.thirdView);
		this.viewsList.add(this.fourthView);
		this.mysqlController.setViews(viewsList);

		this.applicationsList.add(application);
		this.mysqlController.setApplications(applicationsList);
		((cMySQLController) this.mysqlController).setPassThrough(true);
		this.firstView.setNextView(this.secondView);
		this.secondView.setNextView(this.thirdView);
		this.thirdView.setNextView(this.fourthView);

		this.firstView.init();
		this.secondView.init();
		this.thirdView.init();
		this.fourthView.init();

		assert this.mysqlController.getApplications().containsAll(applicationsList);
		assert this.mysqlController.getViews().containsAll(viewsList);
	}

	@Override
	public void start(Stage stage) throws Exception
	{
		this.stage = stage;
		//this.init(); cMain(stage);
		this.fourthView.setStage(this.stage);
		this.fourthView.setPreviousView(this.thirdView);

		this.thirdView.setStage(this.stage);
		this.secondView.setStage(this.stage);

		firstView.start(stage);

		//All must happen in a Scene, which must happen on a Stage. A Scene must be created with a root Parent- a Node that can have children. A Pane is such a Parent.
		//stage.show();
	}

	@Override
	public void stop() throws Exception
	{
		this.firstView.stop();
		this.secondView.stop();
		this.thirdView.stop();
		this.fourthView.stop();
	}

	public Pane getFirstViewPane()
	{
		return ((cView1) this.firstView).getPane();
	}

	public IDataTrainView getFirstView()
	{
		return this.firstView;
	}

	public void setPaneManager(Object obj)
	{
		IDataTrainView.setPaneManager(obj);
	}
	
	public static void main(String[] args)
	{
		launch(args);
	}

}
