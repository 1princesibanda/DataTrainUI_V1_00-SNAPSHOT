/*
 * Copyright (c) 2019 GEW Technologies (GEW). All rights reserved.
 */
package com.gew.fusion.datatrain.ui.addon.view.dashboard;

import com.gew.fusion.datatrain.ui.addon.cFXDataTrainUIManager;
import com.gew.fusion.datatrain.ui.addon.data.cEtlMapItemSummary;
import com.gew.fusion.datatrain.ui.addon.data.cMapSummaryConfig;
import com.gew.ifs.ext.datatrain.core.transformation.cMap;
import com.gew.ifs.ext.datatrain.core.transformation.connector.rdbms.cJDBCDriver;
import com.gew.ifs.ext.datatrain.lib.config.IProperties;
import com.gew.ifs.ext.datatrain.lib.config.cConfig;
import com.gew.ifs.ext.datatrain.lib.config.cMapConfig;
import com.gew.ifs.ext.datatrain.lib.config.cTransformationParameters;
import com.gew.persistence.IPersistenceStore;
import com.gew.persistence.cPersistenceStoreFactory;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.Collection;
import java.util.TreeMap;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.w3c.dom.Element;

/**
 * This will be manage the source destination count view and obtaining the data from the sources.
 * 
 * @author 0400553
 */
public class cSourceDestinationViewManager
{
  private final cSourceDestinationCountView view;
  private cJDBCDriver m_oJDBCDriver;
  private IPersistenceStore destinationDbCon;
  private boolean dbConnectorsInitialzing = false;
  private Connection sourceConnector;
  private cMapConfig mapConfig;
  
  private long lastReloadTime=0;
  
  private long RELOAD_INTERVAL = 60000*10; //Reload every 10 minutes
  
  
  
  public cSourceDestinationViewManager(cFXDataTrainUIManager masterUiManager) 
  {
    //Init view
    view = new cSourceDestinationCountView(masterUiManager ,600, 800);
    try
    {
      cConfig etlConfig = cConfig.instance();
      etlConfig.loadConfig();
      initDataCollectors();
    }
    catch (Exception ex)
    {
      Logger.getLogger(cSourceDestinationViewManager.class.getName()).log(Level.SEVERE, null, ex);
    }
  }
  
  

  
  /**
   * Initialize the data Collectors on a new Thread.
   * @throws Exception 
   */
  private void initDataCollectors() throws Exception
  {
    new Thread(() ->
    {
      try
      {
        loadMaps();
        startSchedule();
      }
      catch (Exception ex)
      {
        Logger.getLogger(cSourceDestinationViewManager.class.getName()).log(Level.SEVERE, null, ex);
      }
    }).start();
    
  }
  
  /**
   * Initialize the db connections. Done on a seperate thread to prevent map loading from hanging
   * @param mapConfig 
   */
  private synchronized void initDbConnectors(cMapConfig mapConfig)
  {
    if(!dbConnectorsInitialzing)
    {
      dbConnectorsInitialzing=true;
      new Thread(() ->
      {
        try
        {
          if(m_oJDBCDriver==null)
          {
            m_oJDBCDriver = new cJDBCDriver("General Connector", true);
            if (!m_oJDBCDriver.isConfigured())
              m_oJDBCDriver.loadJDBCSettings(mapConfig);
          }
            
          if(sourceConnector!=null)
            sourceConnector.close();
                  
          sourceConnector = m_oJDBCDriver.getConnection(true);
          
          
          System.out.println("JDBC Connected");
          
          destinationDbCon = cPersistenceStoreFactory.getInstance();
          System.out.println("Neo4j connected");
        }
        catch (Exception ex)
        {
          Logger.getLogger(cSourceDestinationViewManager.class.getName()).log(Level.SEVERE, null, ex);
        }
      }).start();
      
    }
  }
  
  /**
   * This will reload the database connection if they are faulty.
   */
  private synchronized void reloadDBConnectors()
  {
    if((System.currentTimeMillis() - lastReloadTime )>RELOAD_INTERVAL)
    {
      lastReloadTime = System.currentTimeMillis();
      dbConnectorsInitialzing = false;
      initDbConnectors(mapConfig);
    }
  }
  
  /**
   * Load the maps from config.
   * @throws Exception 
   */
  private void loadMaps() throws Exception
  {
    TreeMap<String, cEtlMapItemSummary> sortedMap = new TreeMap<>();
    Collection<cTransformationParameters> transformations = cTransformationParameters.getTransformations();
    for (cTransformationParameters transformation : transformations)
    {
      if (transformation.sXsdFile == null ||transformation.sXsdFile.isEmpty())
        transformation.sXsdFile = IProperties.sPROPERTY_ETL_MAP_SCHEMA_DEFAULT;

      mapConfig = new cMapConfig(transformation.sXmlFile, transformation.sXsdFile);
      Element oMaps = mapConfig.getElement("Maps");
      ArrayList<Element> maps = mapConfig.getChildElements(oMaps, "Map");

      maps.stream().forEach((oMapElement) ->
      {
        // create the map and put it in the list for the schedular to execute.
        String sMapName = oMapElement.getAttribute("Name");

        //@TODO - Only process entities, this is to change
        Element oOutputElement = mapConfig.getChildElement(oMapElement, "Output");
        String containerType = mapConfig.getChildElement(oOutputElement, "container").getAttribute("Type");


        if(containerType.equalsIgnoreCase("entity"))
        {
          initDbConnectors(mapConfig);
          //Create the Summary map item
          Element oInputElement = mapConfig.getChildElement(oMapElement, "Input");
          Element oTransformationElement = mapConfig.getChildElement(oMapElement, "Transformation");

          cMapSummaryConfig summaryConfig = new cMapSummaryConfig(sMapName
                  ,  new cMap(sMapName, sMapName, 0, "",oInputElement, oTransformationElement, null, oOutputElement)
                  ,mapConfig
          );
          sortedMap.put(sMapName, new cEtlMapItemSummary(summaryConfig));
        }
      });
    } 
    
    sortedMap.values().forEach((e)->view.addOrUpdate(e));
    view.reload();
  }
  
  /**
   * Start the schedule to recalculate the values from the db.
   */
  private void startSchedule()
  {
    ScheduledExecutorService scheduleThreadPool = Executors.newSingleThreadScheduledExecutor();
    Runnable task = () ->
    {
      //Only perform calculation when db connections are available
      if(m_oJDBCDriver==null || sourceConnector==null) 
        return;
      view.getViewData().forEach((t) ->
      {
        try
        {
          t.reloadData(sourceConnector, destinationDbCon);
        }
        catch (Exception e)
        {
          e.printStackTrace();
          reloadDBConnectors();
        }
      });
      
    };
    scheduleThreadPool.scheduleAtFixedRate(task, 10, 60, TimeUnit.SECONDS);
  }
  
  /**
   * Get the published view.
   * @return 
   */
  public cSourceDestinationCountView getView()
  {
    return view;
  }
}
