/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gew.fusion.datatrain.ui.addon.view.wizard.Controller.textfilecontroller.antlr4;

import com.gew.fusion.datatrain.ui.addon.view.wizard.Controller.mysqlcontroller.ISQLParser;
import com.gew.fusion.datatrain.ui.addon.view.wizard.Controller.mysqlcontroller.Table;
import com.gew.fusion.datatrain.ui.addon.view.wizard.Controller.mysqlcontroller.cTableDefinitionVisitor;
import com.gew.fusion.datatrain.ui.addon.view.wizard.parsers.antlr4.mysql.tabledefinition.TableDefinitionLexer;
import com.gew.fusion.datatrain.ui.addon.view.wizard.parsers.antlr4.mysql.tabledefinition.TableDefinitionParser;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Set;
import org.antlr.v4.runtime.ANTLRInputStream;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.tree.ParseTree;

/**
 *
 * @author 0400626 Use the parser produced by antlr4 to provide read/write into a mySql table definitions file
 *
 */
public class cMySQLTableDefinitionController
{

	public static final String DEFAULT_TABLE_DEFINITION_SQL_FILE = "/main/resources/tableCreationStatements.sql";

	TableDefinitionLexer lexer;
	ANTLRInputStream input;
	InputStream inputStream;
	String inputSqlFilename;
	CommonTokenStream tokens;
	TableDefinitionParser parser;
	ParseTree tree;
	cTableDefinitionVisitor eval;

	//cannot call constructor with string sql statements because antlr4 needs to know the file its parsing
	public cMySQLTableDefinitionController(String inputSqlFilename) throws FileNotFoundException, IOException
	{
		this.inputSqlFilename = inputSqlFilename;
		this.inputStream = new FileInputStream(inputSqlFilename);
		//this.inputStream = this.getClass().getResourceAsStream(this.inputSqlFilename);
		assert this.inputStream != null;
		this.input = new ANTLRInputStream(this.inputStream);
		this.lexer = new TableDefinitionLexer(input);

		this.tokens = new CommonTokenStream(this.lexer);
		this.parser = new TableDefinitionParser(tokens);
		this.tree = parser.prog(); // parse

		this.eval = new cTableDefinitionVisitor();

		//this.eval.visit(tree);
	}

	private void init() throws FileNotFoundException
	{
		this.lexer = null;
		this.input = null;
		this.inputStream = null;
		this.inputSqlFilename = null;
		this.tokens = null;
		this.parser = null;
		this.tree = null;
		this.eval = null;
	}

	//Here create all the new objects and beging the visits
	public cMySQLTableDefinitionController() throws FileNotFoundException, IOException
	{
		//this.inputStream = cMySQLTableDefinitionController.class.getResourceAsStream(this.inputSqlFilename);
		this(cMySQLTableDefinitionController.class.getResourceAsStream(cMySQLTableDefinitionController.DEFAULT_TABLE_DEFINITION_SQL_FILE));
	}

	public cMySQLTableDefinitionController(InputStream inStream) throws FileNotFoundException, IOException
	{
		if (inStream == null)
			throw new NullPointerException("cMySQLTableDefinitionController: Empty stream not allowed.");
		this.inputSqlFilename = null;
		this.inputStream = inStream;
		this.input = new ANTLRInputStream(this.inputStream);
		this.lexer = new TableDefinitionLexer(input);

		this.tokens = new CommonTokenStream(this.lexer);
		this.parser = new TableDefinitionParser(tokens);
		this.tree = parser.prog(); // parse

		this.eval = new cTableDefinitionVisitor();
	}

	public Set<Table> getTableSet()
	{
		return this.eval.visit(this.tree);
	}

	public static void main(String[] args) throws Exception
	{
		String inputFile = "C:\\Users\\0400626\\workspace\\prince\\ETL_Wizard\\V1_00\\DatatrainUI\\src\\main\\resources\\tableCreationStatements.sql";
		if (args.length > 0)
			inputFile = args[0];
		InputStream is = new FileInputStream(inputFile);
		cMySQLTableDefinitionController controller = new cMySQLTableDefinitionController(is);
		controller.eval.visit(controller.tree);
	} //make new file in generated output, make new class that drives like in calc, add the grammar into this same folder, cd into folder and compile then run testrig
}
