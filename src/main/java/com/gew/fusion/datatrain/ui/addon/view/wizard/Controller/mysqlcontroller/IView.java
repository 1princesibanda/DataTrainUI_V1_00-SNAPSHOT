/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gew.fusion.datatrain.ui.addon.view.wizard.Controller.mysqlcontroller;

import java.util.List;
import java.util.Map;

/**
 *
 * @author 0400626
 */
public interface IView extends IControllee
{
	//public void onApplicationWrite(String database, String table); //when controller can support multiple databases

	//public void onApplicationWrite(String table);

	public void onApplicationWrite(String table, Map<String,List<String>> data);

	//public void onApplicationRead(String database, String table);
}
