/*
 * Copyright (c) 2019 GEW Technologies (GEW). All rights reserved.
 */
package com.gew.fusion.datatrain.ui.addon.view.dashboard;

import com.gew.fusion.datatrain.ui.addon.cFXDataTrainUIManager;
import com.gew.fusion.datatrain.ui.addon.data.cEtlMapItemSummary;
import java.awt.Dimension;
import java.util.List;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.embed.swing.JFXPanel;
import javafx.scene.Group;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.MenuItem;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.paint.Color;

/**
 *
 * @author 0400553
 */
public class cSourceDestinationCountView
{
  private final ObservableList<cEtlMapItemSummary> viewData = FXCollections.observableArrayList();
  
  
  private final int defaultWidth;
  private final int defaultHeight;
  
    /**
   * The parent pane
   */
  public BorderPane m_oParent = new BorderPane();
  /**
   * The JFX panel (not needed if the parent is FX).
   */
  private JFXPanel m_oPanel;
  
  
  private TableView<cEtlMapItemSummary> tableView = new TableView();
  private final cFXDataTrainUIManager masterUiManager;
  private ContextMenu m_oContextMenu;
  private MenuItem m_oMenuItemScreenCapture;
  private MenuItem m_oMenuItemSendToReport;
  
  
  
  
  public cSourceDestinationCountView(cFXDataTrainUIManager masterUiManager, int p_iDefaultWidth, int p_iDefaultHeight)
  {
     this.masterUiManager = masterUiManager;
     this.defaultWidth = p_iDefaultWidth;
     this.defaultHeight = p_iDefaultHeight;
     initUI();
  }
  
  
    /**
   * Initializes the view.
   */
  private void initUI()
  {
    final Scene oScene = new Scene(new Group());
    Group oSceneRoot = (Group) oScene.getRoot();
    

    m_oParent = setupPane(oScene);
    oSceneRoot.getChildren().add(m_oParent);
    m_oPanel = new JFXPanel();
    m_oPanel.setPreferredSize(new Dimension(defaultWidth, defaultHeight));
    m_oPanel.setScene(oScene);
    createContextMenu();
    tableView.addEventHandler(MouseEvent.MOUSE_PRESSED, event ->
    {
      if (event.isSecondaryButtonDown())
      {
        m_oContextMenu.show( m_oParent, event.getScreenX(), event.getScreenY());
      }
      else
      {
        m_oContextMenu.hide();
      }
    });
  }
  
  private void createContextMenu()
  {
    // create a menu 
    m_oContextMenu = new ContextMenu();

    // create menuitems 
    m_oMenuItemScreenCapture = new MenuItem("Screen Capture");
    m_oMenuItemSendToReport = new MenuItem("Send To Report");

    // create action handlers
    m_oMenuItemScreenCapture.setOnAction((event) ->
    {
      masterUiManager.getScreenCapture().capture(m_oParent);
    });
    m_oMenuItemSendToReport.setOnAction((event) ->
    {
      masterUiManager.getScreenCapture().capture(m_oParent);
    });

    // add menu items to menu 
    //m_oContextMenu.getItems().add(m_oMenuItemScreenCapture);
    m_oContextMenu.getItems().add(m_oMenuItemSendToReport);
    
  }
  

  public void reload()
  {
    Platform.runLater(() ->tableView.refresh());
  }

  protected BorderPane setupPane(Scene p_oScene)
  {
    
    m_oParent.setPrefSize(defaultWidth, defaultHeight);
    
    //Table
    TableColumn columnMapName = new TableColumn("ETL Map Name");
    columnMapName.setMinWidth(200);
    columnMapName.setCellValueFactory(new PropertyValueFactory<>("mapName"));
    
    TableColumn<cEtlMapItemSummary, Number> columnSourceCount = new TableColumn("Source Count");
    columnSourceCount.setMinWidth(100);
    columnSourceCount.setCellValueFactory(cellData ->cellData.getValue().getSourceDataProperty());
    
    
    TableColumn<cEtlMapItemSummary, Number> columnDestinationCount = new TableColumn("Destination Count");
    columnDestinationCount.setMinWidth(120);
    columnDestinationCount.setCellValueFactory(cellData ->cellData.getValue().getDestinationDataProperty());
    
    
    TableColumn<cEtlMapItemSummary,Number> columnOffset = new TableColumn("Variance");
    columnOffset.setMinWidth(80);
    columnOffset.setCellValueFactory(cellData ->cellData.getValue().getVarianceDataProperty());
    //Customer cell factory to set colours
    columnOffset.setCellFactory( column ->
    {
      return new TableCell<cEtlMapItemSummary,Number>()
      {
        @Override
        protected void updateItem(Number p_variance, boolean p_empty)
        {
          super.updateItem(p_variance, p_empty);
          
          if(p_variance ==null || p_empty)
          {
            setText(null);
            setStyle("");
            return;
          }
          
          setText(p_variance.toString());
          //Set the cell background if based on the variance value
          if(p_variance.longValue() <0)
            setTextFill(Color.RED);
          else if (p_variance.longValue() ==0)
            setTextFill(Color.WHITE);
          else if (p_variance.longValue()>0)
            setTextFill(Color.DARKGREEN);
        }
      };
    });
    
    
    tableView.getColumns().addAll(columnMapName,columnSourceCount,columnDestinationCount,columnOffset);
    tableView.setItems(viewData);
    
    m_oParent.setCenter(tableView);
    
    return m_oParent;
  }

  public Node getNode()
  {
    return m_oParent;
  }
  
  /**
   * @return The new of the view
   */
  public String getViewName()
  {
    return "source_destination_count_view";
  }
  
  /**
   * Add or update a row in the table.
   * 
   * @param record Value to add/update
   */
  public void addOrUpdate(cEtlMapItemSummary record)
  {
    viewData.add(record);
  }
  
  /**
   * Remove the the table
   * 
   * @param record record to remove
   */
  public void remove(cEtlMapItemSummary record)
  {
    viewData.remove(record);
  }
  
  /**
   * List of current data in the table
   * @return 
   */
  public List<cEtlMapItemSummary> getViewData()
  {
    return viewData;
  }
 
}
