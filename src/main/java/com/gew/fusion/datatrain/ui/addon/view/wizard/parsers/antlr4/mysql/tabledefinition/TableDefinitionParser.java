// Generated from ..\..\prince\ETL_Wizard\V1_00\DatatrainUI\src\main\java\com\gew\datatrain\grammars\antlr4\TableDefinition.g4 by ANTLR 4.7.2
package com.gew.fusion.datatrain.ui.addon.view.wizard.parsers.antlr4.mysql.tabledefinition;
import org.antlr.v4.runtime.atn.*;
import org.antlr.v4.runtime.dfa.DFA;
import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.misc.*;
import org.antlr.v4.runtime.tree.*;
import java.util.List;
import java.util.Iterator;
import java.util.ArrayList;

@SuppressWarnings({"all", "warnings", "unchecked", "unused", "cast"})
public class TableDefinitionParser extends Parser {
	static { RuntimeMetaData.checkVersion("4.7.2", RuntimeMetaData.VERSION); }

	protected static final DFA[] _decisionToDFA;
	protected static final PredictionContextCache _sharedContextCache =
		new PredictionContextCache();
	public static final int
		T__0=1, STRING_LITERAL=2, NUMERIC_LITERAL=3, DATE_AND_TIME_LITERAL=4, 
		HEXADECIMAL_LITERAL=5, BITVALUE_LITERAL=6, BOOLEAN_LITERAL=7, ODBC_date=8, 
		ODBC_time=9, ODBC_timestamp=10, MYINT=11, IF=12, NOT=13, EXISTS=14, TRUE=15, 
		FALSE=16, PRIMARY=17, KEY=18, CREATE=19, TABLE=20, COLON=21, COMMA=22, 
		VARCHAR=23, CLOSINGROUNDBRACE=24, OPENINGROUNDBRACE=25, TINYINT=26, SMALLINT=27, 
		YEAR=28, INT_=29, BIGINT=30, UNSIGNED=31, FLOAT=32, DOUBLE=33, NUMERIC=34, 
		DECIMAL=35, DATETIME=36, TIMESTAMP=37, TIME=38, DATE=39, BIT=40, BOOLEAN=41, 
		ENGINE=42, INNODB=43, AUTO_INCREMENT=44, NULL=45, DEFAULT=46, ID=47, INT=48, 
		WS=49, COMMENT=50;
	public static final int
		RULE_prog = 0, RULE_line = 1, RULE_columnDefinitionList = 2, RULE_columnDefinition = 3, 
		RULE_columnNameList = 4, RULE_sqlDataType = 5, RULE_directType = 6, RULE_precisionType = 7, 
		RULE_tableName = 8, RULE_comment = 9, RULE_ifnotexists = 10, RULE_literal = 11;
	private static String[] makeRuleNames() {
		return new String[] {
			"prog", "line", "columnDefinitionList", "columnDefinition", "columnNameList", 
			"sqlDataType", "directType", "precisionType", "tableName", "comment", 
			"ifnotexists", "literal"
		};
	}
	public static final String[] ruleNames = makeRuleNames();

	private static String[] makeLiteralNames() {
		return new String[] {
			null, "'='", null, null, null, null, null, null, null, null, null, null, 
			null, null, null, null, null, null, null, null, null, "';'", "','", null, 
			"')'", "'('", null, "'SMALLINT'"
		};
	}
	private static final String[] _LITERAL_NAMES = makeLiteralNames();
	private static String[] makeSymbolicNames() {
		return new String[] {
			null, null, "STRING_LITERAL", "NUMERIC_LITERAL", "DATE_AND_TIME_LITERAL", 
			"HEXADECIMAL_LITERAL", "BITVALUE_LITERAL", "BOOLEAN_LITERAL", "ODBC_date", 
			"ODBC_time", "ODBC_timestamp", "MYINT", "IF", "NOT", "EXISTS", "TRUE", 
			"FALSE", "PRIMARY", "KEY", "CREATE", "TABLE", "COLON", "COMMA", "VARCHAR", 
			"CLOSINGROUNDBRACE", "OPENINGROUNDBRACE", "TINYINT", "SMALLINT", "YEAR", 
			"INT_", "BIGINT", "UNSIGNED", "FLOAT", "DOUBLE", "NUMERIC", "DECIMAL", 
			"DATETIME", "TIMESTAMP", "TIME", "DATE", "BIT", "BOOLEAN", "ENGINE", 
			"INNODB", "AUTO_INCREMENT", "NULL", "DEFAULT", "ID", "INT", "WS", "COMMENT"
		};
	}
	private static final String[] _SYMBOLIC_NAMES = makeSymbolicNames();
	public static final Vocabulary VOCABULARY = new VocabularyImpl(_LITERAL_NAMES, _SYMBOLIC_NAMES);

	/**
	 * @deprecated Use {@link #VOCABULARY} instead.
	 */
	@Deprecated
	public static final String[] tokenNames;
	static {
		tokenNames = new String[_SYMBOLIC_NAMES.length];
		for (int i = 0; i < tokenNames.length; i++) {
			tokenNames[i] = VOCABULARY.getLiteralName(i);
			if (tokenNames[i] == null) {
				tokenNames[i] = VOCABULARY.getSymbolicName(i);
			}

			if (tokenNames[i] == null) {
				tokenNames[i] = "<INVALID>";
			}
		}
	}

	@Override
	@Deprecated
	public String[] getTokenNames() {
		return tokenNames;
	}

	@Override

	public Vocabulary getVocabulary() {
		return VOCABULARY;
	}

	@Override
	public String getGrammarFileName() { return "TableDefinition.g4"; }

	@Override
	public String[] getRuleNames() { return ruleNames; }

	@Override
	public String getSerializedATN() { return _serializedATN; }

	@Override
	public ATN getATN() { return _ATN; }

	public TableDefinitionParser(TokenStream input) {
		super(input);
		_interp = new ParserATNSimulator(this,_ATN,_decisionToDFA,_sharedContextCache);
	}

	public static class ProgContext extends ParserRuleContext {
		public ProgContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_prog; }
	 
		public ProgContext() { }
		public void copyFrom(ProgContext ctx) {
			super.copyFrom(ctx);
		}
	}
	public static class AllTablesContext extends ProgContext {
		public List<LineContext> line() {
			return getRuleContexts(LineContext.class);
		}
		public LineContext line(int i) {
			return getRuleContext(LineContext.class,i);
		}
		public AllTablesContext(ProgContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof TableDefinitionVisitor ) return ((TableDefinitionVisitor<? extends T>)visitor).visitAllTables(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ProgContext prog() throws RecognitionException {
		ProgContext _localctx = new ProgContext(_ctx, getState());
		enterRule(_localctx, 0, RULE_prog);
		int _la;
		try {
			_localctx = new AllTablesContext(_localctx);
			enterOuterAlt(_localctx, 1);
			{
			setState(25); 
			_errHandler.sync(this);
			_la = _input.LA(1);
			do {
				{
				{
				setState(24);
				line();
				}
				}
				setState(27); 
				_errHandler.sync(this);
				_la = _input.LA(1);
			} while ( _la==CREATE || _la==COMMENT );
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class LineContext extends ParserRuleContext {
		public LineContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_line; }
	 
		public LineContext() { }
		public void copyFrom(LineContext ctx) {
			super.copyFrom(ctx);
		}
	}
	public static class CommentsContext extends LineContext {
		public List<TerminalNode> COMMENT() { return getTokens(TableDefinitionParser.COMMENT); }
		public TerminalNode COMMENT(int i) {
			return getToken(TableDefinitionParser.COMMENT, i);
		}
		public CommentsContext(LineContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof TableDefinitionVisitor ) return ((TableDefinitionVisitor<? extends T>)visitor).visitComments(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class TableDefinitionContext extends LineContext {
		public TerminalNode CREATE() { return getToken(TableDefinitionParser.CREATE, 0); }
		public TerminalNode TABLE() { return getToken(TableDefinitionParser.TABLE, 0); }
		public TableNameContext tableName() {
			return getRuleContext(TableNameContext.class,0);
		}
		public TerminalNode OPENINGROUNDBRACE() { return getToken(TableDefinitionParser.OPENINGROUNDBRACE, 0); }
		public ColumnDefinitionListContext columnDefinitionList() {
			return getRuleContext(ColumnDefinitionListContext.class,0);
		}
		public TerminalNode CLOSINGROUNDBRACE() { return getToken(TableDefinitionParser.CLOSINGROUNDBRACE, 0); }
		public TerminalNode COLON() { return getToken(TableDefinitionParser.COLON, 0); }
		public IfnotexistsContext ifnotexists() {
			return getRuleContext(IfnotexistsContext.class,0);
		}
		public TerminalNode ENGINE() { return getToken(TableDefinitionParser.ENGINE, 0); }
		public TerminalNode INNODB() { return getToken(TableDefinitionParser.INNODB, 0); }
		public TableDefinitionContext(LineContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof TableDefinitionVisitor ) return ((TableDefinitionVisitor<? extends T>)visitor).visitTableDefinition(this);
			else return visitor.visitChildren(this);
		}
	}

	public final LineContext line() throws RecognitionException {
		LineContext _localctx = new LineContext(_ctx, getState());
		enterRule(_localctx, 2, RULE_line);
		int _la;
		try {
			int _alt;
			setState(50);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case CREATE:
				_localctx = new TableDefinitionContext(_localctx);
				enterOuterAlt(_localctx, 1);
				{
				setState(29);
				match(CREATE);
				setState(30);
				match(TABLE);
				setState(32);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if (_la==IF) {
					{
					setState(31);
					ifnotexists();
					}
				}

				setState(34);
				tableName();
				setState(35);
				match(OPENINGROUNDBRACE);
				setState(36);
				columnDefinitionList();
				setState(37);
				match(CLOSINGROUNDBRACE);
				setState(41);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if (_la==ENGINE) {
					{
					setState(38);
					match(ENGINE);
					setState(39);
					match(T__0);
					setState(40);
					match(INNODB);
					}
				}

				setState(43);
				match(COLON);
				}
				break;
			case COMMENT:
				_localctx = new CommentsContext(_localctx);
				enterOuterAlt(_localctx, 2);
				{
				setState(46); 
				_errHandler.sync(this);
				_alt = 1;
				do {
					switch (_alt) {
					case 1:
						{
						{
						setState(45);
						match(COMMENT);
						}
						}
						break;
					default:
						throw new NoViableAltException(this);
					}
					setState(48); 
					_errHandler.sync(this);
					_alt = getInterpreter().adaptivePredict(_input,3,_ctx);
				} while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER );
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ColumnDefinitionListContext extends ParserRuleContext {
		public ColumnDefinitionListContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_columnDefinitionList; }
	 
		public ColumnDefinitionListContext() { }
		public void copyFrom(ColumnDefinitionListContext ctx) {
			super.copyFrom(ctx);
		}
	}
	public static class PrimaryKeyListingContext extends ColumnDefinitionListContext {
		public TerminalNode PRIMARY() { return getToken(TableDefinitionParser.PRIMARY, 0); }
		public TerminalNode KEY() { return getToken(TableDefinitionParser.KEY, 0); }
		public TerminalNode OPENINGROUNDBRACE() { return getToken(TableDefinitionParser.OPENINGROUNDBRACE, 0); }
		public ColumnNameListContext columnNameList() {
			return getRuleContext(ColumnNameListContext.class,0);
		}
		public TerminalNode CLOSINGROUNDBRACE() { return getToken(TableDefinitionParser.CLOSINGROUNDBRACE, 0); }
		public PrimaryKeyListingContext(ColumnDefinitionListContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof TableDefinitionVisitor ) return ((TableDefinitionVisitor<? extends T>)visitor).visitPrimaryKeyListing(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class ColumnDefListContext extends ColumnDefinitionListContext {
		public ColumnDefinitionContext columnDefinition() {
			return getRuleContext(ColumnDefinitionContext.class,0);
		}
		public TerminalNode COMMA() { return getToken(TableDefinitionParser.COMMA, 0); }
		public ColumnDefinitionListContext columnDefinitionList() {
			return getRuleContext(ColumnDefinitionListContext.class,0);
		}
		public ColumnDefListContext(ColumnDefinitionListContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof TableDefinitionVisitor ) return ((TableDefinitionVisitor<? extends T>)visitor).visitColumnDefList(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class ColumnDefContext extends ColumnDefinitionListContext {
		public ColumnDefinitionContext columnDefinition() {
			return getRuleContext(ColumnDefinitionContext.class,0);
		}
		public ColumnDefContext(ColumnDefinitionListContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof TableDefinitionVisitor ) return ((TableDefinitionVisitor<? extends T>)visitor).visitColumnDef(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ColumnDefinitionListContext columnDefinitionList() throws RecognitionException {
		ColumnDefinitionListContext _localctx = new ColumnDefinitionListContext(_ctx, getState());
		enterRule(_localctx, 4, RULE_columnDefinitionList);
		try {
			setState(63);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,5,_ctx) ) {
			case 1:
				_localctx = new ColumnDefContext(_localctx);
				enterOuterAlt(_localctx, 1);
				{
				setState(52);
				columnDefinition();
				}
				break;
			case 2:
				_localctx = new PrimaryKeyListingContext(_localctx);
				enterOuterAlt(_localctx, 2);
				{
				setState(53);
				match(PRIMARY);
				setState(54);
				match(KEY);
				setState(55);
				match(OPENINGROUNDBRACE);
				setState(56);
				columnNameList();
				setState(57);
				match(CLOSINGROUNDBRACE);
				}
				break;
			case 3:
				_localctx = new ColumnDefListContext(_localctx);
				enterOuterAlt(_localctx, 3);
				{
				setState(59);
				columnDefinition();
				setState(60);
				match(COMMA);
				setState(61);
				columnDefinitionList();
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ColumnDefinitionContext extends ParserRuleContext {
		public TerminalNode ID() { return getToken(TableDefinitionParser.ID, 0); }
		public SqlDataTypeContext sqlDataType() {
			return getRuleContext(SqlDataTypeContext.class,0);
		}
		public TerminalNode AUTO_INCREMENT() { return getToken(TableDefinitionParser.AUTO_INCREMENT, 0); }
		public TerminalNode NOT() { return getToken(TableDefinitionParser.NOT, 0); }
		public TerminalNode NULL() { return getToken(TableDefinitionParser.NULL, 0); }
		public TerminalNode DEFAULT() { return getToken(TableDefinitionParser.DEFAULT, 0); }
		public LiteralContext literal() {
			return getRuleContext(LiteralContext.class,0);
		}
		public ColumnDefinitionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_columnDefinition; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof TableDefinitionVisitor ) return ((TableDefinitionVisitor<? extends T>)visitor).visitColumnDefinition(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ColumnDefinitionContext columnDefinition() throws RecognitionException {
		ColumnDefinitionContext _localctx = new ColumnDefinitionContext(_ctx, getState());
		enterRule(_localctx, 6, RULE_columnDefinition);
		int _la;
		try {
			setState(81);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,8,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(65);
				match(ID);
				setState(66);
				sqlDataType();
				setState(68);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if (_la==AUTO_INCREMENT) {
					{
					setState(67);
					match(AUTO_INCREMENT);
					}
				}

				setState(72);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if (_la==NOT) {
					{
					setState(70);
					match(NOT);
					setState(71);
					match(NULL);
					}
				}

				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(74);
				match(ID);
				setState(75);
				sqlDataType();
				}
				break;
			case 3:
				enterOuterAlt(_localctx, 3);
				{
				setState(76);
				match(ID);
				setState(77);
				sqlDataType();
				setState(78);
				match(DEFAULT);
				setState(79);
				literal();
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ColumnNameListContext extends ParserRuleContext {
		public ColumnNameListContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_columnNameList; }
	 
		public ColumnNameListContext() { }
		public void copyFrom(ColumnNameListContext ctx) {
			super.copyFrom(ctx);
		}
	}
	public static class PrimaryKeyColumnListContext extends ColumnNameListContext {
		public TerminalNode ID() { return getToken(TableDefinitionParser.ID, 0); }
		public TerminalNode COMMA() { return getToken(TableDefinitionParser.COMMA, 0); }
		public ColumnNameListContext columnNameList() {
			return getRuleContext(ColumnNameListContext.class,0);
		}
		public PrimaryKeyColumnListContext(ColumnNameListContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof TableDefinitionVisitor ) return ((TableDefinitionVisitor<? extends T>)visitor).visitPrimaryKeyColumnList(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class PrimaryKeyColumnContext extends ColumnNameListContext {
		public TerminalNode ID() { return getToken(TableDefinitionParser.ID, 0); }
		public PrimaryKeyColumnContext(ColumnNameListContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof TableDefinitionVisitor ) return ((TableDefinitionVisitor<? extends T>)visitor).visitPrimaryKeyColumn(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ColumnNameListContext columnNameList() throws RecognitionException {
		ColumnNameListContext _localctx = new ColumnNameListContext(_ctx, getState());
		enterRule(_localctx, 8, RULE_columnNameList);
		try {
			setState(87);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,9,_ctx) ) {
			case 1:
				_localctx = new PrimaryKeyColumnContext(_localctx);
				enterOuterAlt(_localctx, 1);
				{
				setState(83);
				match(ID);
				}
				break;
			case 2:
				_localctx = new PrimaryKeyColumnListContext(_localctx);
				enterOuterAlt(_localctx, 2);
				{
				setState(84);
				match(ID);
				setState(85);
				match(COMMA);
				setState(86);
				columnNameList();
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class SqlDataTypeContext extends ParserRuleContext {
		public DirectTypeContext directType() {
			return getRuleContext(DirectTypeContext.class,0);
		}
		public PrecisionTypeContext precisionType() {
			return getRuleContext(PrecisionTypeContext.class,0);
		}
		public SqlDataTypeContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_sqlDataType; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof TableDefinitionVisitor ) return ((TableDefinitionVisitor<? extends T>)visitor).visitSqlDataType(this);
			else return visitor.visitChildren(this);
		}
	}

	public final SqlDataTypeContext sqlDataType() throws RecognitionException {
		SqlDataTypeContext _localctx = new SqlDataTypeContext(_ctx, getState());
		enterRule(_localctx, 10, RULE_sqlDataType);
		try {
			setState(91);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case TINYINT:
			case SMALLINT:
			case YEAR:
			case INT_:
			case BIGINT:
			case UNSIGNED:
			case FLOAT:
			case DOUBLE:
			case NUMERIC:
			case DECIMAL:
			case DATETIME:
			case TIMESTAMP:
			case TIME:
			case DATE:
			case BOOLEAN:
				enterOuterAlt(_localctx, 1);
				{
				setState(89);
				directType();
				}
				break;
			case VARCHAR:
			case BIT:
				enterOuterAlt(_localctx, 2);
				{
				setState(90);
				precisionType();
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class DirectTypeContext extends ParserRuleContext {
		public TerminalNode TINYINT() { return getToken(TableDefinitionParser.TINYINT, 0); }
		public TerminalNode SMALLINT() { return getToken(TableDefinitionParser.SMALLINT, 0); }
		public TerminalNode YEAR() { return getToken(TableDefinitionParser.YEAR, 0); }
		public TerminalNode INT_() { return getToken(TableDefinitionParser.INT_, 0); }
		public TerminalNode BIGINT() { return getToken(TableDefinitionParser.BIGINT, 0); }
		public TerminalNode UNSIGNED() { return getToken(TableDefinitionParser.UNSIGNED, 0); }
		public TerminalNode FLOAT() { return getToken(TableDefinitionParser.FLOAT, 0); }
		public TerminalNode DOUBLE() { return getToken(TableDefinitionParser.DOUBLE, 0); }
		public TerminalNode NUMERIC() { return getToken(TableDefinitionParser.NUMERIC, 0); }
		public TerminalNode DECIMAL() { return getToken(TableDefinitionParser.DECIMAL, 0); }
		public TerminalNode DATETIME() { return getToken(TableDefinitionParser.DATETIME, 0); }
		public TerminalNode TIMESTAMP() { return getToken(TableDefinitionParser.TIMESTAMP, 0); }
		public TerminalNode TIME() { return getToken(TableDefinitionParser.TIME, 0); }
		public TerminalNode DATE() { return getToken(TableDefinitionParser.DATE, 0); }
		public TerminalNode BOOLEAN() { return getToken(TableDefinitionParser.BOOLEAN, 0); }
		public DirectTypeContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_directType; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof TableDefinitionVisitor ) return ((TableDefinitionVisitor<? extends T>)visitor).visitDirectType(this);
			else return visitor.visitChildren(this);
		}
	}

	public final DirectTypeContext directType() throws RecognitionException {
		DirectTypeContext _localctx = new DirectTypeContext(_ctx, getState());
		enterRule(_localctx, 12, RULE_directType);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(93);
			_la = _input.LA(1);
			if ( !((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << TINYINT) | (1L << SMALLINT) | (1L << YEAR) | (1L << INT_) | (1L << BIGINT) | (1L << UNSIGNED) | (1L << FLOAT) | (1L << DOUBLE) | (1L << NUMERIC) | (1L << DECIMAL) | (1L << DATETIME) | (1L << TIMESTAMP) | (1L << TIME) | (1L << DATE) | (1L << BOOLEAN))) != 0)) ) {
			_errHandler.recoverInline(this);
			}
			else {
				if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
				_errHandler.reportMatch(this);
				consume();
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class PrecisionTypeContext extends ParserRuleContext {
		public TerminalNode BIT() { return getToken(TableDefinitionParser.BIT, 0); }
		public TerminalNode OPENINGROUNDBRACE() { return getToken(TableDefinitionParser.OPENINGROUNDBRACE, 0); }
		public TerminalNode INT() { return getToken(TableDefinitionParser.INT, 0); }
		public TerminalNode CLOSINGROUNDBRACE() { return getToken(TableDefinitionParser.CLOSINGROUNDBRACE, 0); }
		public TerminalNode VARCHAR() { return getToken(TableDefinitionParser.VARCHAR, 0); }
		public TerminalNode NUMERIC_LITERAL() { return getToken(TableDefinitionParser.NUMERIC_LITERAL, 0); }
		public PrecisionTypeContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_precisionType; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof TableDefinitionVisitor ) return ((TableDefinitionVisitor<? extends T>)visitor).visitPrecisionType(this);
			else return visitor.visitChildren(this);
		}
	}

	public final PrecisionTypeContext precisionType() throws RecognitionException {
		PrecisionTypeContext _localctx = new PrecisionTypeContext(_ctx, getState());
		enterRule(_localctx, 14, RULE_precisionType);
		try {
			setState(103);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case BIT:
				enterOuterAlt(_localctx, 1);
				{
				setState(95);
				match(BIT);
				setState(96);
				match(OPENINGROUNDBRACE);
				setState(97);
				match(INT);
				setState(98);
				match(CLOSINGROUNDBRACE);
				}
				break;
			case VARCHAR:
				enterOuterAlt(_localctx, 2);
				{
				setState(99);
				match(VARCHAR);
				setState(100);
				match(OPENINGROUNDBRACE);
				setState(101);
				match(NUMERIC_LITERAL);
				setState(102);
				match(CLOSINGROUNDBRACE);
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class TableNameContext extends ParserRuleContext {
		public TerminalNode ID() { return getToken(TableDefinitionParser.ID, 0); }
		public TableNameContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_tableName; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof TableDefinitionVisitor ) return ((TableDefinitionVisitor<? extends T>)visitor).visitTableName(this);
			else return visitor.visitChildren(this);
		}
	}

	public final TableNameContext tableName() throws RecognitionException {
		TableNameContext _localctx = new TableNameContext(_ctx, getState());
		enterRule(_localctx, 16, RULE_tableName);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(105);
			match(ID);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class CommentContext extends ParserRuleContext {
		public TerminalNode COMMENT() { return getToken(TableDefinitionParser.COMMENT, 0); }
		public CommentContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_comment; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof TableDefinitionVisitor ) return ((TableDefinitionVisitor<? extends T>)visitor).visitComment(this);
			else return visitor.visitChildren(this);
		}
	}

	public final CommentContext comment() throws RecognitionException {
		CommentContext _localctx = new CommentContext(_ctx, getState());
		enterRule(_localctx, 18, RULE_comment);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(107);
			match(COMMENT);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class IfnotexistsContext extends ParserRuleContext {
		public TerminalNode IF() { return getToken(TableDefinitionParser.IF, 0); }
		public TerminalNode NOT() { return getToken(TableDefinitionParser.NOT, 0); }
		public TerminalNode EXISTS() { return getToken(TableDefinitionParser.EXISTS, 0); }
		public IfnotexistsContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_ifnotexists; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof TableDefinitionVisitor ) return ((TableDefinitionVisitor<? extends T>)visitor).visitIfnotexists(this);
			else return visitor.visitChildren(this);
		}
	}

	public final IfnotexistsContext ifnotexists() throws RecognitionException {
		IfnotexistsContext _localctx = new IfnotexistsContext(_ctx, getState());
		enterRule(_localctx, 20, RULE_ifnotexists);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(109);
			match(IF);
			setState(110);
			match(NOT);
			setState(111);
			match(EXISTS);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class LiteralContext extends ParserRuleContext {
		public TerminalNode STRING_LITERAL() { return getToken(TableDefinitionParser.STRING_LITERAL, 0); }
		public TerminalNode NUMERIC_LITERAL() { return getToken(TableDefinitionParser.NUMERIC_LITERAL, 0); }
		public TerminalNode DATE_AND_TIME_LITERAL() { return getToken(TableDefinitionParser.DATE_AND_TIME_LITERAL, 0); }
		public TerminalNode HEXADECIMAL_LITERAL() { return getToken(TableDefinitionParser.HEXADECIMAL_LITERAL, 0); }
		public TerminalNode BITVALUE_LITERAL() { return getToken(TableDefinitionParser.BITVALUE_LITERAL, 0); }
		public TerminalNode BOOLEAN_LITERAL() { return getToken(TableDefinitionParser.BOOLEAN_LITERAL, 0); }
		public TerminalNode NULL() { return getToken(TableDefinitionParser.NULL, 0); }
		public LiteralContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_literal; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof TableDefinitionVisitor ) return ((TableDefinitionVisitor<? extends T>)visitor).visitLiteral(this);
			else return visitor.visitChildren(this);
		}
	}

	public final LiteralContext literal() throws RecognitionException {
		LiteralContext _localctx = new LiteralContext(_ctx, getState());
		enterRule(_localctx, 22, RULE_literal);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(113);
			_la = _input.LA(1);
			if ( !((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << STRING_LITERAL) | (1L << NUMERIC_LITERAL) | (1L << DATE_AND_TIME_LITERAL) | (1L << HEXADECIMAL_LITERAL) | (1L << BITVALUE_LITERAL) | (1L << BOOLEAN_LITERAL) | (1L << NULL))) != 0)) ) {
			_errHandler.recoverInline(this);
			}
			else {
				if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
				_errHandler.reportMatch(this);
				consume();
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static final String _serializedATN =
		"\3\u608b\ua72a\u8133\ub9ed\u417c\u3be7\u7786\u5964\3\64v\4\2\t\2\4\3\t"+
		"\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7\t\7\4\b\t\b\4\t\t\t\4\n\t\n\4\13\t\13\4"+
		"\f\t\f\4\r\t\r\3\2\6\2\34\n\2\r\2\16\2\35\3\3\3\3\3\3\5\3#\n\3\3\3\3\3"+
		"\3\3\3\3\3\3\3\3\3\3\5\3,\n\3\3\3\3\3\3\3\6\3\61\n\3\r\3\16\3\62\5\3\65"+
		"\n\3\3\4\3\4\3\4\3\4\3\4\3\4\3\4\3\4\3\4\3\4\3\4\5\4B\n\4\3\5\3\5\3\5"+
		"\5\5G\n\5\3\5\3\5\5\5K\n\5\3\5\3\5\3\5\3\5\3\5\3\5\3\5\5\5T\n\5\3\6\3"+
		"\6\3\6\3\6\5\6Z\n\6\3\7\3\7\5\7^\n\7\3\b\3\b\3\t\3\t\3\t\3\t\3\t\3\t\3"+
		"\t\3\t\5\tj\n\t\3\n\3\n\3\13\3\13\3\f\3\f\3\f\3\f\3\r\3\r\3\r\2\2\16\2"+
		"\4\6\b\n\f\16\20\22\24\26\30\2\4\4\2\34)++\4\2\4\t//\2w\2\33\3\2\2\2\4"+
		"\64\3\2\2\2\6A\3\2\2\2\bS\3\2\2\2\nY\3\2\2\2\f]\3\2\2\2\16_\3\2\2\2\20"+
		"i\3\2\2\2\22k\3\2\2\2\24m\3\2\2\2\26o\3\2\2\2\30s\3\2\2\2\32\34\5\4\3"+
		"\2\33\32\3\2\2\2\34\35\3\2\2\2\35\33\3\2\2\2\35\36\3\2\2\2\36\3\3\2\2"+
		"\2\37 \7\25\2\2 \"\7\26\2\2!#\5\26\f\2\"!\3\2\2\2\"#\3\2\2\2#$\3\2\2\2"+
		"$%\5\22\n\2%&\7\33\2\2&\'\5\6\4\2\'+\7\32\2\2()\7,\2\2)*\7\3\2\2*,\7-"+
		"\2\2+(\3\2\2\2+,\3\2\2\2,-\3\2\2\2-.\7\27\2\2.\65\3\2\2\2/\61\7\64\2\2"+
		"\60/\3\2\2\2\61\62\3\2\2\2\62\60\3\2\2\2\62\63\3\2\2\2\63\65\3\2\2\2\64"+
		"\37\3\2\2\2\64\60\3\2\2\2\65\5\3\2\2\2\66B\5\b\5\2\678\7\23\2\289\7\24"+
		"\2\29:\7\33\2\2:;\5\n\6\2;<\7\32\2\2<B\3\2\2\2=>\5\b\5\2>?\7\30\2\2?@"+
		"\5\6\4\2@B\3\2\2\2A\66\3\2\2\2A\67\3\2\2\2A=\3\2\2\2B\7\3\2\2\2CD\7\61"+
		"\2\2DF\5\f\7\2EG\7.\2\2FE\3\2\2\2FG\3\2\2\2GJ\3\2\2\2HI\7\17\2\2IK\7/"+
		"\2\2JH\3\2\2\2JK\3\2\2\2KT\3\2\2\2LM\7\61\2\2MT\5\f\7\2NO\7\61\2\2OP\5"+
		"\f\7\2PQ\7\60\2\2QR\5\30\r\2RT\3\2\2\2SC\3\2\2\2SL\3\2\2\2SN\3\2\2\2T"+
		"\t\3\2\2\2UZ\7\61\2\2VW\7\61\2\2WX\7\30\2\2XZ\5\n\6\2YU\3\2\2\2YV\3\2"+
		"\2\2Z\13\3\2\2\2[^\5\16\b\2\\^\5\20\t\2][\3\2\2\2]\\\3\2\2\2^\r\3\2\2"+
		"\2_`\t\2\2\2`\17\3\2\2\2ab\7*\2\2bc\7\33\2\2cd\7\62\2\2dj\7\32\2\2ef\7"+
		"\31\2\2fg\7\33\2\2gh\7\5\2\2hj\7\32\2\2ia\3\2\2\2ie\3\2\2\2j\21\3\2\2"+
		"\2kl\7\61\2\2l\23\3\2\2\2mn\7\64\2\2n\25\3\2\2\2op\7\16\2\2pq\7\17\2\2"+
		"qr\7\20\2\2r\27\3\2\2\2st\t\3\2\2t\31\3\2\2\2\16\35\"+\62\64AFJSY]i";
	public static final ATN _ATN =
		new ATNDeserializer().deserialize(_serializedATN.toCharArray());
	static {
		_decisionToDFA = new DFA[_ATN.getNumberOfDecisions()];
		for (int i = 0; i < _ATN.getNumberOfDecisions(); i++) {
			_decisionToDFA[i] = new DFA(_ATN.getDecisionState(i), i);
		}
	}
}