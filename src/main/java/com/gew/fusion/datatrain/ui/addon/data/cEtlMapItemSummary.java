/*
 * Copyright (c) 2019 GEW Technologies (GEW). All rights reserved.
 */
package com.gew.fusion.datatrain.ui.addon.data;

import com.gew.persistence.IPersistenceStore;
import java.sql.Connection;
import javafx.beans.property.LongProperty;
import javafx.beans.property.SimpleLongProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/**
 * This POJO will contain the values that will be displayed in the table.
 * 
 * @author 0400553
 */
public class cEtlMapItemSummary
{
  private final LongProperty sourceDataCount=new SimpleLongProperty(0);
  private final LongProperty destinationDataCount=new SimpleLongProperty(0);
  private final LongProperty variance=new SimpleLongProperty(0);
  
  private final StringProperty mapName ;
  
  private final cMapSummaryConfig mapConfig;

  public cEtlMapItemSummary(cMapSummaryConfig p_mapConfig)
  {
    this.mapConfig = p_mapConfig;
    mapName = new SimpleStringProperty(p_mapConfig.getMapName());
  }

  /**
   * This will create a automatic mapConfig based on the map name provided.
   * 
   * @param mapName Map Name
   */
  public cEtlMapItemSummary(String mapName)
  {
    this(new  cMapSummaryConfig(mapName));
  }
  
  
  /**
   * @return Map Name
   */
  public String getMapName()
  {
    return mapName.get();
  }

  /**
   * Set the source data count
   * @param p_sourceDataCount count
   */
  public void setSourceDataCount(long p_sourceDataCount)
  {
    this.sourceDataCount.set(p_sourceDataCount);
    calculateVariance();
  }

  /**
   * Set the destination data count.
   * 
   * @param p_destinationDataCount count
   */
  public void setDestinationDataCount(long p_destinationDataCount)
  {
    this.destinationDataCount.set(p_destinationDataCount);
    calculateVariance();
  }
  
  /**
   * Calculates the variance between the source and destination count using positive logic.
   */
  private void calculateVariance()
  {
    variance.set(getDestinationDataCount()-getSourceDataCount());
  }

  /**
   * @return the source data count
   */
  public long getSourceDataCount()
  {
    return sourceDataCount.get();
  }
  
  public LongProperty getSourceDataProperty()
  {
    return sourceDataCount;
  }

  /**
   * @return The destination data count
   */
  public long getDestinationDataCount()
  {
    return destinationDataCount.get();
  }

  public long getVariance()
  {
    return variance.get();
  }
  
  public LongProperty getDestinationDataProperty()
  {
    return destinationDataCount;
  }
  
  public LongProperty getVarianceDataProperty()
  {
    return variance;
  }
  
  /**
   * Reload the count from the data.
   * 
   * @param sourceDbConnector source db connector.
   * @param destinationDbConnector destDbConnector.
   */
  public void reloadData(Connection sourceDbConnector,IPersistenceStore destinationDbConnector) throws Exception
  {
    setSourceDataCount(mapConfig.loadSourceCount(sourceDbConnector));
    setDestinationDataCount(mapConfig.loadDestinationCount(destinationDbConnector));
  }
  
  
  @Override
  public String toString()
  {
    return getClass().getSimpleName()+ "[ MapName:" + mapName
            +"]";
  }

  
  
}
