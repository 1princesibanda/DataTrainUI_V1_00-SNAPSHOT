/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gew.fusion.datatrain.ui.addon.view.wizard.Controller.mysqlcontroller;

import java.sql.ResultSet;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 *
 * @author 0400626
 */
public interface IController
{

	enum TYPE
	{
		APPLICATION,
		VIEW
	}

	//We only notify on writing, but not on reading because the DB acts as a buffer to keep all written data to be read any time and any number of times; reading is not interesting.
	//public void write(IControllee controlled, String databaseName, String tableName, String sql) throws Exception;
	//public void write(IControllee controlled, String tableName, String sql) throws Exception;

	//Write without building sql first in a Controllee object
	public void write(IControllee controlled, String tableName, List<String> columnsList, List<String> valuesList) throws Exception;
	
	//Write in one step multiple tables
	public void write(IControllee controlled, List<String> tableNameList, List<List<String>> columnsList, List<List<String>> valuesList) throws Exception;
	
/***********
	*Need a write that does not put down to the DB- ie bypass/skip actual write- maybe make it a controller config
	*Need a write that can run many writes in a transaction- maybe later if use-case develops
************/	
	
	//If you just want to write without notifying anybody
	//We dont provide this method because we want to use the DB as a buffer and applications can read whenever they desire to after notification of data write
	//public void write(String sql) throws Exception;
	//public void read(String databaseName, String tableName, Object... dataArgs);
	//public ResultSet read(String sql) throws Exception;

	public Map<String, List<String>> read(String tableName, List<String> columnsList) throws Exception;
	
	public Map<String, List<String>> read(String sql, List<String> tableName, List<String> schemaList, List<String> columnJavaTypes, List<String> columnsList) throws Exception;
	
	public String toStringColumns(String tableName, List<String> columnsList) throws Exception;
	
	public String toStringValues(String tableName, List<String> columnsList, List<String> valuesList) throws Exception;
	
	//public void subscribe(IControllee obj, IController.TYPE type) throws Exception;
	public void setMySQLCredentials(String host, String databaseName, String username, String password) throws Exception;

	public String getHost();

	public void setHost(String host);

	public String getDatabaseName();

	public void setDatabaseName(String databaseName);

	public String getUsername();

	public void setUsername(String username);

	public String getPassword();

	public void setPassword(String password);

	public Collection<IApplication> getApplications();

	public void setApplications(Collection<IApplication> applications);

	public Collection<IView> getViews();

	public void setViews(Collection<IView> views);

}
