/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gew.fusion.datatrain.ui.addon.view.wizard.com.gew.datatrain.ui;

import com.gew.fusion.datatrain.ui.addon.view.wizard.Controller.mysqlcontroller.IApplication;
import com.gew.fusion.datatrain.ui.addon.view.wizard.Controller.mysqlcontroller.IControllee;
import com.gew.fusion.datatrain.ui.addon.view.wizard.Controller.mysqlcontroller.IView;
import com.gew.fusion.datatrain.ui.addon.view.wizard.com.gew.datatrain.ui.cView3.Properties2;
import com.gew.fusion.datatrain.ui.addon.view.wizard.com.gew.datatrain.ui.cView4.Properties;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.Random;
import java.util.Set;

import javafx.application.Application;
import javafx.stage.Stage;

/**
 *
 * @author 0400626
 */
public abstract class IDataTrainView extends Application implements IView
{

	private static Hashtable<String, Boolean> map = new Hashtable<>();
	final private static Random random = new Random();
	private static String unique = "";
	protected IDataTrainView nextView;
	protected IDataTrainView previousView;
	protected Stage stage;
	protected Set<IApplication> applicationsSet;
	protected static Set<OutputMap> mapSet;
	protected static OutputMap runningMap;
	protected static Object paneManager;
	
	public IDataTrainView()
	{
		this.mapSet = new HashSet<>();
		this.runningMap = new OutputMap();
		IDataTrainView.paneManager = null;
	}

	//abstract public void start(Stage stage);
//We trust that the random.nextDouble() can produce more than enough numbers to uniquely allocate ids without running out/duplicates.
	//If this lets us down, we'll trust that the machine has enough memory for us to construct a sufficiently long string to uniquely id objects.
	public static String uniqueId()
	{
		unique = "" + random.nextDouble();
		Boolean used = map.put(unique, true);

		while (used != null)
		{
			unique += unique;
			used = map.put(unique, true);
		}
		return unique;
	}

	public void setNextView(IDataTrainView nextView)
	{
		this.nextView = nextView;
	}

	public IDataTrainView getNextView()
	{
		return this.nextView;
	}

	public void setPreviousView(IDataTrainView previousView)
	{
		this.previousView = previousView;
	}

	public IDataTrainView getPreviousView()
	{
		return this.previousView;
	}

	public void setStage(Stage stage)
	{
		this.stage = stage;
		if (this.stage == null)
			System.out.println("cView3: setStage(): Warning: object stage set to null!");
	}

	public OutputMap getMapByName(String mapName) throws Exception
	{
		Iterator<OutputMap> iter = IDataTrainView.mapSet.iterator();
		OutputMap outputMap;
		String name;

		while (iter.hasNext())
		{
			outputMap = iter.next();
			name = outputMap.getName();
			if (name.equals(mapName))
				return outputMap;
		}

		throw new Exception("No map with name '" + mapName + "' could be found.");
	}

	public static void setPaneManager(Object obj)
	{
		IDataTrainView.paneManager = obj;
	}
	
	abstract public void loadMap(OutputMap map);

	//public boolean equals()
}
