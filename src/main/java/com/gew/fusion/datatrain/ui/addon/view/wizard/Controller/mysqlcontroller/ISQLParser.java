/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gew.fusion.datatrain.ui.addon.view.wizard.Controller.mysqlcontroller;

import com.gew.fusion.datatrain.ui.addon.view.wizard.Controller.textfilecontroller.antlr4.cMySQLTableDefinitionController;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

/**
 *
 * @author 0400626 This class must walk the parse tree of the sql grammar and performa visits until all input sql is parsed. Parse sql and create all table entries so that children of this class know
 * about application tables to be read/written.
 */
public class ISQLParser
{

	private cMySQLTableDefinitionController tableDefinitionController;
	protected final Set<Table> tableSet;

	//Provide inputSqlFilename, a filename on disk containing sql statements to create tables
	public ISQLParser(String inputSqlFilename) throws IOException
	{
		this.tableDefinitionController = new cMySQLTableDefinitionController(new FileInputStream(inputSqlFilename));
		this.tableSet = this.tableDefinitionController.getTableSet();
	}

	//Create a table definition controller using the default file in jar resources. 
	//Here create all the new objects and beging the visits
	public ISQLParser() throws IOException
	{
		this.tableDefinitionController = new cMySQLTableDefinitionController();
		this.tableSet = this.tableDefinitionController.getTableSet();
	}

	//return a set of table names, never a null ;
	public Set<String> getTableNames()
	{
		//assert this.tableNameSet != null : "table name set assumed created in constructor so that it is never null here.";
		Set<String> tableNameSet = new HashSet<>();

		Iterator<Table> tablesIter;
		String tableName;
		Table table;

		tableNameSet.clear();

		tablesIter = this.tableSet.iterator();

		while (tablesIter.hasNext())
		{
			table = tablesIter.next();
			tableName = table.getName();
			assert !tableNameSet.contains(tableName) : "table names should be unique, yet found duplicate name '" + tableName + "'. Please correct table config.";
			tableNameSet.add(tableName);
		}

		return tableNameSet;
	}

	public Table getTableByName(String tableName) throws Exception
	{
		Iterator<Table> tablesIter;
		String tempName;
		Table table;


		tablesIter = this.tableSet.iterator();

		while (tablesIter.hasNext())
		{
			table = tablesIter.next();
			tempName = table.getName();
			if(tempName.equals(tableName))
				return table;
		}

		throw new Exception("No table with table name '"+tableName+"'. Please update config with table, or correct tableName.");
		//return table;
		
	}

	public Set<String> getColumnNames(String tableName) throws Exception
	{
		Set<String> tableNames = getTableNames();
		Set<String> columnNames = new HashSet<>();
		Set<Column> tableColumns;
		Iterator<Column> columnsIter;

		assert tableNames != null : "The getTableNames() should return an empty set of table names in the very least, never null.";

		if (!tableNames.contains(tableName))
			throw new Exception("Request for columns on unknown table name '" + tableName + "'. Please update config with table, or correct tableName.");

		Table table = getTableByName(tableName);
		
		tableColumns = table.columnSet;
		
		columnsIter = tableColumns.iterator();
		
		while(columnsIter.hasNext())
			columnNames.add(columnsIter.next().getName());
		
		return columnNames;
	}
	
	public Set<String> getAutoIncrementColumnNames(String tableName) throws Exception
	{
		Set<String> autoIncrementColumnNamesSet;
		Set<Column> autoIncrementColumns;
		Iterator<Column> autoIncrementColumnsIter;
		Table table;
		
		autoIncrementColumnNamesSet = new HashSet<>();
		
		table = getTableByName(tableName);
		assert table != null : "getTableByName() always returns a non null table, or exception if table of given name not found. ";
		autoIncrementColumns = table.getAutoIncrementColumns();
		autoIncrementColumnsIter = autoIncrementColumns.iterator();
		
		while(autoIncrementColumnsIter.hasNext())
			autoIncrementColumnNamesSet.add(autoIncrementColumnsIter.next().getName());
		
		return autoIncrementColumnNamesSet;
	}
	
	public Set<String> getPrimaryKeyColumnsNames(String tableName) throws Exception
	{
		Table table;
		
		table = getTableByName(tableName);
		
		return table.getPrimaryKeyColumNamesSet();
	}

}
