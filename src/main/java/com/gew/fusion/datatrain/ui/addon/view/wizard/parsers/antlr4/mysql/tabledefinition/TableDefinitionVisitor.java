// Generated from ..\..\prince\ETL_Wizard\V1_00\DatatrainUI\src\main\java\com\gew\datatrain\grammars\antlr4\TableDefinition.g4 by ANTLR 4.7.2
package com.gew.fusion.datatrain.ui.addon.view.wizard.parsers.antlr4.mysql.tabledefinition;
import org.antlr.v4.runtime.tree.ParseTreeVisitor;

/**
 * This interface defines a complete generic visitor for a parse tree produced
 * by {@link TableDefinitionParser}.
 *
 * @param <T> The return type of the visit operation. Use {@link Void} for
 * operations with no return type.
 */
public interface TableDefinitionVisitor<T> extends ParseTreeVisitor<T> {
	/**
	 * Visit a parse tree produced by the {@code allTables}
	 * labeled alternative in {@link TableDefinitionParser#prog}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitAllTables(TableDefinitionParser.AllTablesContext ctx);
	/**
	 * Visit a parse tree produced by the {@code tableDefinition}
	 * labeled alternative in {@link TableDefinitionParser#line}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitTableDefinition(TableDefinitionParser.TableDefinitionContext ctx);
	/**
	 * Visit a parse tree produced by the {@code comments}
	 * labeled alternative in {@link TableDefinitionParser#line}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitComments(TableDefinitionParser.CommentsContext ctx);
	/**
	 * Visit a parse tree produced by the {@code columnDef}
	 * labeled alternative in {@link TableDefinitionParser#columnDefinitionList}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitColumnDef(TableDefinitionParser.ColumnDefContext ctx);
	/**
	 * Visit a parse tree produced by the {@code primaryKeyListing}
	 * labeled alternative in {@link TableDefinitionParser#columnDefinitionList}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitPrimaryKeyListing(TableDefinitionParser.PrimaryKeyListingContext ctx);
	/**
	 * Visit a parse tree produced by the {@code columnDefList}
	 * labeled alternative in {@link TableDefinitionParser#columnDefinitionList}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitColumnDefList(TableDefinitionParser.ColumnDefListContext ctx);
	/**
	 * Visit a parse tree produced by {@link TableDefinitionParser#columnDefinition}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitColumnDefinition(TableDefinitionParser.ColumnDefinitionContext ctx);
	/**
	 * Visit a parse tree produced by the {@code primaryKeyColumn}
	 * labeled alternative in {@link TableDefinitionParser#columnNameList}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitPrimaryKeyColumn(TableDefinitionParser.PrimaryKeyColumnContext ctx);
	/**
	 * Visit a parse tree produced by the {@code primaryKeyColumnList}
	 * labeled alternative in {@link TableDefinitionParser#columnNameList}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitPrimaryKeyColumnList(TableDefinitionParser.PrimaryKeyColumnListContext ctx);
	/**
	 * Visit a parse tree produced by {@link TableDefinitionParser#sqlDataType}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitSqlDataType(TableDefinitionParser.SqlDataTypeContext ctx);
	/**
	 * Visit a parse tree produced by {@link TableDefinitionParser#directType}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitDirectType(TableDefinitionParser.DirectTypeContext ctx);
	/**
	 * Visit a parse tree produced by {@link TableDefinitionParser#precisionType}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitPrecisionType(TableDefinitionParser.PrecisionTypeContext ctx);
	/**
	 * Visit a parse tree produced by {@link TableDefinitionParser#tableName}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitTableName(TableDefinitionParser.TableNameContext ctx);
	/**
	 * Visit a parse tree produced by {@link TableDefinitionParser#comment}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitComment(TableDefinitionParser.CommentContext ctx);
	/**
	 * Visit a parse tree produced by {@link TableDefinitionParser#ifnotexists}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitIfnotexists(TableDefinitionParser.IfnotexistsContext ctx);
	/**
	 * Visit a parse tree produced by {@link TableDefinitionParser#literal}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitLiteral(TableDefinitionParser.LiteralContext ctx);
}