/**
 * Copyright (c) 2019 GEW Technologies (GEW). All rights reserved.
 */
package com.gew.fusion.datatrain.ui.addon.data;

import com.gew.ifs.ext.datatrain.lib.files.EFileOperationStatus;
import com.gew.lib.time.cTime;
import java.util.Comparator;

/**
 * Util.
 *
 * Various utility constructs.
 *
 * @author Philip M. Trenwith
 */
public class cUtil
{

  /**
   * String comparator using <code>String.CASE_INSENSITIVE_ORDER</code> but with null checks.
   */
  public static final Comparator<Object> g_oSTRING_COMPARATOR;
  /**
   * Integer comparator using <code>Integer.compare</code> but with null checks.
   */
  public static final Comparator<Object> g_oINTEGER_COMPARATOR;
  /**
   * Long comparator using <code>Long.compare</code> but with null checks.
   */
  public static final Comparator<Object> g_oLONG_COMPARATOR;
  /**
   * cTime comparator, with null checks.
   */
  public static final Comparator<Object> g_oTIME_COMPARATOR;
  /**
   * Status (EExportImportStatus) comparator using <code>EExportImportStatus.ordinal</code> but with null checks.
   */
  public static final Comparator<Object> g_oSTATUS_COMPARATOR;


  static
  {

    g_oSTRING_COMPARATOR = (o1, o2) ->
    {
      return o1 != null && o2 != null
             ? String.CASE_INSENSITIVE_ORDER.compare((String) o1, (String) o2)
             : 0;
    };

    g_oINTEGER_COMPARATOR = (o1, o2) ->
    {
      return o1 != null && o2 != null
             ? Integer.compare((Integer) o1, (Integer) o2)
             : 0;
    };
    
    g_oLONG_COMPARATOR = (o1, o2) ->
    {
      return o1 != null && o2 != null
             ? Long.compare((Long) o1, (Long) o2)
             : 0;
    };

    g_oTIME_COMPARATOR = (o1, o2) ->
    {
      return o1 != null && o2 != null
             ? ((cTime) o1).before((cTime) o2) ? -1
               : ((cTime) o1).equals((cTime) o2) ? 0 : 1
             : 0;
    };

    g_oSTATUS_COMPARATOR = (o1, o2) ->
    {
      return o1 != null && o2 != null
             ? ((EFileOperationStatus) o1).ordinal() < ((EFileOperationStatus) o2).ordinal() ? -1
               : ((EFileOperationStatus) o1).ordinal() == ((EFileOperationStatus) o2).ordinal() ? 0 : 1
             : 0;
    };
  }

  private cUtil()
  {
    throw new AssertionError("cUtil cannot be instantiated.");
  }

}
