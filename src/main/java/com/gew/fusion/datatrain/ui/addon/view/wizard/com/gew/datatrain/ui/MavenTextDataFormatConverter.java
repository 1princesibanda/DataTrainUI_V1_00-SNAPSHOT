
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gew.fusion.datatrain.ui.addon.view.wizard.com.gew.datatrain.ui;

import java.util.Optional;
import javafx.application.Application;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Bounds;
import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.geometry.VPos;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.control.TextInputDialog;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.Region;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontPosture;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.stage.Stage;

/**
 *
 * @author 0400626
 */
public class MavenTextDataFormatConverter extends Application
{

    private GridPane grid = null;
    ComboBox comboBox;
    Stage primaryStage = null;
    int sceneX = 1300, sceneY = 500;

    @Override
    public void start(Stage stage)
    {
        /*
        primaryStage = stage;
        stage.setTitle("Text Data Format Converter");
        stage.setMaxHeight(500);
        stage.setMaxWidth(1300);
        stage.setMinHeight(500);
        stage.setMinWidth(1300);
        stage.setFullScreen(false);
        stage.setResizable(false); //Toggles availability of Windows minimise and maximise. if set to false and setMaximized() is set, then it is fixed to the setting of setMaximized()
        stage.setMaximized(false);
        //stage.toFront(); Not really data- these are meant for use at runtime
        stage.setIconified(false); //Puts the window down at the bottom task bar where the open programs are. Takes precedence over toBack() and toFront();
        stage.setFullScreenExitHint("This is exit hint");
        stage.setAlwaysOnTop(false);

        //All must happen in a Scene, which must happen on a Stage. A Scene must be created with a root Parent- a Node that can have children. A Pane is such a Parent.
        Pane canvas = new Pane();
        Scene scene = new Scene(canvas, 200, 200);
        stage.setScene(scene);
        stage.show();  
        
        Text scenetitle = new Text("From");
        scenetitle.setFont(Font.font("Tahoma", FontWeight.NORMAL, FontPosture.REGULAR, 20));
        scenetitle.setX(100);
        scenetitle.setY(100);
        canvas.getChildren().add(scenetitle);

        ObservableList<String> options = FXCollections.observableArrayList("Oracle Database", "CSV File", "JSON File");
        //position combobox using Region's static void positionInArea() method, which uses the Node's set bounds(ie width and height) to position the Node in the Region's area.
        comboBox = new ComboBox(options);
        Bounds bounds = comboBox.getLayoutBounds();
        bounds.getWidth(); //get the bounds so that we can position comboBox using layout area that is just sufficient and tight.
        Region.positionInArea(comboBox, 100, 150, bounds.getWidth(), bounds.getHeight(), 0, Insets.EMPTY, HPos.LEFT, VPos.BASELINE, true);
        canvas.getChildren().add(comboBox);
        comboBox.setOnAction(new EventHandler<ActionEvent>()
        {
            @Override
            public void handle(ActionEvent e)
            {
                oracleDBconnectionGUI();
            }
        });

        Button btn = new Button("Next");
        btn.setDisable(true);
        bounds = btn.getLayoutBounds();
        Region.positionInArea(btn, 1000, 400, bounds.getWidth(), bounds.getHeight(), 0, Insets.EMPTY, HPos.LEFT, VPos.BASELINE, true);
        canvas.getChildren().add(btn);
        btn.setOnAction(new EventHandler<ActionEvent>()
        {

            @Override
            public void handle(ActionEvent e)
            {
                screen2();
            }
        });

        */
        primaryStage = stage;
        primaryStage.setTitle("Text Data Format Converter");
        grid = new GridPane();
        grid.setAlignment(Pos.CENTER);
        grid.setHgap(10);
        grid.setVgap(10);
        grid.setPadding(new Insets(25, 25, 25, 25));
        Text scenetitle = new Text("From");
        scenetitle.setFont(Font.font("Tahoma", FontWeight.NORMAL, 20));
        grid.add(scenetitle, 0, 0, 2, 1);

        ObservableList<String> options = FXCollections.observableArrayList(
                "Oracle Database",
                "CSV File",
                "JSON File"
        );
        comboBox = new ComboBox(options);
        grid.add(comboBox, 1, 1);
        comboBox.setOnAction(new EventHandler<ActionEvent>()
        {
            @Override
            public void handle(ActionEvent e)
            {
                oracleDBconnectionGUI();
            }
        });

        Button btn = new Button("Next");
        HBox hbBtn = new HBox(10);
        hbBtn.setAlignment(Pos.BOTTOM_RIGHT);
        hbBtn.getChildren().add(btn);
        grid.add(hbBtn, 10, 9);
        //grid.setGridLinesVisible(true);

        final Text actiontarget = new Text();
        grid.add(actiontarget, 1, 7);
        btn.setOnAction(new EventHandler<ActionEvent>()
        {

            @Override
            public void handle(ActionEvent e)
            {
                screen2();
            }
        });

        Scene scene = new Scene(grid, sceneX, sceneY);
        primaryStage.setScene(scene);
        primaryStage.show();

    }

    public void screen2()
    {
        primaryStage.setTitle("Text Data Format Converter");
        grid = new GridPane();
        grid.setAlignment(Pos.CENTER);
        grid.setHgap(10);
        grid.setVgap(10);
        grid.setPadding(new Insets(25, 25, 25, 25));
        Text scenetitle = new Text("Data selection SQL");
        scenetitle.setFont(Font.font("Tahoma", FontWeight.NORMAL, 20));
        grid.add(scenetitle, 0, 0, 2, 1);

        grid.add(new Label("TABLE NAMES"), 0, 1);
        ListView<String> tableNames = new ListView<String>();
        ObservableList<String> names = FXCollections.observableArrayList(
                "Single", "Double", "Suite", "Family App");
        tableNames.setItems(names);
        grid.add(tableNames, 0, 2);

        grid.add(new Label("COLUMN NAMES"), 1, 1);
        ListView<String> columnNames = new ListView<String>();
        ObservableList<String> columns = FXCollections.observableArrayList(
                "Single", "Double", "Suite", "Family App");
        columnNames.setItems(columns);
        grid.add(columnNames, 1, 2);

        Label host = new Label("Oracle SQL query:");
        grid.add(host, 3, 1);

        TextArea queryTextArea = new TextArea();
        queryTextArea.setText("#Enter Oracle Query here and test query with the test button");
        grid.add(queryTextArea, 3, 2);

        GridPane grid2 = new GridPane();
        grid2.setAlignment(Pos.CENTER);
        grid2.setHgap(10);
        grid2.setVgap(10);
        grid2.setPadding(new Insets(25, 25, 25, 25));
        Button testSQLBtn = new Button("Test Query");
        grid2.add(testSQLBtn, 0, 0);
        Button explainBtn = new Button("Explain");
        grid2.add(explainBtn, 1, 0);
        grid.add(grid2, 3, 3);

        Button btnNext = new Button("Next");
        grid.add(btnNext, 10, 9);

        btnNext.setOnAction(new EventHandler<ActionEvent>()
        {

            @Override
            public void handle(ActionEvent e)
            {
                screen3();
            }
        });

        Button backBtn = new Button("Back");
        grid.add(backBtn, 7, 9);
        backBtn.setOnAction(new EventHandler<ActionEvent>()
        {

            @Override
            public void handle(ActionEvent e)
            {
                start(primaryStage);
            }
        });

        Scene scene = new Scene(grid, sceneX, sceneY);
        primaryStage.setScene(scene);
        primaryStage.show();
    }

    public void screen3()
    {
        primaryStage.setTitle("Text Data Format Converter");
        grid = new GridPane();
        grid.setAlignment(Pos.CENTER);
        grid.setHgap(10);
        grid.setVgap(10);
        grid.setPadding(new Insets(25, 25, 25, 25));
        Text scenetitle = new Text("Properties");
        scenetitle.setFont(Font.font("Tahoma", FontWeight.NORMAL, 20));
        grid.add(scenetitle, 0, 0, 2, 1);

        grid.add(new Label("QUERY COLUMNS"), 0, 1);
        ListView<String> tableNames = new ListView<String>();
        ObservableList<String> names = FXCollections.observableArrayList(
                "RScolumn1", "RScolumn2", "RScolumn3", "RScolumn4");
        tableNames.setItems(names);
        grid.add(tableNames, 0, 2);

        grid.add(new Label("COLUMN DATA"), 1, 1);
        ListView<String> cols = new ListView<String>();
        ObservableList<String> data = FXCollections.observableArrayList("dataItem1", "dataItem2");
        cols.setItems(data);
        grid.add(cols, 1, 2);

        GridPane grid2 = new GridPane();
        grid2.setAlignment(Pos.CENTER);
        grid2.setHgap(10);
        grid2.setVgap(10);
        grid2.setPadding(new Insets(25, 25, 25, 25));

        Label transformLabel = new Label("Type");
        grid2.add(transformLabel, 0, 0);

        ObservableList<String> options = FXCollections.observableArrayList(
                "String",
                "Integer",
                "Char", "Boolean"
        );
        comboBox = new ComboBox(options);
        comboBox.setOnAction(new EventHandler<ActionEvent>()
        {
            @Override
            public void handle(ActionEvent e)
            {
                if (comboBox.getValue() == "XML File")
                {

                }
                else
                {
                    System.out.println(comboBox.getValue() + ": No GUI update action.");
                }
            }
        });
        grid2.add(comboBox, 0, 1);
        grid.add(grid2, 2, 2);

        CheckBox cb = new CheckBox("File reference?");
        grid2.add(cb, 0, 2);

        Button btn1 = new Button("Populate");
        grid2.add(btn1, 0, 3);
        btn1.setOnAction(
                new EventHandler<ActionEvent>()
        {

            @Override
            public void handle(ActionEvent e
            )
            {
                System.out.println("Accept Transformation.");
            }
        }
        );

        Button removeBtn = new Button("Remove");
        grid2.add(removeBtn, 0, 4);
        removeBtn.setOnAction(new EventHandler<ActionEvent>()
        {

            @Override
            public void handle(ActionEvent e
            )
            {
                System.out.println("Accept Transformation.");
            }
        }
        );

        grid.add(new Label("PROPERTIES"), 3, 1);
        TableView<Properties2> props = new TableView<Properties2>();
        props.setEditable(false);
        TableColumn column6 = new TableColumn("Column Name");
        TableColumn column7 = new TableColumn("fileRef");
        TableColumn column8 = new TableColumn("Type");
        ObservableList<Properties2> propsData = FXCollections.observableArrayList(new Properties2("RScolumn1", "TRUE", "STRING"), new Properties2("RScolumn1", "FALSE", "INTEGER"));
        column6.setCellValueFactory(new PropertyValueFactory<Properties2, String>("ColumnName"));
        column7.setCellValueFactory(new PropertyValueFactory<Properties2, String>("FileRef"));
        column8.setCellValueFactory(new PropertyValueFactory<Properties2, String>("Type"));

        props.setItems(propsData);
        props.getColumns().setAll(column6, column7, column8);
        grid.add(props, 3, 2);

        Button btnNext = new Button("Next");
        HBox hbBtn = new HBox(10);

        hbBtn.setAlignment(Pos.BOTTOM_RIGHT);

        hbBtn.getChildren().add(btnNext);
        grid.add(hbBtn, 10, 9);

        final Text actiontarget = new Text();

        grid.add(actiontarget,
                1, 7);
        btnNext.setOnAction(
                new EventHandler<ActionEvent>()
        {

            @Override
            public void handle(ActionEvent e
            )
            {
                actiontarget.setFill(Color.FIREBRICK);
                actiontarget.setText("Next button pressed");
                screen4();
            }
        }
        );

        Button backBtn = new Button("Back");
        HBox hbBtnBack = new HBox(10);

        hbBtnBack.setAlignment(Pos.BOTTOM_RIGHT);

        hbBtnBack.getChildren()
                .add(backBtn);
        grid.add(hbBtnBack,
                7, 9);

        final Text actiontargetBack = new Text();

        grid.add(actiontargetBack,
                1, 7);
        backBtn.setOnAction(
                new EventHandler<ActionEvent>()
        {

            @Override
            public void handle(ActionEvent e
            )
            {
                actiontargetBack.setFill(Color.FIREBRICK);
                actiontargetBack.setText("Back button pressed");
                screen2();
            }
        }
        );

        Scene scene = new Scene(grid, sceneX, sceneY);
        primaryStage.setScene(scene);
        primaryStage.show();

    }

    public void screen4()
    {
        primaryStage.setTitle("Text Data Format Converter");
        grid = new GridPane();
        grid.setAlignment(Pos.CENTER);
        grid.setHgap(10);
        grid.setVgap(10);
        grid.setPadding(new Insets(25, 25, 25, 25));
        Text scenetitle = new Text("Transformations");
        scenetitle.setFont(Font.font("Tahoma", FontWeight.NORMAL, 20));
        grid.add(scenetitle, 0, 0, 2, 1);

        grid.add(new Label("PROPERTIES"), 0, 1);
        TableView<Properties2> props = new TableView<Properties2>();
        props.setEditable(false);
        TableColumn column6 = new TableColumn("Column Name");
        TableColumn column7 = new TableColumn("fileRef");
        TableColumn column8 = new TableColumn("Type");
        ObservableList<Properties2> propsData = FXCollections.observableArrayList(new Properties2("RScolumn1", "TRUE", "STRING"), new Properties2("RScolumn2", "FALSE", "INTEGER"));
        column6.setCellValueFactory(new PropertyValueFactory<Properties2, String>("ColumnName"));
        column7.setCellValueFactory(new PropertyValueFactory<Properties2, String>("FileRef"));
        column8.setCellValueFactory(new PropertyValueFactory<Properties2, String>("Type"));

        props.setItems(propsData);
        props.getColumns().setAll(column6, column7, column8);
        grid.add(props, 0, 2);

        GridPane grid2 = new GridPane();
        grid2.setAlignment(Pos.CENTER);
        grid2.setHgap(10);
        grid2.setVgap(10);
        grid2.setPadding(new Insets(25, 25, 25, 25));

        Label transformLabel = new Label("Transform");
        grid2.add(transformLabel, 0, 0);

        ObservableList<String> options = FXCollections.observableArrayList(
                "String_To_String",
                "String_To_Int",
                "String_To_Boolean", "String_To_Char"
        );
        comboBox = new ComboBox(options);
        comboBox.setOnAction(new EventHandler<ActionEvent>()
        {
            @Override
            public void handle(ActionEvent e)
            {
                if (comboBox.getValue() == "XML File")
                {

                }
                else
                {
                    System.out.println(comboBox.getValue() + ": No GUI update action.");
                }
            }
        });
        grid2.add(comboBox, 0, 1);

        Button btnTransform = new Button("Add Transformation");
        btnTransform.setOnAction(new EventHandler<ActionEvent>()
        {

            @Override
            public void handle(ActionEvent e)
            {
                /*
                actiontarget.setFill(Color.FIREBRICK);
                actiontarget.setText("Next button pressed");
                 */
                //screen4();
            }
        });
        grid2.add(btnTransform, 0, 2);

        Button rmTransform = new Button("Remove Transformation");
        rmTransform.setOnAction(new EventHandler<ActionEvent>()
        {

            @Override
            public void handle(ActionEvent e)
            {
                /*
                actiontarget.setFill(Color.FIREBRICK);
                actiontarget.setText("Next button pressed");
                 */
                //screen4();
            }
        });
        grid2.add(rmTransform, 0, 3);

        grid.add(grid2, 2, 2);

        grid.add(new Label("TRANSFORMATIONS"), 3, 1);
        TableView<Properties> transTable = new TableView<Properties>();
        transTable.setEditable(false);
        TableColumn column2 = new TableColumn("Column Name");
        TableColumn column3 = new TableColumn("Transformation");

        ObservableList<Properties> transData = FXCollections.observableArrayList(new Properties("RScolumn1", "String_To_String"), new Properties("RScolumn2", "String_To_Char"));
        column2.setCellValueFactory(new PropertyValueFactory<Properties, String>("ColumnName"));
        column3.setCellValueFactory(new PropertyValueFactory<Properties, String>("Transformation"));

        transTable.setItems(transData);
        transTable.getColumns().setAll(column2, column3);
        grid.add(transTable, 3, 2);

        Button btnNext = new Button("Next");
        HBox hbBtn = new HBox(10);

        hbBtn.setAlignment(Pos.BOTTOM_RIGHT);

        hbBtn.getChildren()
                .add(btnNext);
        grid.add(hbBtn,
                13, 9);

        final Text actiontarget = new Text();

        grid.add(actiontarget,
                1, 7);
        btnNext.setOnAction(
                new EventHandler<ActionEvent>()
        {

            @Override
            public void handle(ActionEvent e
            )
            {
                //screen3();
            }
        }
        );

        Button backBtn = new Button("Back");
        grid.add(backBtn, 10, 9);
        backBtn.setOnAction(
                new EventHandler<ActionEvent>()
        {
            @Override
            public void handle(ActionEvent e
            )
            {
                screen3();
            }
        }
        );

        Button addMapBtn = new Button("Add Map");
        grid.add(addMapBtn, 7, 9);
        addMapBtn.setOnAction(
                new EventHandler<ActionEvent>()
        {
            @Override
            public void handle(ActionEvent e
            )
            {
                TextInputDialog dialog = new TextInputDialog("Map1");
                dialog.setTitle("New Map Creation");
                dialog.setHeaderText("Your new Map Needs a name");
                dialog.setContentText("Please enter name for your new Map:");

                Optional<String> result = dialog.showAndWait();
                if (result.isPresent())
                {
                    System.out.println("Your name: " + result.get());
                }
                else
                {
                    System.out.println("New Map not created because no name was provided.");
                    Alert alert = new Alert(AlertType.WARNING);
                    alert.setTitle("Warning");
                    alert.setHeaderText("Problem creating your new Map");
                    alert.setContentText("New Map not created because no name was provided.");
                    alert.showAndWait();
                }
            }
        }
        );

        Button genFileBtn = new Button("Generate File");
        grid.add(genFileBtn, 4, 9);
        genFileBtn.setOnAction(
                new EventHandler<ActionEvent>()
        {
            @Override
            public void handle(ActionEvent e
            )
            {
                //
            }
        }
        );

        Scene scene = new Scene(grid, sceneX, sceneY);
        primaryStage.setScene(scene);
        primaryStage.show();
    }

    /**
     * ******************************************************
     */
    public void oracleDBconnectionGUI()
    {
        if (comboBox.getValue() == "Oracle Database")
        {
            Label host = new Label("Host:");
            grid.add(host, 0, 2);

            TextField hostTextField = new TextField();
            grid.add(hostTextField, 1, 2);

            Label fname = new Label("Filename:");
            grid.add(fname, 0, 3);

            TextField fnameTextField = new TextField();
            grid.add(fnameTextField, 1, 3);

            Label user = new Label("User Name:");
            grid.add(user, 0, 4);

            TextField userTextField = new TextField();
            grid.add(userTextField, 1, 4);

            Label pw = new Label("Password:");
            grid.add(pw, 0, 5);

            PasswordField pwBox = new PasswordField();
            grid.add(pwBox, 1, 5);

            Button test = new Button("Test");
            HBox hbBtn2 = new HBox(10);
            hbBtn2.setAlignment(Pos.BOTTOM_RIGHT);
            hbBtn2.getChildren().add(test);
            grid.add(hbBtn2, 7, 9);

            final Text testactiontarget = new Text();
            grid.add(testactiontarget, 1, 7);
            test.setOnAction(new EventHandler<ActionEvent>()
            {
                @Override
                public void handle(ActionEvent e)
                {
                    testactiontarget.setFill(Color.GREEN);
                    testactiontarget.setText("Test button pressed");
                }
            });
        }
        else
        {
            System.out.println(comboBox.getValue() + ": No GUI update action.");
        }
    }

    public static class Properties
    {

        private final SimpleStringProperty columnName;
        private final SimpleStringProperty transformation;

        private Properties(String clmName, String tr)
        {
            this.transformation = new SimpleStringProperty(tr);
            this.columnName = new SimpleStringProperty(clmName);
        }

        public String getTransformation()
        {
            return transformation.get();
        }

        public void setTrasnformation(String tr)
        {
            transformation.set(tr);
        }

        public String getColumnName()
        {
            return columnName.get();
        }

        public void setColumnName(String cName)
        {
            columnName.set(cName);
        }
    }

    public static class Properties2
    {

        private final SimpleStringProperty columnName;
        private final SimpleStringProperty fileRef;
        private final SimpleStringProperty type;

        private Properties2(String clmName, String fref, String t)
        {
            this.columnName = new SimpleStringProperty(clmName);
            this.fileRef = new SimpleStringProperty(fref);
            this.type = new SimpleStringProperty(t);
        }

        public String getColumnName()
        {
            return columnName.get();
        }

        public void setColumnName(String cName)
        {
            columnName.set(cName);
        }

        public void setFileRef(String fRef)
        {
            fileRef.set(fRef);
        }

        public String getFileRef()
        {
            return fileRef.get();
        }

        public void setType(String t)
        {
            type.set(t);
        }

        public String getType()
        {
            return type.get();
        }
    }

    public static class Transformation //Make Pojo later
    {

        private final SimpleStringProperty tableName;
        private final SimpleStringProperty columnName;
        private final SimpleStringProperty transformation;

        private Transformation(String tblName, String clmName, String trans)
        {
            this.tableName = new SimpleStringProperty(tblName);
            this.columnName = new SimpleStringProperty(clmName);
            this.transformation = new SimpleStringProperty(trans);
        }

        public String getTableName()
        {
            return tableName.get();
        }

        public void setTableName(String tName)
        {
            tableName.set(tName);
        }

        public String getColumnName()
        {
            return columnName.get();
        }

        public void setColumnName(String cName)
        {
            columnName.set(cName);
        }

        public void setTransformation(String trans)
        {
            transformation.set(trans);
        }

        public String getTransformation()
        {
            return transformation.get();
        }
    }

    public static class Property3 //Make Pojo later
    {

        private final SimpleStringProperty columnName;

        private Property3(String cn)
        {
            this.columnName = new SimpleStringProperty(cn);
        }

        public String getColumnName()
        {
            return columnName.get();
        }

        public void setColumnName(String cn)
        {
            columnName.set(cn);
        }

    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args)
    {
        launch(args);
    }

}
