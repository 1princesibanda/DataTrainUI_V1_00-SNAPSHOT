// Generated from ..\..\prince\ETL_Wizard\V1_00\DatatrainUI\src\main\java\com\gew\datatrain\grammars\antlr4\TableDefinition.g4 by ANTLR 4.7.2
package com.gew.fusion.datatrain.ui.addon.view.wizard.parsers.antlr4.mysql.tabledefinition;
import org.antlr.v4.runtime.Lexer;
import org.antlr.v4.runtime.CharStream;
import org.antlr.v4.runtime.Token;
import org.antlr.v4.runtime.TokenStream;
import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.atn.*;
import org.antlr.v4.runtime.dfa.DFA;
import org.antlr.v4.runtime.misc.*;

@SuppressWarnings({"all", "warnings", "unchecked", "unused", "cast"})
public class TableDefinitionLexer extends Lexer {
	static { RuntimeMetaData.checkVersion("4.7.2", RuntimeMetaData.VERSION); }

	protected static final DFA[] _decisionToDFA;
	protected static final PredictionContextCache _sharedContextCache =
		new PredictionContextCache();
	public static final int
		T__0=1, STRING_LITERAL=2, NUMERIC_LITERAL=3, DATE_AND_TIME_LITERAL=4, 
		HEXADECIMAL_LITERAL=5, BITVALUE_LITERAL=6, BOOLEAN_LITERAL=7, ODBC_date=8, 
		ODBC_time=9, ODBC_timestamp=10, MYINT=11, IF=12, NOT=13, EXISTS=14, TRUE=15, 
		FALSE=16, PRIMARY=17, KEY=18, CREATE=19, TABLE=20, COLON=21, COMMA=22, 
		VARCHAR=23, CLOSINGROUNDBRACE=24, OPENINGROUNDBRACE=25, TINYINT=26, SMALLINT=27, 
		YEAR=28, INT_=29, BIGINT=30, UNSIGNED=31, FLOAT=32, DOUBLE=33, NUMERIC=34, 
		DECIMAL=35, DATETIME=36, TIMESTAMP=37, TIME=38, DATE=39, BIT=40, BOOLEAN=41, 
		ENGINE=42, INNODB=43, AUTO_INCREMENT=44, NULL=45, DEFAULT=46, ID=47, INT=48, 
		WS=49, COMMENT=50;
	public static String[] channelNames = {
		"DEFAULT_TOKEN_CHANNEL", "HIDDEN"
	};

	public static String[] modeNames = {
		"DEFAULT_MODE"
	};

	private static String[] makeRuleNames() {
		return new String[] {
			"T__0", "STRING_LITERAL", "NUMERIC_LITERAL", "DATE_AND_TIME_LITERAL", 
			"HEXADECIMAL_LITERAL", "BITVALUE_LITERAL", "BOOLEAN_LITERAL", "ODBC_date", 
			"ODBC_time", "ODBC_timestamp", "MYINT", "IF", "NOT", "EXISTS", "TRUE", 
			"FALSE", "PRIMARY", "KEY", "CREATE", "TABLE", "COLON", "COMMA", "VARCHAR", 
			"CLOSINGROUNDBRACE", "OPENINGROUNDBRACE", "TINYINT", "SMALLINT", "YEAR", 
			"INT_", "BIGINT", "UNSIGNED", "FLOAT", "DOUBLE", "NUMERIC", "DECIMAL", 
			"DATETIME", "TIMESTAMP", "TIME", "DATE", "BIT", "BOOLEAN", "ENGINE", 
			"INNODB", "AUTO_INCREMENT", "NULL", "DEFAULT", "ID", "INT", "WS", "COMMENT"
		};
	}
	public static final String[] ruleNames = makeRuleNames();

	private static String[] makeLiteralNames() {
		return new String[] {
			null, "'='", null, null, null, null, null, null, null, null, null, null, 
			null, null, null, null, null, null, null, null, null, "';'", "','", null, 
			"')'", "'('", null, "'SMALLINT'"
		};
	}
	private static final String[] _LITERAL_NAMES = makeLiteralNames();
	private static String[] makeSymbolicNames() {
		return new String[] {
			null, null, "STRING_LITERAL", "NUMERIC_LITERAL", "DATE_AND_TIME_LITERAL", 
			"HEXADECIMAL_LITERAL", "BITVALUE_LITERAL", "BOOLEAN_LITERAL", "ODBC_date", 
			"ODBC_time", "ODBC_timestamp", "MYINT", "IF", "NOT", "EXISTS", "TRUE", 
			"FALSE", "PRIMARY", "KEY", "CREATE", "TABLE", "COLON", "COMMA", "VARCHAR", 
			"CLOSINGROUNDBRACE", "OPENINGROUNDBRACE", "TINYINT", "SMALLINT", "YEAR", 
			"INT_", "BIGINT", "UNSIGNED", "FLOAT", "DOUBLE", "NUMERIC", "DECIMAL", 
			"DATETIME", "TIMESTAMP", "TIME", "DATE", "BIT", "BOOLEAN", "ENGINE", 
			"INNODB", "AUTO_INCREMENT", "NULL", "DEFAULT", "ID", "INT", "WS", "COMMENT"
		};
	}
	private static final String[] _SYMBOLIC_NAMES = makeSymbolicNames();
	public static final Vocabulary VOCABULARY = new VocabularyImpl(_LITERAL_NAMES, _SYMBOLIC_NAMES);

	/**
	 * @deprecated Use {@link #VOCABULARY} instead.
	 */
	@Deprecated
	public static final String[] tokenNames;
	static {
		tokenNames = new String[_SYMBOLIC_NAMES.length];
		for (int i = 0; i < tokenNames.length; i++) {
			tokenNames[i] = VOCABULARY.getLiteralName(i);
			if (tokenNames[i] == null) {
				tokenNames[i] = VOCABULARY.getSymbolicName(i);
			}

			if (tokenNames[i] == null) {
				tokenNames[i] = "<INVALID>";
			}
		}
	}

	@Override
	@Deprecated
	public String[] getTokenNames() {
		return tokenNames;
	}

	@Override

	public Vocabulary getVocabulary() {
		return VOCABULARY;
	}


	public TableDefinitionLexer(CharStream input) {
		super(input);
		_interp = new LexerATNSimulator(this,_ATN,_decisionToDFA,_sharedContextCache);
	}

	@Override
	public String getGrammarFileName() { return "TableDefinition.g4"; }

	@Override
	public String[] getRuleNames() { return ruleNames; }

	@Override
	public String getSerializedATN() { return _serializedATN; }

	@Override
	public String[] getChannelNames() { return channelNames; }

	@Override
	public String[] getModeNames() { return modeNames; }

	@Override
	public ATN getATN() { return _ATN; }

	public static final String _serializedATN =
		"\3\u608b\ua72a\u8133\ub9ed\u417c\u3be7\u7786\u5964\2\64\u01cf\b\1\4\2"+
		"\t\2\4\3\t\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7\t\7\4\b\t\b\4\t\t\t\4\n\t\n\4"+
		"\13\t\13\4\f\t\f\4\r\t\r\4\16\t\16\4\17\t\17\4\20\t\20\4\21\t\21\4\22"+
		"\t\22\4\23\t\23\4\24\t\24\4\25\t\25\4\26\t\26\4\27\t\27\4\30\t\30\4\31"+
		"\t\31\4\32\t\32\4\33\t\33\4\34\t\34\4\35\t\35\4\36\t\36\4\37\t\37\4 \t"+
		" \4!\t!\4\"\t\"\4#\t#\4$\t$\4%\t%\4&\t&\4\'\t\'\4(\t(\4)\t)\4*\t*\4+\t"+
		"+\4,\t,\4-\t-\4.\t.\4/\t/\4\60\t\60\4\61\t\61\4\62\t\62\4\63\t\63\3\2"+
		"\3\2\3\3\3\3\7\3l\n\3\f\3\16\3o\13\3\3\3\3\3\3\3\7\3t\n\3\f\3\16\3w\13"+
		"\3\3\3\5\3z\n\3\3\4\5\4}\n\4\3\4\7\4\u0080\n\4\f\4\16\4\u0083\13\4\3\4"+
		"\5\4\u0086\n\4\3\4\6\4\u0089\n\4\r\4\16\4\u008a\3\4\3\4\5\4\u008f\n\4"+
		"\3\4\6\4\u0092\n\4\r\4\16\4\u0093\5\4\u0096\n\4\3\5\3\5\3\5\3\5\3\5\3"+
		"\5\5\5\u009e\n\5\3\5\3\5\7\5\u00a2\n\5\f\5\16\5\u00a5\13\5\3\5\3\5\3\5"+
		"\7\5\u00aa\n\5\f\5\16\5\u00ad\13\5\3\5\5\5\u00b0\n\5\3\6\3\6\3\6\3\6\3"+
		"\6\3\6\3\6\3\6\5\6\u00ba\n\6\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\5\7\u00c4"+
		"\n\7\3\b\3\b\5\b\u00c8\n\b\3\t\3\t\3\n\3\n\3\13\3\13\3\13\3\f\6\f\u00d2"+
		"\n\f\r\f\16\f\u00d3\3\r\3\r\3\r\3\16\3\16\3\16\3\16\3\17\3\17\3\17\3\17"+
		"\3\17\3\17\3\17\3\20\3\20\3\20\3\20\3\20\3\21\3\21\3\21\3\21\3\21\3\21"+
		"\3\22\3\22\3\22\3\22\3\22\3\22\3\22\3\22\3\23\3\23\3\23\3\23\3\24\3\24"+
		"\3\24\3\24\3\24\3\24\3\24\3\25\3\25\3\25\3\25\3\25\3\25\3\26\3\26\3\27"+
		"\3\27\3\30\3\30\3\30\3\30\3\30\3\30\3\30\3\30\3\31\3\31\3\32\3\32\3\33"+
		"\3\33\3\33\3\33\3\33\3\33\3\33\3\33\3\34\3\34\3\34\3\34\3\34\3\34\3\34"+
		"\3\34\3\34\3\35\3\35\3\35\3\35\3\35\3\36\3\36\3\36\3\36\3\37\3\37\3\37"+
		"\3\37\3\37\3\37\3\37\3 \3 \3 \3 \3 \3 \3 \3 \3 \3!\3!\3!\3!\3!\3!\3\""+
		"\3\"\3\"\3\"\3\"\3\"\3\"\3#\3#\3#\3#\3#\3#\3#\3#\3$\3$\3$\3$\3$\3$\3$"+
		"\3$\3%\3%\3%\3%\3%\3%\3%\3%\3%\3&\3&\3&\3&\3&\3&\3&\3&\3&\3&\3\'\3\'\3"+
		"\'\3\'\3\'\3(\3(\3(\3(\3(\3)\3)\3)\3)\3*\3*\3*\3*\3*\3*\3*\5*\u0187\n"+
		"*\3+\3+\3+\3+\3+\3+\3+\3,\3,\3,\3,\3,\3,\3,\3-\3-\3-\3-\3-\3-\3-\3-\3"+
		"-\3-\3-\3-\3-\3-\3-\3.\3.\3.\3.\3.\3/\3/\3/\3/\3/\3/\3/\3/\3\60\6\60\u01b4"+
		"\n\60\r\60\16\60\u01b5\3\61\6\61\u01b9\n\61\r\61\16\61\u01ba\3\62\6\62"+
		"\u01be\n\62\r\62\16\62\u01bf\3\62\3\62\3\63\3\63\3\63\3\63\7\63\u01c8"+
		"\n\63\f\63\16\63\u01cb\13\63\3\63\3\63\3\63\7mu\u00a3\u00ab\u01c9\2\64"+
		"\3\3\5\4\7\5\t\6\13\7\r\b\17\t\21\n\23\13\25\f\27\r\31\16\33\17\35\20"+
		"\37\21!\22#\23%\24\'\25)\26+\27-\30/\31\61\32\63\33\65\34\67\359\36;\37"+
		"= ?!A\"C#E$G%I&K\'M(O)Q*S+U,W-Y.[/]\60_\61a\62c\63e\64\3\2 \3\2))\3\2"+
		"$$\4\2--//\3\2\62;\4\2GGgg\4\2ZZzz\4\2\62;CH\4\2DDdd\4\2FFff\4\2VVvv\4"+
		"\2UUuu\4\2KKkk\4\2HHhh\4\2PPpp\4\2QQqq\4\2TTtt\4\2WWww\4\2CCcc\4\2NNn"+
		"n\4\2RRrr\4\2OOoo\4\2[[{{\4\2MMmm\4\2EEee\4\2XXxx\4\2JJjj\4\2IIii\4\2"+
		"VVkk\5\2C\\aac|\5\2\13\f\17\17\"\"\2\u01e9\2\3\3\2\2\2\2\5\3\2\2\2\2\7"+
		"\3\2\2\2\2\t\3\2\2\2\2\13\3\2\2\2\2\r\3\2\2\2\2\17\3\2\2\2\2\21\3\2\2"+
		"\2\2\23\3\2\2\2\2\25\3\2\2\2\2\27\3\2\2\2\2\31\3\2\2\2\2\33\3\2\2\2\2"+
		"\35\3\2\2\2\2\37\3\2\2\2\2!\3\2\2\2\2#\3\2\2\2\2%\3\2\2\2\2\'\3\2\2\2"+
		"\2)\3\2\2\2\2+\3\2\2\2\2-\3\2\2\2\2/\3\2\2\2\2\61\3\2\2\2\2\63\3\2\2\2"+
		"\2\65\3\2\2\2\2\67\3\2\2\2\29\3\2\2\2\2;\3\2\2\2\2=\3\2\2\2\2?\3\2\2\2"+
		"\2A\3\2\2\2\2C\3\2\2\2\2E\3\2\2\2\2G\3\2\2\2\2I\3\2\2\2\2K\3\2\2\2\2M"+
		"\3\2\2\2\2O\3\2\2\2\2Q\3\2\2\2\2S\3\2\2\2\2U\3\2\2\2\2W\3\2\2\2\2Y\3\2"+
		"\2\2\2[\3\2\2\2\2]\3\2\2\2\2_\3\2\2\2\2a\3\2\2\2\2c\3\2\2\2\2e\3\2\2\2"+
		"\3g\3\2\2\2\5y\3\2\2\2\7|\3\2\2\2\t\u009d\3\2\2\2\13\u00b9\3\2\2\2\r\u00c3"+
		"\3\2\2\2\17\u00c7\3\2\2\2\21\u00c9\3\2\2\2\23\u00cb\3\2\2\2\25\u00cd\3"+
		"\2\2\2\27\u00d1\3\2\2\2\31\u00d5\3\2\2\2\33\u00d8\3\2\2\2\35\u00dc\3\2"+
		"\2\2\37\u00e3\3\2\2\2!\u00e8\3\2\2\2#\u00ee\3\2\2\2%\u00f6\3\2\2\2\'\u00fa"+
		"\3\2\2\2)\u0101\3\2\2\2+\u0107\3\2\2\2-\u0109\3\2\2\2/\u010b\3\2\2\2\61"+
		"\u0113\3\2\2\2\63\u0115\3\2\2\2\65\u0117\3\2\2\2\67\u011f\3\2\2\29\u0128"+
		"\3\2\2\2;\u012d\3\2\2\2=\u0131\3\2\2\2?\u0138\3\2\2\2A\u0141\3\2\2\2C"+
		"\u0147\3\2\2\2E\u014e\3\2\2\2G\u0156\3\2\2\2I\u015e\3\2\2\2K\u0167\3\2"+
		"\2\2M\u0171\3\2\2\2O\u0176\3\2\2\2Q\u017b\3\2\2\2S\u017f\3\2\2\2U\u0188"+
		"\3\2\2\2W\u018f\3\2\2\2Y\u0196\3\2\2\2[\u01a5\3\2\2\2]\u01aa\3\2\2\2_"+
		"\u01b3\3\2\2\2a\u01b8\3\2\2\2c\u01bd\3\2\2\2e\u01c3\3\2\2\2gh\7?\2\2h"+
		"\4\3\2\2\2im\t\2\2\2jl\13\2\2\2kj\3\2\2\2lo\3\2\2\2mn\3\2\2\2mk\3\2\2"+
		"\2np\3\2\2\2om\3\2\2\2pz\t\2\2\2qu\t\3\2\2rt\13\2\2\2sr\3\2\2\2tw\3\2"+
		"\2\2uv\3\2\2\2us\3\2\2\2vx\3\2\2\2wu\3\2\2\2xz\t\3\2\2yi\3\2\2\2yq\3\2"+
		"\2\2z\6\3\2\2\2{}\t\4\2\2|{\3\2\2\2|}\3\2\2\2}\u0081\3\2\2\2~\u0080\t"+
		"\5\2\2\177~\3\2\2\2\u0080\u0083\3\2\2\2\u0081\177\3\2\2\2\u0081\u0082"+
		"\3\2\2\2\u0082\u0085\3\2\2\2\u0083\u0081\3\2\2\2\u0084\u0086\7\60\2\2"+
		"\u0085\u0084\3\2\2\2\u0085\u0086\3\2\2\2\u0086\u0088\3\2\2\2\u0087\u0089"+
		"\t\5\2\2\u0088\u0087\3\2\2\2\u0089\u008a\3\2\2\2\u008a\u0088\3\2\2\2\u008a"+
		"\u008b\3\2\2\2\u008b\u0095\3\2\2\2\u008c\u008e\t\6\2\2\u008d\u008f\t\4"+
		"\2\2\u008e\u008d\3\2\2\2\u008e\u008f\3\2\2\2\u008f\u0091\3\2\2\2\u0090"+
		"\u0092\t\5\2\2\u0091\u0090\3\2\2\2\u0092\u0093\3\2\2\2\u0093\u0091\3\2"+
		"\2\2\u0093\u0094\3\2\2\2\u0094\u0096\3\2\2\2\u0095\u008c\3\2\2\2\u0095"+
		"\u0096\3\2\2\2\u0096\b\3\2\2\2\u0097\u009e\5O(\2\u0098\u009e\5\21\t\2"+
		"\u0099\u009e\5M\'\2\u009a\u009e\5\23\n\2\u009b\u009e\5K&\2\u009c\u009e"+
		"\5\25\13\2\u009d\u0097\3\2\2\2\u009d\u0098\3\2\2\2\u009d\u0099\3\2\2\2"+
		"\u009d\u009a\3\2\2\2\u009d\u009b\3\2\2\2\u009d\u009c\3\2\2\2\u009e\u00af"+
		"\3\2\2\2\u009f\u00a3\7)\2\2\u00a0\u00a2\13\2\2\2\u00a1\u00a0\3\2\2\2\u00a2"+
		"\u00a5\3\2\2\2\u00a3\u00a4\3\2\2\2\u00a3\u00a1\3\2\2\2\u00a4\u00a6\3\2"+
		"\2\2\u00a5\u00a3\3\2\2\2\u00a6\u00b0\7)\2\2\u00a7\u00ab\7$\2\2\u00a8\u00aa"+
		"\13\2\2\2\u00a9\u00a8\3\2\2\2\u00aa\u00ad\3\2\2\2\u00ab\u00ac\3\2\2\2"+
		"\u00ab\u00a9\3\2\2\2\u00ac\u00ae\3\2\2\2\u00ad\u00ab\3\2\2\2\u00ae\u00b0"+
		"\7$\2\2\u00af\u009f\3\2\2\2\u00af\u00a7\3\2\2\2\u00b0\n\3\2\2\2\u00b1"+
		"\u00b2\t\7\2\2\u00b2\u00b3\t\2\2\2\u00b3\u00b4\t\b\2\2\u00b4\u00ba\t\2"+
		"\2\2\u00b5\u00b6\7\62\2\2\u00b6\u00b7\7z\2\2\u00b7\u00b8\3\2\2\2\u00b8"+
		"\u00ba\t\b\2\2\u00b9\u00b1\3\2\2\2\u00b9\u00b5\3\2\2\2\u00ba\f\3\2\2\2"+
		"\u00bb\u00bc\t\t\2\2\u00bc\u00bd\t\2\2\2\u00bd\u00be\t\b\2\2\u00be\u00c4"+
		"\t\2\2\2\u00bf\u00c0\7\62\2\2\u00c0\u00c1\7d\2\2\u00c1\u00c2\3\2\2\2\u00c2"+
		"\u00c4\t\b\2\2\u00c3\u00bb\3\2\2\2\u00c3\u00bf\3\2\2\2\u00c4\16\3\2\2"+
		"\2\u00c5\u00c8\5\37\20\2\u00c6\u00c8\5!\21\2\u00c7\u00c5\3\2\2\2\u00c7"+
		"\u00c6\3\2\2\2\u00c8\20\3\2\2\2\u00c9\u00ca\t\n\2\2\u00ca\22\3\2\2\2\u00cb"+
		"\u00cc\t\13\2\2\u00cc\24\3\2\2\2\u00cd\u00ce\t\13\2\2\u00ce\u00cf\t\f"+
		"\2\2\u00cf\26\3\2\2\2\u00d0\u00d2\t\5\2\2\u00d1\u00d0\3\2\2\2\u00d2\u00d3"+
		"\3\2\2\2\u00d3\u00d1\3\2\2\2\u00d3\u00d4\3\2\2\2\u00d4\30\3\2\2\2\u00d5"+
		"\u00d6\t\r\2\2\u00d6\u00d7\t\16\2\2\u00d7\32\3\2\2\2\u00d8\u00d9\t\17"+
		"\2\2\u00d9\u00da\t\20\2\2\u00da\u00db\t\13\2\2\u00db\34\3\2\2\2\u00dc"+
		"\u00dd\t\6\2\2\u00dd\u00de\t\7\2\2\u00de\u00df\t\r\2\2\u00df\u00e0\t\f"+
		"\2\2\u00e0\u00e1\t\13\2\2\u00e1\u00e2\t\f\2\2\u00e2\36\3\2\2\2\u00e3\u00e4"+
		"\t\13\2\2\u00e4\u00e5\t\21\2\2\u00e5\u00e6\t\22\2\2\u00e6\u00e7\t\6\2"+
		"\2\u00e7 \3\2\2\2\u00e8\u00e9\t\16\2\2\u00e9\u00ea\t\23\2\2\u00ea\u00eb"+
		"\t\24\2\2\u00eb\u00ec\t\f\2\2\u00ec\u00ed\t\6\2\2\u00ed\"\3\2\2\2\u00ee"+
		"\u00ef\t\25\2\2\u00ef\u00f0\t\21\2\2\u00f0\u00f1\t\r\2\2\u00f1\u00f2\t"+
		"\26\2\2\u00f2\u00f3\t\23\2\2\u00f3\u00f4\t\21\2\2\u00f4\u00f5\t\27\2\2"+
		"\u00f5$\3\2\2\2\u00f6\u00f7\t\30\2\2\u00f7\u00f8\t\6\2\2\u00f8\u00f9\t"+
		"\27\2\2\u00f9&\3\2\2\2\u00fa\u00fb\t\31\2\2\u00fb\u00fc\t\21\2\2\u00fc"+
		"\u00fd\t\6\2\2\u00fd\u00fe\t\23\2\2\u00fe\u00ff\t\13\2\2\u00ff\u0100\t"+
		"\6\2\2\u0100(\3\2\2\2\u0101\u0102\t\13\2\2\u0102\u0103\t\23\2\2\u0103"+
		"\u0104\t\t\2\2\u0104\u0105\t\24\2\2\u0105\u0106\t\6\2\2\u0106*\3\2\2\2"+
		"\u0107\u0108\7=\2\2\u0108,\3\2\2\2\u0109\u010a\7.\2\2\u010a.\3\2\2\2\u010b"+
		"\u010c\t\32\2\2\u010c\u010d\t\23\2\2\u010d\u010e\t\21\2\2\u010e\u010f"+
		"\t\31\2\2\u010f\u0110\t\33\2\2\u0110\u0111\t\23\2\2\u0111\u0112\t\21\2"+
		"\2\u0112\60\3\2\2\2\u0113\u0114\7+\2\2\u0114\62\3\2\2\2\u0115\u0116\7"+
		"*\2\2\u0116\64\3\2\2\2\u0117\u0118\t\13\2\2\u0118\u0119\t\r\2\2\u0119"+
		"\u011a\t\17\2\2\u011a\u011b\t\27\2\2\u011b\u011c\t\r\2\2\u011c\u011d\t"+
		"\17\2\2\u011d\u011e\t\13\2\2\u011e\66\3\2\2\2\u011f\u0120\7U\2\2\u0120"+
		"\u0121\7O\2\2\u0121\u0122\7C\2\2\u0122\u0123\7N\2\2\u0123\u0124\7N\2\2"+
		"\u0124\u0125\7K\2\2\u0125\u0126\7P\2\2\u0126\u0127\7V\2\2\u01278\3\2\2"+
		"\2\u0128\u0129\t\27\2\2\u0129\u012a\t\6\2\2\u012a\u012b\t\23\2\2\u012b"+
		"\u012c\t\21\2\2\u012c:\3\2\2\2\u012d\u012e\t\r\2\2\u012e\u012f\t\17\2"+
		"\2\u012f\u0130\t\13\2\2\u0130<\3\2\2\2\u0131\u0132\t\t\2\2\u0132\u0133"+
		"\t\r\2\2\u0133\u0134\t\34\2\2\u0134\u0135\t\r\2\2\u0135\u0136\t\17\2\2"+
		"\u0136\u0137\t\13\2\2\u0137>\3\2\2\2\u0138\u0139\t\22\2\2\u0139\u013a"+
		"\t\17\2\2\u013a\u013b\t\f\2\2\u013b\u013c\t\r\2\2\u013c\u013d\t\34\2\2"+
		"\u013d\u013e\t\17\2\2\u013e\u013f\t\6\2\2\u013f\u0140\t\n\2\2\u0140@\3"+
		"\2\2\2\u0141\u0142\t\16\2\2\u0142\u0143\t\24\2\2\u0143\u0144\t\20\2\2"+
		"\u0144\u0145\t\23\2\2\u0145\u0146\t\13\2\2\u0146B\3\2\2\2\u0147\u0148"+
		"\t\n\2\2\u0148\u0149\t\20\2\2\u0149\u014a\t\22\2\2\u014a\u014b\t\t\2\2"+
		"\u014b\u014c\t\24\2\2\u014c\u014d\t\6\2\2\u014dD\3\2\2\2\u014e\u014f\t"+
		"\17\2\2\u014f\u0150\t\22\2\2\u0150\u0151\t\26\2\2\u0151\u0152\t\6\2\2"+
		"\u0152\u0153\t\21\2\2\u0153\u0154\t\r\2\2\u0154\u0155\t\31\2\2\u0155F"+
		"\3\2\2\2\u0156\u0157\t\n\2\2\u0157\u0158\t\6\2\2\u0158\u0159\t\31\2\2"+
		"\u0159\u015a\t\r\2\2\u015a\u015b\t\26\2\2\u015b\u015c\t\23\2\2\u015c\u015d"+
		"\t\24\2\2\u015dH\3\2\2\2\u015e\u015f\t\n\2\2\u015f\u0160\t\23\2\2\u0160"+
		"\u0161\t\13\2\2\u0161\u0162\t\6\2\2\u0162\u0163\t\13\2\2\u0163\u0164\t"+
		"\r\2\2\u0164\u0165\t\26\2\2\u0165\u0166\t\6\2\2\u0166J\3\2\2\2\u0167\u0168"+
		"\t\13\2\2\u0168\u0169\t\r\2\2\u0169\u016a\t\26\2\2\u016a\u016b\t\6\2\2"+
		"\u016b\u016c\t\f\2\2\u016c\u016d\t\13\2\2\u016d\u016e\t\23\2\2\u016e\u016f"+
		"\t\26\2\2\u016f\u0170\t\25\2\2\u0170L\3\2\2\2\u0171\u0172\t\13\2\2\u0172"+
		"\u0173\t\r\2\2\u0173\u0174\t\26\2\2\u0174\u0175\t\6\2\2\u0175N\3\2\2\2"+
		"\u0176\u0177\t\n\2\2\u0177\u0178\t\23\2\2\u0178\u0179\t\13\2\2\u0179\u017a"+
		"\t\6\2\2\u017aP\3\2\2\2\u017b\u017c\t\t\2\2\u017c\u017d\t\r\2\2\u017d"+
		"\u017e\t\35\2\2\u017eR\3\2\2\2\u017f\u0180\t\t\2\2\u0180\u0181\t\20\2"+
		"\2\u0181\u0182\t\20\2\2\u0182\u0186\t\24\2\2\u0183\u0184\t\6\2\2\u0184"+
		"\u0185\t\23\2\2\u0185\u0187\t\17\2\2\u0186\u0183\3\2\2\2\u0186\u0187\3"+
		"\2\2\2\u0187T\3\2\2\2\u0188\u0189\t\6\2\2\u0189\u018a\t\17\2\2\u018a\u018b"+
		"\t\34\2\2\u018b\u018c\t\r\2\2\u018c\u018d\t\17\2\2\u018d\u018e\t\6\2\2"+
		"\u018eV\3\2\2\2\u018f\u0190\t\r\2\2\u0190\u0191\t\17\2\2\u0191\u0192\t"+
		"\17\2\2\u0192\u0193\t\20\2\2\u0193\u0194\t\n\2\2\u0194\u0195\t\t\2\2\u0195"+
		"X\3\2\2\2\u0196\u0197\t\23\2\2\u0197\u0198\t\22\2\2\u0198\u0199\t\13\2"+
		"\2\u0199\u019a\t\20\2\2\u019a\u019b\7a\2\2\u019b\u019c\t\r\2\2\u019c\u019d"+
		"\t\17\2\2\u019d\u019e\t\31\2\2\u019e\u019f\t\21\2\2\u019f\u01a0\t\6\2"+
		"\2\u01a0\u01a1\t\26\2\2\u01a1\u01a2\t\6\2\2\u01a2\u01a3\t\17\2\2\u01a3"+
		"\u01a4\t\13\2\2\u01a4Z\3\2\2\2\u01a5\u01a6\t\17\2\2\u01a6\u01a7\t\22\2"+
		"\2\u01a7\u01a8\t\24\2\2\u01a8\u01a9\t\24\2\2\u01a9\\\3\2\2\2\u01aa\u01ab"+
		"\t\n\2\2\u01ab\u01ac\t\6\2\2\u01ac\u01ad\t\16\2\2\u01ad\u01ae\t\23\2\2"+
		"\u01ae\u01af\t\22\2\2\u01af\u01b0\t\24\2\2\u01b0\u01b1\t\13\2\2\u01b1"+
		"^\3\2\2\2\u01b2\u01b4\t\36\2\2\u01b3\u01b2\3\2\2\2\u01b4\u01b5\3\2\2\2"+
		"\u01b5\u01b3\3\2\2\2\u01b5\u01b6\3\2\2\2\u01b6`\3\2\2\2\u01b7\u01b9\t"+
		"\5\2\2\u01b8\u01b7\3\2\2\2\u01b9\u01ba\3\2\2\2\u01ba\u01b8\3\2\2\2\u01ba"+
		"\u01bb\3\2\2\2\u01bbb\3\2\2\2\u01bc\u01be\t\37\2\2\u01bd\u01bc\3\2\2\2"+
		"\u01be\u01bf\3\2\2\2\u01bf\u01bd\3\2\2\2\u01bf\u01c0\3\2\2\2\u01c0\u01c1"+
		"\3\2\2\2\u01c1\u01c2\b\62\2\2\u01c2d\3\2\2\2\u01c3\u01c4\7\61\2\2\u01c4"+
		"\u01c5\7,\2\2\u01c5\u01c9\3\2\2\2\u01c6\u01c8\13\2\2\2\u01c7\u01c6\3\2"+
		"\2\2\u01c8\u01cb\3\2\2\2\u01c9\u01ca\3\2\2\2\u01c9\u01c7\3\2\2\2\u01ca"+
		"\u01cc\3\2\2\2\u01cb\u01c9\3\2\2\2\u01cc\u01cd\7,\2\2\u01cd\u01ce\7\61"+
		"\2\2\u01cef\3\2\2\2\32\2muy|\u0081\u0085\u008a\u008e\u0093\u0095\u009d"+
		"\u00a3\u00ab\u00af\u00b9\u00c3\u00c7\u00d3\u0186\u01b5\u01ba\u01bf\u01c9"+
		"\3\b\2\2";
	public static final ATN _ATN =
		new ATNDeserializer().deserialize(_serializedATN.toCharArray());
	static {
		_decisionToDFA = new DFA[_ATN.getNumberOfDecisions()];
		for (int i = 0; i < _ATN.getNumberOfDecisions(); i++) {
			_decisionToDFA[i] = new DFA(_ATN.getDecisionState(i), i);
		}
	}
}