/**
 * Copyright (c) 2019 GEW Technologies (GEW). All rights reserved.
 */
package com.gew.fusion.datatrain.ui.addon.view;

import com.gew.fusion.datatrain.ui.addon.cFXDataTrainUIAddon;
import com.gew.fusion.datatrain.ui.addon.view.wizard.cMain;
import com.gew.fusion.datatrain.ui.addon.view.wizard.cWizardView;
import com.gew.fusion.datatrain.ui.addon.view.wizard.com.gew.datatrain.ui.IDataTrainView;
import com.gew.lib.ui.view.cViewableNode;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;

/**
 *
 * @author Philip M. Trenwith
 */
public class cStandaloneUI extends Application
{

	public static cMain cmain;

	public static void main(String[] args)
	{
		launch();
	}

	public cStandaloneUI() throws Exception
	{
		this.cmain = new cMain();
	}

	@Override
	public void init() throws Exception
	{
		cmain.init();
	}

	@Override
	public void start(Stage primaryStage) throws Exception
	{
		cFXDataTrainUIAddon oAddon = new cFXDataTrainUIAddon(null, null);
		oAddon.initAddon();

		cViewableNode[] oViewableNodes = oAddon.getViewableNodes();

		TabPane oTabPane = new TabPane();
		for (cViewableNode oNode : oViewableNodes)
		{
			Tab oTab = new Tab(oNode.getViewName());
			oTab.setContent(oNode.getNode());
			oTabPane.getTabs().add(oTab);
		}

		primaryStage.setScene(new Scene(oTabPane, 1600, 600));
		primaryStage.setTitle("DataTrain UI Standalone");
		primaryStage.show();
		this.cmain.start(primaryStage);
	}

	@Override
	public void stop() throws Exception
	{
		this.cmain.stop();
	}
}
