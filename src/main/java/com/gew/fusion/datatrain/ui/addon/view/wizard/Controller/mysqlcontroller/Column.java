/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gew.fusion.datatrain.ui.addon.view.wizard.Controller.mysqlcontroller;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Objects;

/**
 *
 * @author 0400626
 */
public class Column implements ITableCreator
{

	private String name;
	int type;
	private String typeName;
	private String anytext;
	private Boolean autoIncrement;
	private List<String> data;

	public Column(String name, int type, String typeName, Boolean autoIncrement, String anytext)
	{
		this.name = name;
		this.type = type;
		this.typeName = typeName;
		this.autoIncrement = autoIncrement;
		this.anytext = anytext;
		this.data = new LinkedList<>();
	}

	public String getName()
	{
		return this.name;
	}

	public Boolean isAutoIncrement()
	{
		return this.autoIncrement;
	}
	
	public List<String> getData()
	{
		return this.data;
	}

	@Override
	public String toString()
	{
		Map<String, String> map = new HashMap<>();
		map.put("Name", this.name);
		map.put("Type", "" + this.type);
		map.put("Anytext", this.anytext);
		return map.toString();
	}

	@Override
	public boolean equals(Object other)
	{
		if (other == null)
			return false;

		return other instanceof Column ? this.name.equals(((Column) other).name) : false;
	}

	@Override
	public int hashCode()
	{
		int hash = 3;
		hash = 67 * hash + Objects.hashCode(this.name);
		return hash;
	}

}
