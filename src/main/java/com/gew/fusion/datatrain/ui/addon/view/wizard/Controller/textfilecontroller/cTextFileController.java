/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gew.fusion.datatrain.ui.addon.view.wizard.Controller.textfilecontroller;

/**
 *
 * @author 0400626
 * Allows user to retrieve or store entries into the parse tree of a text file, using path names.
 * The user must provide a grammar describing the text file, then the controller must be re-compiled whenever the grammar is modified.
 * The controller wont allow for user to do reads where
 * The controller wont allow user to do writes that insert new nodes not understood by the grammar of the specified file, ie we wont allow
 * for user to make writes outside of the description of the underlying file such that the grammar is not able to parse the written file. Permissible
 * writes are those that the grammar can still parse after the write modification by user.
 */
public class cTextFileController
{
	
}
