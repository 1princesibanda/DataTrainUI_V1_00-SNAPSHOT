/**
 * Copyright (c) 2019 GEW Technologies (GEW). All rights reserved.
 */
package com.gew.fusion.datatrain.ui.addon.view.factories;


import com.gew.ifs.ext.datatrain.lib.files.EFileOperationStatus;
import com.gew.lib.time.cTime;
import java.text.SimpleDateFormat;
import java.util.Date;
import javafx.beans.value.ObservableValue;
import javafx.geometry.Pos;
import javafx.scene.control.*;
import javafx.scene.layout.HBox;
import javafx.util.Callback;
import com.gew.ifs.ext.datatrain.lib.files.IFileOperationFeedbackWrapper;
import com.gew.ifs.ext.datatrain.lib.statistics.IStatisticsWrapper;
import java.text.DecimalFormat;

/**
 * Table Cell Factories.
 *
 * Provides various table cell factories for rendering cell information or producing cell values in the import and
 * export history tables.
 *
 * @author Philip M. Trenwith
 */
public class cTableCellFactory
{

  /**
   * @return a cell factory for file name values.
   */
  public static Callback fileNameValueFactory()
  {
    return new cFileNameValue();
  }


  /**
   * @return a cell factory for status values.
   */
  public static Callback statusValueFactory()
  {
    return new cStatusValue();
  }


  /**
   * @return a cell factory for progress values.
   */
  public static Callback progressValueFactory()
  {
    return new cProgressValue();
  }


  /**
   * @return a cell factory for size values.
   */
  public static Callback sizeValueFactory()
  {
    return new cSizeValue();
  }
  
  
  /**
   * @return a cell factory for property name values.
   */
  public static Callback propertyNameValueFactory()
  {
    return new cPropertyNameValue();
  }

  
  /**
   * @return a cell factory for property type values.
   */
  public static Callback propertyTypeValueFactory()
  {
    return new cPropertyTypeValue();
  }
  

  /**
   * @return a cell factory for property count values.
   */
  public static Callback propertyCountValueFactory()
  {
    return new cPropertyCountValue();
  }
  
  
  /**
   * @return a cell factory for time (cTime) values.
   */
  public static Callback startTimeValueFactory()
  {
    return new cStartTimeValue();
  }


  /**
   * @return a cell factory for time (cTime) values.
   */
  public static Callback endTimeValueFactory()
  {
    return new cEndTimeValue();
  }


  /**
   * @return a cell factory for rendering string information.
   */
  public static TableCell<?, String> stringCellFactory()
  {
    return new cStringCell();
  }


  /**
   * @return a cell factory for rendering time (cTime) information.
   */
  public static TableCell<?, cTime> timeCellFactory()
  {
    return new cTimeCell();
  }


  /**
   * @return a cell factory for rendering status (EImportExportStatus) information.
   */
  public static TableCell<?, EFileOperationStatus> statusCellFactory()
  {
    return new cStatusCell();
  }


  /**
   * @return a cell factory for rendering progress information (renders a progress bar).
   */
  public static TableCell<?, Integer> progressCellFactory()
  {
    return new cProgressCell();
  }
  
  /**
   * @return a cell factory for rendering size information.
   */
  public static TableCell<?, Long> sizeCellFactory()
  {
    return new cSizeCell();
  }
  

  public static TableCell<?, Long> propertyCountCellFactory()
  {
    return new cLongCell();
  }


  private static class cFileNameValue implements 
          Callback<TableColumn.CellDataFeatures<IFileOperationFeedbackWrapper, String>, ObservableValue<String>>
  {

    public cFileNameValue()
    {
    }


    @Override
    public ObservableValue<String> call(TableColumn.CellDataFeatures<IFileOperationFeedbackWrapper, String> oCellItem)
    {
      return oCellItem.getValue().getFileName();
    }

  }

  
  private static class cStatusValue implements 
          Callback<TableColumn.CellDataFeatures<IFileOperationFeedbackWrapper, EFileOperationStatus>, ObservableValue<EFileOperationStatus>>
  {

    public cStatusValue()
    {
    }


    @Override
    public ObservableValue<EFileOperationStatus> 
        call(TableColumn.CellDataFeatures<IFileOperationFeedbackWrapper, EFileOperationStatus> oCellItem)
    {
      return oCellItem.getValue().getStatus();
    }

  }

  
  private static class cStartTimeValue implements 
          Callback<TableColumn.CellDataFeatures<IFileOperationFeedbackWrapper, cTime>, ObservableValue<cTime>>
  {

    public cStartTimeValue()
    {
    }


    @Override
    public ObservableValue<cTime> call(TableColumn.CellDataFeatures<IFileOperationFeedbackWrapper, cTime> oCellItem)
    {
      return oCellItem.getValue().getStartTime();
    }

  }

  
  private static class cEndTimeValue implements 
          Callback<TableColumn.CellDataFeatures<IFileOperationFeedbackWrapper, cTime>, ObservableValue<cTime>>
  {

    public cEndTimeValue()
    {
    }


    @Override
    public ObservableValue<cTime> call(TableColumn.CellDataFeatures<IFileOperationFeedbackWrapper, cTime> oCellItem)
    {
      return oCellItem.getValue().getEndTime();
    }

  }

  
  private static class cProgressValue implements 
          Callback<TableColumn.CellDataFeatures<IFileOperationFeedbackWrapper, Integer>, ObservableValue<Integer>>
  {

    public cProgressValue()
    {
    }


    @Override
    public ObservableValue<Integer> call(TableColumn.CellDataFeatures<IFileOperationFeedbackWrapper, Integer> oCellItem)
    {
      return oCellItem.getValue().getProgress();
    }

  }

  
  private static class cSizeValue implements 
          Callback<TableColumn.CellDataFeatures<IFileOperationFeedbackWrapper, Long>, ObservableValue<Long>>
  {

    public cSizeValue()
    {
    }


    @Override
    public ObservableValue<Long> call(TableColumn.CellDataFeatures<IFileOperationFeedbackWrapper, Long> oCellItem)
    {
      return oCellItem.getValue().getSize();
    }

  }

  
  private static class cPropertyNameValue implements 
          Callback<TableColumn.CellDataFeatures<IStatisticsWrapper, String>, ObservableValue<String>>
  {

    public cPropertyNameValue()
    {
    }


    @Override
    public ObservableValue<String> call(TableColumn.CellDataFeatures<IStatisticsWrapper, String> oCellItem)
    {
      return oCellItem.getValue().getPropertyName();
    }

  }
  
  
  private static class cPropertyTypeValue implements 
          Callback<TableColumn.CellDataFeatures<IStatisticsWrapper, String>, ObservableValue<String>>
  {

    public cPropertyTypeValue()
    {
    }


    @Override
    public ObservableValue<String> call(TableColumn.CellDataFeatures<IStatisticsWrapper, String> oCellItem)
    {
      return oCellItem.getValue().getPropertyType();
    }

  }
  
  
  private static class cPropertyCountValue implements 
          Callback<TableColumn.CellDataFeatures<IStatisticsWrapper, Long>, ObservableValue<Long>>
  {

    public cPropertyCountValue()
    {
    }


    @Override
    public ObservableValue<Long> call(TableColumn.CellDataFeatures<IStatisticsWrapper, Long> oCellItem)
    {
      return oCellItem.getValue().getCount();
    }

  }
  
  
  private static class cFileNameCell extends TableCell<IFileOperationFeedbackWrapper, String>
  {

    public cFileNameCell()
    {
    }


    @Override
    public void updateItem(String sString, boolean bIsEmpty)
    {
      super.updateItem(sString, bIsEmpty);
      if (!bIsEmpty && getTableRow() != null)
      {
        IFileOperationFeedbackWrapper oItem = getTableView().getItems().get(getTableRow().getIndex());
        setText(oItem.getFileName().get());
        Tooltip oTip = new Tooltip(oItem.getFileName().get());
        setTooltip(oTip);
      }
      else
      {
        setText(null);
        setGraphic(null);
      }
    }

  }

  
  private static class cStringCell extends TableCell<Object, String>
  {

    public cStringCell()
    {
    }


    @Override
    public void updateItem(String sString, boolean bIsEmpty)
    {
      super.updateItem(sString, bIsEmpty);
      if (!bIsEmpty)
      {
        setText(sString);
        setTooltip(new Tooltip(sString));
      }
      else
      {
        setText(null);
        setGraphic(null);
      }
    }

  }

  
  private static class cTimeCell extends TableCell<IFileOperationFeedbackWrapper, cTime>
  {
    private final SimpleDateFormat m_oSimpleDateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
    
    public cTimeCell()
    {
    }


    @Override
    public void updateItem(cTime oTime, boolean bIsEmpty)
    {
      super.updateItem(oTime, bIsEmpty);
      if (!bIsEmpty)
      {
        setText(oTime != null ? m_oSimpleDateFormat.format(new Date(oTime.getTime())) : "N/A");
        Tooltip oTip = new Tooltip(oTime != null ? m_oSimpleDateFormat.format(new Date(oTime.getTime())) : "N/A");
        setTooltip(oTip);
      }
      else
      {
        setText(null);
        setGraphic(null);
      }
    }

  }

  
  private static class cStatusCell extends TableCell<IFileOperationFeedbackWrapper, EFileOperationStatus>
  {

    public cStatusCell()
    {
    }


    @Override
    public void updateItem(EFileOperationStatus oStatus, boolean bIsEmpty)
    {
      super.updateItem(oStatus, bIsEmpty);
      if (!bIsEmpty)
      {
        setText(oStatus != null ? oStatus.getNiceName() : "N/A");
        Tooltip oTip = new Tooltip(oStatus != null ? oStatus.getNiceName() : "N/A");
        setTooltip(oTip);
      }
      else
      {
        setText(null);
        setGraphic(null);
      }
    }

  }

  
  private static class cSizeCell extends TableCell<IFileOperationFeedbackWrapper, Long>
  {

    public cSizeCell()
    {
    }


    @Override
    public void updateItem(Long lSize, boolean bIsEmpty)
    {
      super.updateItem(lSize, bIsEmpty);
      if (!bIsEmpty)
      {
        if (lSize == -1)
        {
          setText("Calculating...");
        }
        else
        {
          setText(convert(lSize, false));
        }
//        Tooltip oTip = new Tooltip(oStatus != null ? oStatus.getNiceName() : "N/A");
//        setTooltip(oTip);
      }
      else
      {
        setText(null);
        setGraphic(null);
      }
    }
    
    public static String convert(long bytes, boolean si) 
    {
      int unit = si ? 1000 : 1024;
      if (bytes < unit) return bytes + " B";
      int exp = (int) (Math.log(bytes) / Math.log(unit));
      String pre = (si ? "kMGTPE" : "KMGTPE").charAt(exp-1) + (si ? "" : "i");
      return String.format("%.1f %sB", bytes / Math.pow(unit, exp), pre);
    }

  }
  
  
  private static class cLongCell extends TableCell<Object, Long>
  {
    private final DecimalFormat g_oDecimal_Format = new DecimalFormat("###,###,###,###,###");
    
    public cLongCell()
    {
    }


    @Override
    public void updateItem(Long lSize, boolean bIsEmpty)
    {
      super.updateItem(lSize, bIsEmpty);
      if (!bIsEmpty)
      {
        setText(g_oDecimal_Format.format(lSize));
      }
      else
      {
        setText(null);
        setGraphic(null);
      }
    }
    
  }
  
  private static class cProgressCell extends TableCell<IFileOperationFeedbackWrapper, Integer>
  {

    private final ProgressBar m_oProgressBar = new ProgressBar();
    private final HBox m_oHBox;


    public cProgressCell()
    {
      m_oHBox = new HBox(5.0d, m_oProgressBar);
      m_oHBox.setAlignment(Pos.CENTER);
    }


    @Override
    public void updateItem(Integer iValue, boolean bIsEmpty)
    {
      super.updateItem(iValue, bIsEmpty);
      if (!bIsEmpty)
      {
        m_oProgressBar.setProgress(iValue / 100.0d);
        setGraphic(m_oHBox);
        setContentDisplay(ContentDisplay.GRAPHIC_ONLY);
        Tooltip oTip = new Tooltip(String.format("%d%%", iValue));
        setTooltip(oTip);
      }
      else
      {
        setText(null);
        setGraphic(null);
      }
    }

  }


  private cTableCellFactory()
  {
    throw new AssertionError("cTableCellFactories cannot be instantiated.");
  }

}
