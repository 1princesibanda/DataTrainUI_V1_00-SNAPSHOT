/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gew.fusion.datatrain.ui.addon.view.wizard.Controller.mysqlcontroller;

import com.gew.fusion.datatrain.ui.addon.view.wizard.parsers.antlr4.mysql.tabledefinition.TableDefinitionBaseVisitor;
import com.gew.fusion.datatrain.ui.addon.view.wizard.parsers.antlr4.mysql.tabledefinition.TableDefinitionLexer;
import com.gew.fusion.datatrain.ui.addon.view.wizard.parsers.antlr4.mysql.tabledefinition.TableDefinitionParser;
import java.sql.Types;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 *
 * @author 0400626
 */
public class cTableDefinitionVisitor extends TableDefinitionBaseVisitor<Set<Table>>
{
	//make sure you override the method with the grammar entry point so that it returns a Set of tables

	public static Integer UNKNOWN_TYPE = -1;

	private static final String TINYINT = "TINYINT", SMALLINT = "SMALLINT", YEAR = "YEAR", INT = "INT", BIGINT = "BIGINT", UNSIGNED = "UNSIGNED", FLOAT = "FLOAT", DOUBLE = "DOUBLE", NUMERIC = "NUMERIC", DECIMAL = "DECIMAL", DATETIME = "DATETIME", TIMESTAMP = "TIMESTAMP", TIME = "TIME", DATE = "DATE";
	private static final String EMPTY_STRING = "", SINGLE_WHITE_SPACE = " ";
	private final Set<Table> tableSet;
	private final Map<String, Integer> typesMap; //according to https://docs.oracle.com/javase/8/docs/api/java/sql/Types.html
	String tableName;
	Set<Column> columnSet;
	Set<String> primaryKeySet;
	Table table;
	Integer type;
	String typeName, anyText;
	Boolean autoIncrement;

	public cTableDefinitionVisitor()
	{
		this.tableSet = new HashSet<>();
		this.typesMap = new HashMap<>();
		initTypesMap();
		initNewTable();
		initNewColumn();
	}

	private void initNewTable()
	{
		this.tableName = null;
		this.columnSet = new HashSet<>();
		this.primaryKeySet = new HashSet<>();
	}

		private void initNewColumn()
	{
		this.type = null;
		this.typeName = null;
		this.autoIncrement = null;
		this.anyText = null;
	}
		
	private void initTypesMap()
	{
		assert this.typesMap != null : "Constructor should instantiate the typesMap map, before it may be initialised.";
		this.typesMap.clear();
		this.typesMap.put(cTableDefinitionVisitor.TINYINT, Types.TINYINT);
		this.typesMap.put(cTableDefinitionVisitor.SMALLINT, Types.SMALLINT);
		this.typesMap.put(cTableDefinitionVisitor.YEAR, Types.OTHER); //this is the mapping if the type is database specific. YEAR is specific to mysql
		this.typesMap.put(cTableDefinitionVisitor.INT, Types.INTEGER);
		this.typesMap.put(cTableDefinitionVisitor.BIGINT, Types.BIGINT);
		this.typesMap.put(cTableDefinitionVisitor.UNSIGNED, Types.OTHER); //specific to mysql
		this.typesMap.put(cTableDefinitionVisitor.FLOAT, Types.FLOAT);
		this.typesMap.put(cTableDefinitionVisitor.DOUBLE, Types.DOUBLE);
		this.typesMap.put(cTableDefinitionVisitor.NUMERIC, Types.NUMERIC);
		this.typesMap.put(cTableDefinitionVisitor.DECIMAL, Types.DECIMAL);
		this.typesMap.put(cTableDefinitionVisitor.DATETIME, Types.OTHER); //specific to mysql
		this.typesMap.put(cTableDefinitionVisitor.TIMESTAMP, Types.TIMESTAMP);
		this.typesMap.put(cTableDefinitionVisitor.TIME, Types.TIME);
		this.typesMap.put(cTableDefinitionVisitor.DATE, Types.DATE);
	}

	@Override
	public Set<Table> visitComments(TableDefinitionParser.CommentsContext ctx)
	{
		//System.out.println("**************VisitComments: " + ctx.getText());
		return null;
	}

	//prog:   line+ ;
	@Override
	public Set<Table> visitAllTables(TableDefinitionParser.AllTablesContext ctx)
	{
		System.out.println("**************Visit all tables:" + ctx.getChildCount());
		//visitChildren(ctx);

		//System.out.println("**************Visit all tables..");
		for (int i = 0; i < ctx.getChildCount(); i++)
		{
			//System.out.println("**************Visit " + i + " t:" + ctx.getText());
			visit(ctx.line(i));
			initNewTable();
		}

		return this.tableSet;
	}

	// CREATE ANYTEXT tableName (columnDefinitionList) PRIMARY KEY (columnNameList) COLON ;
	@Override
	public Set<Table> visitTableDefinition(TableDefinitionParser.TableDefinitionContext ctx)
	{
		System.out.println("**************Table def");
		this.tableName = ctx.tableName().getText();
		visit(ctx.columnDefinitionList());
		//visit(ctx.columnNameList());
		Table table = new Table(this.tableName, this.columnSet, this.primaryKeySet);
		this.tableSet.add(table);
		return null;
	}

	/*
	@Override
	public Set<Table> visitSqlDataType(TableDefinitionParser.SqlDataTypeContext ctx)
	{
		System.out.println("**************My sql type:" + ctx.getText()+" precisitiontype:"+(ctx.precisionType()==null? "null" : ctx.precisionType().getText())+" directtype:"+(ctx.directType() == null? "null":ctx.directType().getText()) );
		visitChildren(ctx);
		return null;
	}
	*/
	

	// columnDefinition:   ID sqlDataType ANYTEXT ;
	@Override
	public Set<Table> visitColumnDefinition(TableDefinitionParser.ColumnDefinitionContext ctx)
	{
		System.out.println("**************Column def:"+ctx.getText());
		String name = ctx.ID().getText();
		visit(ctx.sqlDataType());
		String defaultValue = null; 

		this.anyText = cTableDefinitionVisitor.EMPTY_STRING;
		if (ctx.AUTO_INCREMENT() != null)
		{
			this.anyText += cTableDefinitionVisitor.SINGLE_WHITE_SPACE + ctx.AUTO_INCREMENT().getText();
			this.autoIncrement = true;
		}
		else
			this.autoIncrement = false;
		
		if (ctx.NOT() != null)
		{
			assert ctx.NULL() != null : "If 'not' present, then 'null' should be too.";
			//anyText = anyText == null ? ctx.NOT().getText() : cTableDefinitionVisitor.SINGLE_WHITE_SPACE + anyText + cTableDefinitionVisitor.SINGLE_WHITE_SPACE +(ctx.NOT().getText());
			this.anyText += cTableDefinitionVisitor.SINGLE_WHITE_SPACE +(ctx.NOT().getText()) + cTableDefinitionVisitor.SINGLE_WHITE_SPACE + ctx.NULL().getText();
			//anyText += cTableDefinitionVisitor.SINGLE_WHITE_SPACE + ctx.NULL().getText();
		}
		if (ctx.DEFAULT() != null)
		{
			defaultValue = cTableDefinitionVisitor.SINGLE_WHITE_SPACE + ctx.literal().getText();
			this.anyText += cTableDefinitionVisitor.SINGLE_WHITE_SPACE + (ctx.DEFAULT().getText() + defaultValue);
		}

		assert this.typeName != null : "TypeName should be set (maybe by visitDirectType() ?) before column object may be created.";
		assert this.type != null : "Type should be set (maybe by visitDirectType() ?) before column object may be created.";
		assert name != null : "Column's name should be set (maybe by visitColumnDefinition() ?) before column object may be created.";
		assert this.autoIncrement != null : "Column's autoIncrement should be set (maybe by visitColumnDefinition() ?) before column object may be created.";
		assert this.anyText != null : "anyText is optional but should be set to at least blank string before column object may be created.";

		Column c = new Column(name, this.type, this.typeName, this.autoIncrement, this.anyText);
		this.columnSet.add(c);
		initNewColumn(); //clean - up, ie get ready for new column in case there is any, by clearing all the fields
		
		return null;
	}

	//directType: TINYINT | SMALLINT | YEAR | INT_ | BIGINT | UNSIGNED | FLOAT | DOUBLE | NUMERIC | DECIMAL | DATETIME | TIMESTAMP | TIME | DATE | BOOLEAN ;
	@Override
	public Set<Table> visitDirectType(TableDefinitionParser.DirectTypeContext ctx)
	{
		this.typeName = ctx.getText(); //map types to arbitrary numbers and the user must request the map later and re-map. Or map the text to one of the Type class types, according to mysql mysql type to java type mapping, which form part of the documentation for this software.
		assert this.typeName != null : "Type should be set, otherwise there is a grammatical error and parser should have already complained with vicinity of fowl line number and character.";
		this.typeName = this.typeName.toUpperCase();
		System.out.println("**************directType: " + this.typeName + "");
		this.type = this.typesMap.get(typeName);
		if (this.type == null)
		{
			System.out.println("cTableDefinitionVisitor: Warning: Unexpected type '" + typeName + "' found. Assuming the type is database specific and accepting.");
			this.typesMap.put(typeName, Types.OTHER);
		}
		this.type = this.typesMap.get(typeName);
		assert this.type != null : "Column type should be a number, not a null. If type unknown, should accept as database specific.";
		this.type = this.typesMap.get(typeName);
		assert this.typeName != null : "Type should be set, otherwise there is a grammatical error and parser should have already complained with vicinity of fowl line number and character.";
		return null;
	}

	//precisionType: ID OPENINGROUNDBRACE NUMERIC_LITERAL CLOSINGROUNDBRACE ;
	@Override
	public Set<Table> visitPrecisionType(TableDefinitionParser.PrecisionTypeContext ctx)
	{
		System.out.println("**************visitPrecisionType ");
		Object bitVarchar = ctx.BIT();

		//These must be true regardless of whether bit or varchar precision type- a precision type must state the type and provide a numeric precision argument
		assert ctx.OPENINGROUNDBRACE() != null : "It is grammatically incorrect for '(' to not be present. Parser should not accept and issue error.";
		assert ctx.NUMERIC_LITERAL() != null : "It is grammatically incorrect for number to not be present. Parser should not accept and issue error.";
		assert ctx.CLOSINGROUNDBRACE() != null : "It is grammatically incorrect for ')' to not be present. Parser should not accept and issue error.";

		this.typeName = ctx.OPENINGROUNDBRACE().getText() + ctx.NUMERIC_LITERAL().getText() + ctx.CLOSINGROUNDBRACE().getText();

		if (bitVarchar != null)
		{
			this.typeName = ctx.BIT().getText() + this.typeName;
			this.type = this.typesMap.get(this.typeName);
			if (this.type == null)
				this.typesMap.put(this.typeName, Types.BIT);
		}
		else
		{
			bitVarchar = ctx.VARCHAR();
			assert bitVarchar != null : "Either the bit precision type or the varchar precision type expected.";
			this.typeName = ctx.VARCHAR().getText() + this.typeName;
			this.type = this.typesMap.get(this.typeName);
			if (this.type == null)
				this.typesMap.put(this.typeName, Types.VARCHAR);
		}

		this.type = this.typesMap.get(this.typeName);
		assert this.type != null : "Column type should be a number, not a null. If type unknown, should accept as database specific.";
		assert this.typeName != null : "typeName should be set to empty string in the very least, but never the initialising null value, otherwise we understand method has neglected to assign value for name of type.";
		return null;
	}

//columnNameList: ID 
	@Override
	public Set<Table> visitPrimaryKeyColumn(TableDefinitionParser.PrimaryKeyColumnContext ctx)
	{
		System.out.println("**************visitPrimaryKeyColumn ");
		String name = ctx.ID().getText();
		this.primaryKeySet.add(name);
		
		return null;
	}
	/*
	public Set<Table> visitColumnDefinitionList(TableDefinitionParser.ColumnDefinitionListContext ctx)
	{
		ctx.getText();
	}
	 */
}
