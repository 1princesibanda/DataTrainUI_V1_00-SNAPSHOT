lexer grammar CommonLexerRules; // note "lexer grammar"

ID  :   [a-zA-Z_]+ ;      // match identifiers
INT :   [0-9]+ ;         // match integers
//NEWLINE:'\r'? '\n' ;     // return newlines to parser (end-statement signal)
WS  :   [ \r\n\t]+ -> skip ; // toss out whitespace

