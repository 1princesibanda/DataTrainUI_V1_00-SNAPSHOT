/**
 * Copyright (c) 2019 GEW Technologies (GEW). All rights reserved.
 */
package com.gew.fusion.datatrain.ui.addon.view;

import com.gew.graphdata.context.event.model.model.cAddOrUpdateResponseEvent;
import com.gew.lib.context.cEventMetaData;
import com.gew.lib.context.cUpdateListener;
import com.gew.lib.context.interfaces.IContext;
import com.gew.persistence.datatypes.IContainer;
import com.gew.persistence.datatypes.IEntity;
import com.mrcm.ifs.lib.ui.screencapgenerator.impl.cScreenCapGenerator;
import java.util.ArrayList;
import javafx.scene.Node;

/**
 *
 * @author Philip M. Trenwith
 */
public class cScreenCapture
{

  /**
   * Context.
   */
  private final IContext m_oContext;

  public cScreenCapture(IContext oContext)
  {
    m_oContext = oContext;
  }

  /**
   * Capture and send a screen capture
   *
   * @param oNode the node to capture
   */
  public void capture(Node oNode)
  {
    if (oNode != null)
    {
      //Generate the screenshot
      IEntity screencap = cScreenCapGenerator.createScreenCap(oNode);

      if (m_oContext != null)
      {
        m_oContext.updateAndGo(new cUpdateListener<cAddOrUpdateResponseEvent>()
        {
          @Override
          public void onUpdate(cAddOrUpdateResponseEvent event, cEventMetaData metadata)
          {
            super.onUpdate(event, metadata);

            ArrayList<IContainer> containers = new ArrayList();

            containers.add(screencap);

            event.setObjects(containers);
          }
        });
      }
    }
  }
}
