/**
 * Copyright (c) 2019 GEW Technologies (GEW). All rights reserved.
 */
package com.gew.fusion.datatrain.ui.addon.view.dashboard.charts;

import com.gew.ifs.ext.datatrain.lib.chats.cChartMetadata;
import com.gew.ifs.ext.datatrain.lib.statistics.IStatistics;
import java.util.Map;
import javafx.scene.chart.BarChart;
import javafx.scene.chart.CategoryAxis;
import javafx.scene.chart.Chart;
import javafx.scene.chart.LineChart;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.XYChart;

/**
 *
 * @author Philip M. Trenwith
 */
public class cFXChart
{

  public enum ChartType
  {
    BAR_CHART,
    LINE_CHART
  }

  public Chart getChart(ChartType oType, cChartMetadata oChartMetadata, IStatistics... oStatsList)
  {
    Chart oChart = null;
    switch (oType)
    {
      case BAR_CHART:
      {
        oChart = createBarChart(oChartMetadata, oStatsList);
        break;
      }
      case LINE_CHART:
      {
        oChart = createLineChart(oChartMetadata, oStatsList);
        break;
      }
    }

    return oChart;
  }
  
  public BarChart createBarChart(cChartMetadata oChartMetadata, IStatistics... oStatsList)
  {
    CategoryAxis xAxis = new CategoryAxis();
    xAxis.setLabel(oChartMetadata.getXAxisLabel() + " (" + oChartMetadata.getXAxisRangeLabel() + ")");

    int iStatSeriesLength = oStatsList.length;

    long lLowest = Long.MAX_VALUE;
    long lHighest = 0;
    for (int i = 0; i < iStatSeriesLength; i++)
    {
      IStatistics oStats = oStatsList[i];
      long lTempHighest = oStats.getHighestValue();
      if (lTempHighest > lHighest)
      {
        lHighest = lTempHighest;
      }
      
      long lTempLowest = oStats.getLowestValue();
      if (lTempLowest < lLowest)
      {
        lLowest = lTempLowest;
      }
    }

    int iIncrement = (int) lHighest / 10;
    NumberAxis yAxis = new NumberAxis((double) lLowest, (double) lHighest, iIncrement);
    yAxis.setLabel(oChartMetadata.getYAxisLabel() + " (" + oChartMetadata.getYAxisRangeLabel() + ")");

    BarChart oChart = new BarChart(xAxis, yAxis);
    oChart.setTitle(oChartMetadata.getTypeLabel());
    oChart.setLegendVisible(iStatSeriesLength>1);

    for (int i = 0; i < iStatSeriesLength; i++)
    {
      IStatistics oStats = oStatsList[i];
      
      XYChart.Series oSeries = new XYChart.Series();
      oSeries.setName(oStats.getName());
    
      Map<Object, Long> typeKeyValueStatistics = oStats.getTypeKeyValueStatistics();
      typeKeyValueStatistics.keySet().forEach((key) ->
      {
        Long lValue = typeKeyValueStatistics.get(key);
        oSeries.getData().add(new XYChart.Data((String) (key + ""), lValue));
      });
      
      oChart.getData().addAll(oSeries);
    }

    
    oChart.setAnimated(false);
    return oChart;
  }

  public LineChart createLineChart(cChartMetadata oChartMetadata, IStatistics... oStatsList)
  {
    CategoryAxis xAxis = new CategoryAxis();
    xAxis.setLabel(oChartMetadata.getXAxisLabel() + " (" + oChartMetadata.getXAxisRangeLabel() + ")");
    int iStatSeriesLength = oStatsList.length;

    long lHighest = 0;
    for (int i = 0; i < iStatSeriesLength; i++)
    {
      IStatistics oStats = oStatsList[i];
      long lTempHighest = oStats.getHighestValue();
      if (lTempHighest > lHighest)
      {
        lHighest = lTempHighest;
      }
    }

    int iIncrement = (int) lHighest / 10;
    NumberAxis yAxis = new NumberAxis(0, (double) lHighest, iIncrement);

    yAxis.setLabel(oChartMetadata.getYAxisLabel() + " (" + oChartMetadata.getYAxisRangeLabel() + ")");

    LineChart oChart = new LineChart(xAxis, yAxis);
    oChart.setTitle(oChartMetadata.getTypeLabel());
    oChart.setLegendVisible(iStatSeriesLength>1);

    for (int i = 0; i < iStatSeriesLength; i++)
    {
      IStatistics oStats = oStatsList[i];
      
      XYChart.Series oSeries = new XYChart.Series();
      oSeries.setName(oStats.getName());
      
      Map<Object, Long> typeKeyValueStatistics = oStats.getTypeKeyValueStatistics();
      typeKeyValueStatistics.keySet().forEach((key) ->
      {
        Long lValue = typeKeyValueStatistics.get(key);
        oSeries.getData().add(new XYChart.Data((String) (key + ""), lValue));
      });
      oChart.getData().addAll(oSeries);
    }

    
    oChart.setAnimated(false);
    return oChart;
  }
}
