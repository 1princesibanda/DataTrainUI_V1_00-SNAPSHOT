/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gew.fusion.datatrain.ui.addon.view.wizard.com.gew.datatrain.ui;

import com.gew.fusion.datatrain.ui.addon.view.wizard.Controller.mysqlcontroller.IApplication;
import com.gew.fusion.datatrain.ui.addon.view.wizard.Controller.mysqlcontroller.IController;
import com.gew.fusion.datatrain.ui.addon.view.wizard.Controller.mysqlcontroller.cMySQLController;
import com.gew.fusion.datatrain.ui.addon.view.wizard.com.gew.datatrain.ui.cView4.Properties;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Application;
import static javafx.application.Application.launch;
import javafx.application.Platform;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.concurrent.Task;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.geometry.Bounds;
import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.geometry.VPos;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextArea;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.Pane;
import javafx.scene.layout.Region;
import javafx.scene.text.Font;
import javafx.scene.text.FontPosture;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import javafx.stage.Window;
import javafx.stage.WindowEvent;

/**
 *
 * @author 0400626
 */
public final class cView3 extends IDataTrainView implements EventHandler<Event>, IApplication
{

	private static final String DEFAULT_OUTPUT_TABLE_NAME = "tblProperties", DEFAULT_QUERY_STRING_TABLE_NAME = "tblQueries", DEFAULT_MAP_TABLE_NAME = "tblMaps", DEFAULT_MAPS_TABLE_MAP_NAME_COLUMN_NAME = "mapName";
	private static final String DEFAULT_INPUT_TABLE_NAME = "tblPropertiesInput";
	private static final List<String> DEFAULT_OUTPUT_TABLE_COLUMN_NAMES = Arrays.asList("propertyId", "columnName", "fileReference", "type", "mapName"), DEFAULT_QUERY_STRING_COLUMN_NAMES_LIST = Arrays.asList("querySql");
	private static final int COLUMN_TYPE_RECOMMENDATION_NOT_POSSIBLE = -1;
	private static final int LIST_TOSTRING_INDEX_OF_FIRST_NONBRACKET_CHAR = 1;
	private static final int ZERO_INDEX_OFFSET = 1;
	private final int SMALLEST_RS_COLUMN_INDEX = 1;

	public final int CONTROLLER_CREATION_FAILED = 1;
	public final int CONTROLLER_READ_FAILED = 2;

	private boolean INITIALISED;

	Pane pane;
	Scene scene;
	IController controller;

	Bounds bounds;
	Text scenetitle;
	Label columnDataLabel, queryColumnsLabel, propertiesLabel, fileReferenceLabel, comboBoxLabel, mapNameLabel, mapLabel;
	ListView<String> queryColumnsListView, columnDataListView, mapsListView;
	ObservableList<String> queryColumnsList, columnDataList, comboBoxOptionsList, mapNamesList;
	ObservableList<Properties2> tableData;
	List<String> arbitraryTablesColumnsList, arbitraryTablesColumnsTypesList, columnDataLinkedList, comboBoxOptionsLinkedList;
	TableView<Properties2> propertiesTable;
	TableColumn columnNameColumn, fileReferenceColumn, typeColumn;
	Button nextButton, backButton, removeButton, addButton, loadMapButton, deleteMapButton, loadMapsFileButton;
	CheckBox fileReferenceCheckBox;
	ComboBox typesComboBox;
	Task<Void> task;
	String outputTableName, inputTableName, queryStringTableName, mapsTableName, mapsTableMapNameColumnName, mapName;
	Map<String, String> javaTypesMap;
	Map<String, List<String>> readOut;
	PropertyValueFactory<Properties2, String> columnNamePropertyFactory, fileReferencePropertyFactory, typePropertyFactory;
	List<String> outputTableColumns, queryStringColumnNamesList, arbitraryTablesNamesList;
	TextArea mapNameTextArea;
	Thread thread;
	String qrStr = null;

	public cView3()
	{
		this.INITIALISED = false;

		this.pane = new Pane();
		this.scene = null; //new Scene(this.pane, 1300, 500);

		this.scenetitle = new Text("Data selection SQL");

		this.mapLabel = new Label("MAP NAMES");
		this.queryColumnsLabel = new Label("QUERY COLUMNS");
		this.columnDataLabel = new Label("COLUMN DATA");
		this.propertiesLabel = new Label("PROPERTIES");
		this.fileReferenceLabel = new Label("Is File reference?");
		this.comboBoxLabel = new Label("Primitive Java Type");
		this.mapNameLabel = new Label("Enter New Map Name:");

		this.mapNameTextArea = new TextArea();

		this.queryColumnsListView = new ListView<>();
		this.columnDataListView = new ListView<>();
		this.mapsListView = new ListView<>();

		this.arbitraryTablesColumnsList = new LinkedList<String>();
		this.arbitraryTablesColumnsTypesList = null;
		this.columnDataLinkedList = new LinkedList<String>();
		this.comboBoxOptionsLinkedList = new LinkedList<String>();

		this.propertiesTable = new TableView<Properties2>();//Dont want to necessarily write a Property3 class? DB already knows structure of data

		this.columnNameColumn = new TableColumn("Column Name");
		this.fileReferenceColumn = new TableColumn("File Reference");
		this.typeColumn = new TableColumn("Type");
		this.tableData = FXCollections.observableArrayList();

		this.nextButton = new Button("Next");
		this.backButton = new Button("Back");
		this.removeButton = new Button("Remove Property");
		this.addButton = new Button("Add Property");
		this.loadMapButton = new Button("LoadMap");
		this.deleteMapButton = new Button("DeleteMap");
		this.loadMapsFileButton = new Button("LoadMapsFile");

		super.applicationsSet = new HashSet<>();

		this.fileReferenceCheckBox = new CheckBox();

		this.typesComboBox = new ComboBox();

		this.javaTypesMap = new HashMap<>();

		this.readOut = null;

		this.queryStringTableName = null;
		this.queryStringColumnNamesList = null;
		this.arbitraryTablesNamesList = null;
		this.mapsTableName = null;
		this.mapsTableMapNameColumnName = null;
		this.mapName = null;

		this.columnNamePropertyFactory = new PropertyValueFactory<>("ColumnName");
		this.fileReferencePropertyFactory = new PropertyValueFactory<>("FileReference");
		this.typePropertyFactory = new PropertyValueFactory<>("Type");

		this.outputTableName = null;
		this.inputTableName = null;
		this.outputTableColumns = null;

		this.controller = null;
//
		this.task = newTask();

		super.nextView = null;
		this.previousView = null;
	}

	public cView3(IController controller)
	{
		this();
		this.controller = controller;
	}

	public cView3(IController controller, IDataTrainView previousView, IDataTrainView nextView)
	{
		this();
		this.controller = controller;
		this.previousView = previousView;
		super.nextView = nextView;
		System.out.println("View3 Setting controller..." + this.controller);
	}

	@Override
	public void init()
	{
		this.INITIALISED = true;

		this.scenetitle.setFont(Font.font("Tahoma", FontWeight.NORMAL, FontPosture.REGULAR, 20));
		this.scenetitle.setId(IDataTrainView.uniqueId());
		this.scenetitle.setX(700);
		this.scenetitle.setY(50);

		this.queryColumnsList = FXCollections.observableArrayList();
		this.columnDataList = FXCollections.observableArrayList();
		this.comboBoxOptionsList = FXCollections.observableArrayList();
		this.mapNamesList = FXCollections.observableArrayList();

		bounds = this.mapLabel.getLayoutBounds();
		this.mapLabel.setDisable(true);
		Region.positionInArea(this.mapLabel, 100, 130, this.bounds.getWidth(), this.bounds.getHeight(), 0, Insets.EMPTY, HPos.LEFT, VPos.BASELINE, true);
		this.mapLabel.setId(IDataTrainView.uniqueId());

		bounds = this.mapNameLabel.getLayoutBounds();
		Region.positionInArea(this.mapNameLabel, 100, 80, this.bounds.getWidth(), this.bounds.getHeight(), 0, Insets.EMPTY, HPos.LEFT, VPos.BASELINE, true);
		this.mapNameLabel.setDisable(true);
		this.mapNameLabel.setId(IDataTrainView.uniqueId());

		bounds = this.queryColumnsLabel.getLayoutBounds();
		Region.positionInArea(this.queryColumnsLabel, 400, 130, this.bounds.getWidth(), this.bounds.getHeight(), 0, Insets.EMPTY, HPos.LEFT, VPos.BASELINE, true);
		this.queryColumnsLabel.setId(IDataTrainView.uniqueId());

		this.columnDataLabel.getLayoutBounds();
		Region.positionInArea(this.columnDataLabel, 700, 130, this.bounds.getWidth(), this.bounds.getHeight(), 0, Insets.EMPTY, HPos.LEFT, VPos.BASELINE, true);
		this.columnDataLabel.setId(IDataTrainView.uniqueId());

		bounds = this.propertiesLabel.getLayoutBounds();
		Region.positionInArea(this.propertiesLabel, 1150, 130, this.bounds.getWidth(), this.bounds.getHeight(), 0, Insets.EMPTY, HPos.LEFT, VPos.BASELINE, true);
		this.propertiesLabel.setId(IDataTrainView.uniqueId());

		bounds = this.fileReferenceLabel.getLayoutBounds();
		Region.positionInArea(this.fileReferenceLabel, 1020, 245, this.bounds.getWidth(), this.bounds.getHeight(), 0, Insets.EMPTY, HPos.LEFT, VPos.BASELINE, true);
		this.fileReferenceLabel.setId(IDataTrainView.uniqueId());

		bounds = this.comboBoxLabel.getLayoutBounds();
		Region.positionInArea(this.comboBoxLabel, 1000, 185, this.bounds.getWidth(), this.bounds.getHeight(), 0, Insets.EMPTY, HPos.LEFT, VPos.BASELINE, true);
		this.comboBoxLabel.setId(IDataTrainView.uniqueId());

		this.bounds = this.mapNameTextArea.getLayoutBounds();
		Region.positionInArea(this.mapNameTextArea, 240, 70, this.bounds.getWidth(), this.bounds.getHeight(), 0, Insets.EMPTY, HPos.LEFT, VPos.BASELINE, true);
		this.mapNameTextArea.setPrefSize(200, 10);
		this.mapNameTextArea.setId(IDataTrainView.uniqueId());
		this.mapNameTextArea.setOnKeyTyped(this);
		this.mapNameTextArea.setDisable(false);
		this.mapNameTextArea.setPromptText("Enter map name here.");

		this.mapsListView.setItems(this.mapNamesList);
		this.mapsListView.setDisable(true);
		this.bounds = this.mapsListView.getLayoutBounds();
		this.mapsListView.setPrefSize(240, 300);
		this.mapsListView.setId(IDataTrainView.uniqueId());
		Region.positionInArea(this.mapsListView, 100, 160, this.bounds.getWidth(), this.bounds.getHeight(), 0, Insets.EMPTY, HPos.LEFT, VPos.BASELINE, true);
		this.mapsListView.setDisable(true);
		this.mapsListView.setOnMouseClicked(this);

		this.queryColumnsListView.setItems(this.queryColumnsList);
		this.bounds = this.queryColumnsListView.getLayoutBounds();
		queryColumnsListView.setPrefSize(240, 300);
		this.queryColumnsListView.setId(IDataTrainView.uniqueId());
		Region.positionInArea(this.queryColumnsListView, 400, 160, 100, 100, 0, Insets.EMPTY, HPos.LEFT, VPos.BASELINE, true);
		this.queryColumnsListView.setOnMouseClicked(this);

		this.columnDataListView.setItems(this.columnDataList);
		this.bounds = this.columnDataListView.getLayoutBounds();
		columnDataListView.setPrefSize(240, 300);
		this.columnDataListView.setId(IDataTrainView.uniqueId());
		Region.positionInArea(this.columnDataListView, 700, 160, this.bounds.getWidth(), this.bounds.getHeight(), 0, Insets.EMPTY, HPos.LEFT, VPos.BASELINE, true);
		this.columnDataListView.setOnMouseClicked(this);

		bounds = this.typesComboBox.getLayoutBounds();
		Region.positionInArea(this.typesComboBox, 1000, 210, this.bounds.getWidth(), this.bounds.getHeight(), 0, Insets.EMPTY, HPos.LEFT, VPos.BASELINE, true);
		this.typesComboBox.setPrefSize(100, 10);
		this.typesComboBox.setOnMouseClicked(this);
		this.typesComboBox.setId(IDataTrainView.uniqueId());
		this.typesComboBox.setVisible(true);
		this.typesComboBox.setItems(this.comboBoxOptionsList);

		bounds = this.fileReferenceCheckBox.getLayoutBounds();
		Region.positionInArea(this.fileReferenceCheckBox, 1000, 245, this.bounds.getWidth(), this.bounds.getHeight(), 0, Insets.EMPTY, HPos.LEFT, VPos.BASELINE, true);
		this.fileReferenceCheckBox.setOnMouseClicked(this);
		this.fileReferenceCheckBox.setId(IDataTrainView.uniqueId());

		this.nextButton.setDisable(true);
		this.nextButton.setId(IDataTrainView.uniqueId());
		this.backButton.setDisable(false);
		this.backButton.setId(IDataTrainView.uniqueId());
		this.removeButton.setDisable(true);
		this.removeButton.setId(IDataTrainView.uniqueId());
		this.addButton.setDisable(true);
		this.addButton.setId(IDataTrainView.uniqueId());
		this.loadMapButton.setDisable(true);
		this.loadMapButton.setId(IDataTrainView.uniqueId());
		this.deleteMapButton.setDisable(true);
		this.deleteMapButton.setId(IDataTrainView.uniqueId());

		bounds = this.nextButton.getLayoutBounds();
		Region.positionInArea(this.nextButton, 1400, 470, this.bounds.getWidth(), this.bounds.getHeight(), 0, Insets.EMPTY, HPos.LEFT, VPos.BASELINE, true);
		this.nextButton.setOnMouseClicked(this);
		this.nextButton.setId(IDataTrainView.uniqueId());

		bounds = this.backButton.getLayoutBounds();
		Region.positionInArea(this.backButton, 1350, 470, this.bounds.getWidth(), this.bounds.getHeight(), 0, Insets.EMPTY, HPos.LEFT, VPos.BASELINE, true);
		this.backButton.setOnMouseClicked(this);

		bounds = this.addButton.getLayoutBounds();
		Region.positionInArea(this.addButton, 1000, 270, this.bounds.getWidth(), this.bounds.getHeight(), 0, Insets.EMPTY, HPos.LEFT, VPos.BASELINE, true);
		this.addButton.setOnMouseClicked(this);
		this.addButton.setId(IDataTrainView.uniqueId());

		bounds = this.removeButton.getLayoutBounds();
		Region.positionInArea(this.removeButton, 1000, 305, this.bounds.getWidth(), this.bounds.getHeight(), 0, Insets.EMPTY, HPos.LEFT, VPos.BASELINE, true);
		this.removeButton.setOnMouseClicked(this);

		bounds = this.loadMapButton.getLayoutBounds();
		Region.positionInArea(this.loadMapButton, 1000, 470, this.bounds.getWidth(), this.bounds.getHeight(), 0, Insets.EMPTY, HPos.LEFT, VPos.BASELINE, true);
		this.loadMapButton.setOnMouseClicked(this);
		this.loadMapButton.setId(cView1.uniqueId());

		bounds = this.deleteMapButton.getLayoutBounds();
		Region.positionInArea(this.deleteMapButton, 1100, 470, this.bounds.getWidth(), this.bounds.getHeight(), 0, Insets.EMPTY, HPos.LEFT, VPos.BASELINE, true);
		this.deleteMapButton.setOnMouseClicked(this);
		this.deleteMapButton.setId(cView1.uniqueId());

		bounds = this.loadMapsFileButton.getLayoutBounds();
		Region.positionInArea(this.loadMapsFileButton, 1200, 470, this.bounds.getWidth(), this.bounds.getHeight(), 0, Insets.EMPTY, HPos.LEFT, VPos.BASELINE, true);
		this.loadMapsFileButton.setOnMouseClicked(this);
		this.loadMapsFileButton.setId(cView1.uniqueId());

		bounds = this.propertiesTable.getLayoutBounds();
		this.propertiesTable.setPrefSize(300, 300);
		Region.positionInArea(this.propertiesTable, 1150, 160, this.bounds.getWidth(), this.bounds.getHeight(), 0, Insets.EMPTY, HPos.LEFT, VPos.BASELINE, true);
		this.propertiesTable.setOnMouseClicked(this);
		this.propertiesTable.setId(IDataTrainView.uniqueId());
		this.propertiesTable.setEditable(false);
		this.propertiesTable.setVisible(true);
		this.propertiesTable.setItems(this.tableData);
		this.propertiesTable.getColumns().setAll(this.columnNameColumn, this.fileReferenceColumn, this.typeColumn);

		this.columnNameColumn.setResizable(true);
		this.columnNameColumn.setPrefWidth(150);
		this.columnNameColumn.setCellValueFactory(this.columnNamePropertyFactory);
		this.columnNameColumn.setId(IDataTrainView.uniqueId());
		this.fileReferenceColumn.setResizable(true);
		this.fileReferenceColumn.setPrefWidth(100);
		this.fileReferenceColumn.setCellValueFactory(this.fileReferencePropertyFactory);
		this.fileReferenceColumn.setId(IDataTrainView.uniqueId());
		this.typeColumn.setResizable(true);
		this.typeColumn.setPrefWidth(50);
		this.typeColumn.setCellValueFactory(this.typePropertyFactory);
		this.typeColumn.setId(IDataTrainView.uniqueId());

		this.comboBoxOptionsLinkedList.add("boolean");
		this.comboBoxOptionsLinkedList.add("byte");
		this.comboBoxOptionsLinkedList.add("short");
		this.comboBoxOptionsLinkedList.add("int");
		this.comboBoxOptionsLinkedList.add("long");
		this.comboBoxOptionsLinkedList.add("float");
		this.comboBoxOptionsLinkedList.add("double");
		this.comboBoxOptionsLinkedList.add("byte[ ]");
		this.comboBoxOptionsLinkedList.add("char");
		this.comboBoxOptionsLinkedList.add("string");
		this.comboBoxOptionsList.setAll(this.comboBoxOptionsLinkedList);

		this.javaTypesMap.put("java.lang.String", "string");
		this.javaTypesMap.put("java.lang.Boolean", "boolean");
		this.javaTypesMap.put("java.lang.Char", "char");
		this.javaTypesMap.put("java.lang.Integer", "int");
		this.javaTypesMap.put("java.lang.Byte", "byte");
		this.javaTypesMap.put("java.lang.Float", "float");
		this.javaTypesMap.put("java.lang.Double", "double");
		this.javaTypesMap.put("java.lang.Short", "short");

		this.queryStringTableName = this.queryStringTableName == null ? cView3.DEFAULT_QUERY_STRING_TABLE_NAME : this.queryStringTableName;
		this.queryStringColumnNamesList = this.queryStringColumnNamesList == null ? cView3.DEFAULT_QUERY_STRING_COLUMN_NAMES_LIST : this.queryStringColumnNamesList;
		this.outputTableColumns = this.outputTableColumns == null ? cView3.DEFAULT_OUTPUT_TABLE_COLUMN_NAMES : this.outputTableColumns;
		this.outputTableName = this.outputTableName == null ? cView3.DEFAULT_OUTPUT_TABLE_NAME : this.outputTableName;
		this.inputTableName = this.inputTableName == null ? cView3.DEFAULT_INPUT_TABLE_NAME : this.inputTableName;
		this.mapsTableName = this.mapsTableName == null ? cView3.DEFAULT_MAP_TABLE_NAME : this.mapsTableName;
		this.mapsTableMapNameColumnName = this.mapsTableMapNameColumnName == null ? cView3.DEFAULT_MAPS_TABLE_MAP_NAME_COLUMN_NAME : this.mapsTableMapNameColumnName;

		super.applicationsSet.add(this);

		if (this.controller != null)
			this.controller.setApplications(super.applicationsSet);

		this.thread = null;
	}

	//make new scene using the pane
	@Override
	public void start(Stage stage)
	{
		assert stage != null : "This method is a start point for by Java fx for this application, and it is expected that fx always calls this method with non null stage argument.";

		if (!this.INITIALISED)
			this.init();
		assert this.INITIALISED;
		//configure the stage
		stage.setTitle("Text Data Format Converter");

		if (this.runningMap != null)
		{
			loadMap(cView3.runningMap);
		}
		/*
		this.queryColumnsList.clear();
		this.columnDataList.clear();
		this.tableData.clear();
		this.arbitraryTablesColumnsList.clear();
		this.columnDataLinkedList.clear();
		 */
		//initColumnDataListView();
		//this.nextButton.setDisable(false);
		loadMapNamesView();
		stage.setMaxHeight(550);
		stage.setMaxWidth(1600);
		stage.setMinHeight(550);
		stage.setMinWidth(1600);
		stage.setFullScreen(false);
		stage.setResizable(false); //Toggles availability of Windows minimise and maximise. if set to false and setMaximized() is set, then it is fixed to the setting of setMaximized()
		stage.setMaximized(false);
		//stage.toFront(); Not really data- these are meant for use at runtime
		stage.setIconified(false); //Puts the window down at the bottom task bar where the open programs are. Takes precedence over toBack() and toFront();
		stage.setFullScreenExitHint("This is exit hint");
		stage.setAlwaysOnTop(false);

		this.stage = stage;
		this.pane.getChildren().setAll(this.scenetitle, this.mapLabel, this.columnDataLabel, this.queryColumnsLabel, this.mapNameLabel, this.propertiesLabel, this.comboBoxLabel, this.fileReferenceLabel, this.mapNameTextArea, this.mapsListView, this.columnDataListView, this.queryColumnsListView, this.nextButton, this.backButton, this.loadMapButton, this.deleteMapButton, this.loadMapsFileButton, this.fileReferenceCheckBox, this.addButton, this.removeButton, this.typesComboBox, this.propertiesTable);

		this.thread = new Thread(this.task);
		this.thread.setDaemon(true);
		this.thread.start();

		if (stage.getScene() == null)
		{
			this.scene = new Scene(this.pane, 1300, 500);
			stage.setScene(this.scene);
			stage.show();//remove when take out main method
		}
		else
		{
			this.scene = stage.getScene();
		}

		if (IDataTrainView.paneManager != null)
		{
			if (IDataTrainView.paneManager instanceof BorderPane)
				((BorderPane) IDataTrainView.paneManager).setCenter(this.pane);
		}

		this.scene.getWindow().setOnCloseRequest(new EventHandler<WindowEvent>()
		{
			@Override
			public void handle(WindowEvent e)
			{
				cView3.this.handle(e);
			}
		});
	}

	public void initWidget(Object widget, Object data, String attributesJSon)
	{
		//Could make method parameterised so that it takes the widget type e.g Button and data type e.g LinkedList as type arguments so that we dont have to type-cast
		//The attributes that should be set for widget come in as a key-values in a JSon string. A JSon parser would be required to unpack this. Also, if just key-values only required, should we just use a map object? 
		//Think about json string argument for attributes considering config that might be required to set all properties of each widget.
		//If JSon can pass values for the generic attributes that are independent of widget, then an internal map to map the generic attibute names to names that FX widgets actually have
		//We could initially have initialisation of each object type as its own function
		//Make calls from the big init() method with the correct arguments for initialisation
	}

	public static class Properties2
	{

		public static boolean DEFAULT_FILEREFERENCE_VALUE = false;
		public static String DEFAULT_TYPE_VALUE = "STRING";

		private final SimpleStringProperty columnName;
		private final SimpleStringProperty fileReference;
		private final SimpleStringProperty type;
		private final Properties transformation;

		public Properties2(String clmName, String fref, String t)
		{
			this.columnName = new SimpleStringProperty(clmName);
			this.fileReference = new SimpleStringProperty(fref);
			this.type = new SimpleStringProperty(t);
			this.transformation = new Properties(this.columnName.get(), "STRING_TO_STRING");
		}

		public Properties2(String clmName, String fref, String t, Properties trans)
		{
			this.columnName = new SimpleStringProperty(clmName);
			this.fileReference = new SimpleStringProperty(fref);
			this.type = new SimpleStringProperty(t);
			this.transformation = trans;
		}
		
		public Properties2(String clmName,  Properties trans)
		{
			this.columnName = new SimpleStringProperty(clmName);
			this.fileReference = new SimpleStringProperty(Boolean.toString(Properties2.DEFAULT_FILEREFERENCE_VALUE));
			this.type = new SimpleStringProperty(Properties2.DEFAULT_TYPE_VALUE);
			this.transformation = trans;
		}
		
		public String getColumnName()
		{
			return columnName.get();
		}

		public void setColumnName(String cName)
		{
			columnName.set(cName);
		}

		public void setFileReference(String fRef)
		{
			fileReference.set(fRef);
		}

		public String getFileReference()
		{
			return fileReference.get();
		}

		public void setType(String t)
		{
			type.set(t);
		}

		public String getType()
		{
			return type.get();
		}

		public Properties getTransformation()
		{
			return this.transformation;
		}

		public void setTransformation(Properties trans)
		{
			this.transformation.setColumnName(trans.getColumnName());
			this.transformation.setTrasnformation(trans.getTransformation());
		}

		@Override
		public String toString()
		{
			return this.columnName + " " + this.fileReference + " " + this.type;
		}
	}

	public void initColumnDataListView()
	{

		if (this.controller != null)
		{
			try
			{
				String queryString = this.getQueryString(this.queryStringTableName);
				populateColumnNames(queryString);
			}
			catch (Exception e)
			{
				System.out.println(e);
			}
		}
		else
		{
			System.out.println("Warning: View3 has no controller set- The view will display with no data.");
		}
		assert this.INITIALISED;
		assert this.controller != null;
	}

	public void populateData(String tableName, List<String> columnsList, List<String> bufferList, ObservableList<String> targetList)
	{
		bufferList.clear();
		try
		{
			Map<String, List<String>> readOut = this.controller.read(tableName, columnsList);
			assert readOut.keySet().containsAll(columnsList);
			Iterator<String> iter = columnsList.iterator(), temp;
			String columnName;
			List<String> list;
			while (iter.hasNext())
			{
				columnName = iter.next();
				list = readOut.get(columnName);
				temp = list.iterator();
				while (temp.hasNext())
				{
					bufferList.add(temp.next());
				}
			}

			targetList.setAll(bufferList);
		}
		catch (Exception ex)
		{
			System.out.println(ex);
		}
	}

	public void populateColumnNames(String queryString)
	{
		//this.controller.read(sql);
		//this.controller.read(sql, List<String> tableName, List<String> schemaList, List<String> columnsList)
		//populateData(this.queryStringTableName, this.queryStringColumnNamesList, this.arbitraryTablesColumnsList, this.queryColumnsList);

		List<String> schemaList = null;

		this.arbitraryTablesColumnsList.clear();

		try
		{
			this.arbitraryTablesNamesList = new LinkedList<>();
			this.arbitraryTablesColumnsTypesList = new LinkedList<>();
			this.arbitraryTablesColumnsList = new LinkedList<>();
			this.readOut = this.controller.read(queryString, this.arbitraryTablesNamesList, schemaList, this.arbitraryTablesColumnsTypesList, this.arbitraryTablesColumnsList);
		}
		catch (Exception e)
		{
			System.out.println(e);
		}

		this.queryColumnsList.setAll(this.arbitraryTablesColumnsList);
	}

	public void populateColumnData(String columnName)
	{
		//carefull- controller might be null
		if (this.controller == null)
		{
			return;
		}

		List<String> schemaList = null;
		String queryString;

		try
		{
			if ((this.arbitraryTablesNamesList == null) || (this.readOut == null))
			{
				this.arbitraryTablesNamesList = new LinkedList<>();
				queryString = this.getQueryString(this.queryStringTableName);
				this.arbitraryTablesColumnsList = null;
				this.readOut = this.controller.read(queryString, this.arbitraryTablesNamesList, schemaList, this.arbitraryTablesColumnsTypesList, this.arbitraryTablesColumnsList);
				assert this.readOut.keySet().contains(columnName);
			}
			assert this.arbitraryTablesNamesList != null; //the above read should at least create a new list of table names, even though the list may be empty but it would exist
			assert this.readOut != null;
			assert this.arbitraryTablesColumnsList.contains(columnName);

			this.columnDataLinkedList = this.readOut.get(columnName);
			this.columnDataList.setAll(this.columnDataLinkedList);

		}
		catch (Exception e)
		{
			System.out.println(e);
		}
	}

	private String getQueryString(String tableName) throws Exception
	{
		String queryString = "";
		Map<String, List<String>> read = this.controller.read(tableName, this.queryStringColumnNamesList);
		Iterator<String> iter = this.queryStringColumnNamesList.iterator();
		List<String> list;
		while (iter.hasNext())
		{
			list = read.get(iter.next());
			queryString = list.get(list.size() - cView3.ZERO_INDEX_OFFSET);
		}

		return queryString; //return the last item in the last query column
	}

	private String getQueryString(Map<String, List<String>> read) throws Exception
	{
		String queryString = "";
		Iterator<String> iter = this.queryStringColumnNamesList.iterator();
		List<String> list;
		String columnName;
		while (iter.hasNext())
		{
			columnName = iter.next();
			if (columnName.equals("querySql"))
			{
				list = read.get(columnName);
				queryString = list.get(list.size() - cView3.ZERO_INDEX_OFFSET);
			}
		}

		return queryString; //return the last item in the last query column
	}

	public void recommendColumnType(String columnName)
	{
		String queryString, fullyQualifiedJavaType, recommendation;
		int columnIndex, indexInListView;
		List<String> schemaList = null;

		try
		{
			if (this.controller != null)
			{
				if (columnName != null)
				{
					if ((this.arbitraryTablesColumnsList == null) || (this.arbitraryTablesColumnsTypesList == null) || (this.readOut == null))
					{
						queryString = this.getQueryString(this.queryStringTableName);
						this.arbitraryTablesNamesList = new LinkedList<>();
						this.arbitraryTablesColumnsList = new LinkedList<>();
						this.arbitraryTablesColumnsTypesList = new LinkedList<>();
						this.readOut = this.controller.read(queryString, this.arbitraryTablesNamesList, schemaList, this.arbitraryTablesColumnsTypesList, this.arbitraryTablesColumnsList);
						assert this.readOut.keySet().contains(columnName);
					}

					assert this.arbitraryTablesNamesList != null; //the above read should at least create a new list of table names, even though the list may be empty but it would exist
					assert this.readOut != null;
					assert this.arbitraryTablesColumnsTypesList != null;

					if (this.arbitraryTablesColumnsList.contains(columnName))
					{
						columnIndex = this.arbitraryTablesColumnsList.indexOf(columnName);
						fullyQualifiedJavaType = this.arbitraryTablesColumnsTypesList.get(columnIndex);
						recommendation = this.javaTypesMap.get(fullyQualifiedJavaType);
						if (this.comboBoxOptionsList.contains(recommendation))
						{
							indexInListView = this.comboBoxOptionsList.indexOf(recommendation);
							assert indexInListView >= 0 : indexInListView;
							this.typesComboBox.getSelectionModel().select(indexInListView);
						}
						else
							System.out.println("Combo-Box has no recommended type.");
					}
					else
					{
						System.out.println("Cannot recommend column type because Column Name " + columnName + " doesnt exist.");
					}
				}
			}
			else
			{
				System.out.println("Warning: View3 has no controller set- Cannot recommend column type.");
			}
		}
		catch (Exception e)
		{
			System.out.println("View3: Could not recommend column type:" + e);
		}
	}

	public void manageTask()
	{
		this.task.cancel();
		//this.thread = new Thread(task);
		try
		{
			if (this.thread != null)
			{
				this.thread.join();
			}
		}
		catch (Exception ex)
		{
			System.out.println(ex);
		}
		this.task = newTask();
	}

	private Task newTask()
	{
		return new Task<Void>()
		{
			@Override
			protected Void call() throws Exception
			{
				while (!isCancelled())
				{
					if ((mapNameTextArea.getText() == null) || (mapNamesList.contains(mapNameTextArea.getText())) || (mapNameTextArea.getText().equals("")))
					//if ((mapNameTextArea.getText().equals("")))
					{
						mapsListView.setDisable(true);
						fileReferenceCheckBox.setDisable(true);
						queryColumnsLabel.setDisable(true);
						columnDataLabel.setDisable(true);
						propertiesLabel.setDisable(true);
						comboBoxLabel.setDisable(true);
						fileReferenceLabel.setDisable(true);
						comboBoxLabel.setDisable(true);
						mapNameLabel.setDisable(true);
						mapNameTextArea.setDisable(true);
						queryColumnsListView.setDisable(true);
						mapsListView.setDisable(true);
						columnDataListView.setDisable(true);
						mapsListView.setDisable(true);
						propertiesTable.setDisable(true);
						mapLabel.setDisable(true);
					}
					else
					{
						mapsListView.setDisable(false);
						fileReferenceCheckBox.setDisable(false);
						queryColumnsLabel.setDisable(false);
						columnDataLabel.setDisable(false);
						propertiesLabel.setDisable(false);
						comboBoxLabel.setDisable(false);
						fileReferenceLabel.setDisable(false);
						comboBoxLabel.setDisable(false);
						mapNameLabel.setDisable(false);
						mapNameTextArea.setDisable(false);
						queryColumnsListView.setDisable(false);
						mapsListView.setDisable(false);
						columnDataListView.setDisable(false);
						mapsListView.setDisable(false);
						propertiesTable.setDisable(false);
						mapLabel.setDisable(false);
					}
					mapNameTextArea.setDisable(false);
					mapNameLabel.setDisable(false); //this widget always active
					Thread.sleep(100); //arguments are milliseconds 
				}
				return null;
			}
		};
	}

	@Override
	public void handle(Event e)
	{
		Node source;
		Window wSource;
		if (e instanceof WindowEvent)
		{
			wSource = (Window) (e.getSource());
			if (wSource == this.scene.getWindow()) //this program's window
				Platform.exit();
			System.out.println("cView3: Request to close window.");
		}
		else
		{
			source = (Node) e.getSource(); //Should always be a Node on the Scene graph
			try
			{
				if (source.getId().equals(this.backButton.getId()))
				{
					manageTask();
					this.runningMap.setProperties(new HashSet<>(this.tableData));
					cView3.runningMap.setName(this.mapNameTextArea.getText());
					if (this.previousView != null)
					{
						this.pane.getChildren().clear();
						this.previousView.start(this.stage);
					}

				}
				else if (source.getId().equals(nextButton.getId()))
				{
					manageTask();
					//Collect all the properties and write them out to DB
					Iterator<Properties2> iter = this.tableData.iterator();
					this.runningMap.setProperties(new HashSet<>(this.tableData));
					cView3.runningMap.setName(this.mapNameTextArea.getText());
					if (this.nextView != null)
					{
						this.nextView.start(this.stage);
					}

				}
				else if (source.getId().equals(this.queryColumnsListView.getId()))
				{
					System.out.println(this.queryColumnsListView.getSelectionModel().getSelectedIndex());
					int selectedColumnNameIndex = this.queryColumnsListView.getSelectionModel().getSelectedIndex();
					if (selectedColumnNameIndex < 0)
					{
						this.columnDataLinkedList.clear();
					}
					else
					{
						boolean columnAlreadyAdded = false;
						String columnName = this.queryColumnsList.get(selectedColumnNameIndex);
						populateColumnData(columnName);
						recommendColumnType(columnName);
						Iterator<Properties2> iter = this.tableData.iterator();
						while (iter.hasNext())
						{
							if (iter.next().getColumnName().equals(columnName))
							{
								columnAlreadyAdded = true;
								break;
							}
						}
						if (columnAlreadyAdded)
						{
							this.addButton.setDisable(true);
							this.removeButton.setDisable(false);
						}
						else
						{
							this.addButton.setDisable(false);
							this.removeButton.setDisable(true);
						}
					}
				}
				else if (source.getId().equals(this.fileReferenceCheckBox.getId()))
				{

				}
				else if (source.getId().equals(this.addButton.getId()))
				{
					String columnName, fileReference, type;
					int selectedColumnNameIndex = this.queryColumnsListView.getSelectionModel().getSelectedIndex();
					columnName = this.queryColumnsList.get(selectedColumnNameIndex);
					fileReference = this.fileReferenceCheckBox.selectedProperty().getValue() ? "TRUE" : "FALSE";
					System.out.println("File Ref:" + fileReference);
					type = (String) this.typesComboBox.getSelectionModel().getSelectedItem();
					this.tableData.add(new Properties2(columnName, fileReference, type));
					this.addButton.setDisable(true);
					if (!this.tableData.isEmpty())//ie the table aint empty, so allow user to go next
						this.nextButton.setDisable(false);
				}
				else if (source.getId().equals(this.removeButton.getId()))
				{
					int selectedColumnNameIndex = this.propertiesTable.getSelectionModel().getSelectedIndex();
					if (selectedColumnNameIndex >= 0)
					{
						Properties2 prop = this.tableData.get(selectedColumnNameIndex);
						this.tableData.remove(prop);

					}
					else
					{
						selectedColumnNameIndex = this.queryColumnsListView.getSelectionModel().getSelectedIndex();
						String columnName = this.queryColumnsList.get(selectedColumnNameIndex);
						Iterator<Properties2> iter = this.tableData.iterator();
						Properties2 prop;
						while (iter.hasNext())
						{
							prop = iter.next();
							if (prop.getColumnName().equals(columnName))
							{
								this.tableData.remove(prop);
								break;
							}
						}
					}
					this.removeButton.setDisable(true);
					if (this.tableData.isEmpty())//ie the table aint empty, so allow user to go next
						this.nextButton.setDisable(true);
				}
				else if (source.getId().equals(this.typesComboBox.getId()))
				{
					//System.out.println();
					int selectedTypeNameIndex = this.typesComboBox.getSelectionModel().getSelectedIndex();
					if (selectedTypeNameIndex >= 0)
					{
						//System.out.println(this.comboBoxOptionsLinkedList.get(selectedTypeNameIndex));
						//this.typesComboBox.getSelectionModel().select(5);
					}

				}
				else if (source.getId().equals(this.propertiesTable.getId()))
				{
					this.removeButton.setDisable(false);//if a row selected, only then allow to remove the item.
					System.out.println("Table selection.");
				}
				else if (source.getId().equals(this.mapsListView.getId()))
				{
					this.loadMapButton.setDisable(false);
					this.deleteMapButton.setDisable(false);
					int index = this.mapsListView.getSelectionModel().getSelectedIndex();
					if (index > 0)
					{
						String mapName = this.mapNamesList.get(index);
						System.out.println("Selected map:" + mapName);
					}
				}
				else if (source.getId().equals(this.loadMapButton.getId()))
				{
					int index = this.mapsListView.getSelectionModel().getSelectedIndex();
					if (index >= 0)
					{
						String mapName = this.mapNamesList.get(index);
						OutputMap outputMap = getMapByName(mapName);
						loadMap(outputMap);

						/*
					if (this.nextView != null)
					{
						this.nextView.loadMap(outputMap);
					}

					if (this.previousView != null)
					{
						this.previousView.loadMap(outputMap);
					}
						 */
						System.out.println("Selected map:" + mapName);
					}
				}
				else if (source.getId().equals(this.deleteMapButton.getId()))
				{
					int index = this.mapsListView.getSelectionModel().getSelectedIndex();
					if (index >= 0)
					{
						String mapName = this.mapNamesList.get(index);
						OutputMap outputMap = getMapByName(mapName);
						//load the map on this view
						IDataTrainView.mapSet.remove(outputMap);
						loadMapNamesView();
					}
				}
				else if (source.getId().equals(this.mapsListView.getId()))
				{
					int index = this.mapsListView.getSelectionModel().getSelectedIndex();
					if (index >= 0)
					{
						this.deleteMapButton.setDisable(false);
						this.loadMapButton.setDisable(false);
					}
					else
					{
						this.deleteMapButton.setDisable(true);
					}
				}
				else
				{
					System.out.println("Unexpected button.");
				}
			}
			catch (Exception except)
			{
				System.out.println("cView3: Exception in handle():" + except);
			}
		}

	}

	public void loadMapNamesView()
	{
		this.mapNamesList.clear();
		Iterator<OutputMap> iter = IDataTrainView.mapSet.iterator();

		while (iter.hasNext())
		{
			this.mapNamesList.add(iter.next().getName());
		}
	}

	@Override
	public void loadMap(OutputMap map)
	{
		this.mapNameTextArea.setText(map.getName());
		System.out.println("cView3: user query:" + map.getUserQuery());
		populateColumnNames(map.getUserQuery());
		loadProperties(map.getProperties());

		this.runningMap.setUserQuery(map.getUserQuery());
		this.runningMap.setProperties(map.getProperties());
		/*
		if (this.previousView != null)
			this.previousView.loadMap(map);
		if (this.nextView != null)
			this.nextView.loadMap(map);
		 */

	}

	public void loadProperties(Set<Properties2> props)
	{
		Iterator<Properties2> iter = props.iterator();

		this.tableData.clear();

		while (iter.hasNext())
		{
			this.tableData.add(iter.next());
		}
	}

	@Override
	public void onViewWrite(String table, Map<String, List<String>> data)
	{
		assert table != null : "cView3: onViewWrite(): controller should never call this method without a table name set.";
		assert data != null : "cView3: onViewWrite(): controller should never call this method without data set.";
		assert this.stage != null : "cView3: onViewWrite(): if cView object used with controller, stage should be set on which cView renders widgets, etc.";

		if (table.equals(this.queryStringTableName))
		{
			String queryString;

			this.start(this.stage);
			try
			{
				List<String> dataColumn = data.get(this.mapsTableMapNameColumnName);
				Integer lastEntryIndex = dataColumn.size() - 1;
				this.mapName = dataColumn.get(lastEntryIndex);
				this.mapNameTextArea.setText(this.mapName);

				queryString = getQueryString(data);
				this.qrStr = queryString;
				populateColumnNames(queryString);
			}
			catch (Exception ex)
			{
				Logger.getLogger(cView3.class.getName()).log(Level.SEVERE, null, ex);
			}

		}
		/*
		if (table.equals(this.mapsTableName))
		{
			List<String> dataColumn = data.get(this.mapsTableMapNameColumnName);
			Integer lastEntryIndex = dataColumn.size() - 1;
			this.mapName = dataColumn.get(lastEntryIndex);
			this.mapNameTextArea.setText(this.mapName);
		}
		 */
		if (table.equals(this.mapsTableName))
		{
			try
			{
				data = ((cMySQLController) this.controller).readValue(this.mapsTableName);
			}
			catch (Exception ex)
			{
				Logger.getLogger(cView3.class.getName()).log(Level.SEVERE, null, ex);
			}
			Set<String> mapNamesSet = new HashSet<>();
			List<String> list = data.get("mapName");
			mapNamesSet.addAll(list);
			this.mapNamesList.setAll(mapNamesSet);
		}
		if (table.equals(this.inputTableName))
		{
			start(this.stage);
			loadMapName(data);
			populateColumnNames(this.qrStr);
			loadPropertiesData(data);
			/*
			System.out.println("***********Here1");
			//here do inner-join with query string table to get the query string entry with the same mapName as the propeties. Properties must all have the same mapName
			//inner-join to get Map<> data with just the entry with query string of this map
			Map<String, List<String>> queryStringEntry = innerJoin(data); //should contain just the entry in tblQueries that has the mapName same as the map name in data map
			System.out.println("***********Here-:" + queryStringEntry);
			String queryString = null;
			try
			{
				queryString = getQueryString(queryStringEntry);
			}
			catch (Exception ex)
			{
				Logger.getLogger(cView3.class.getName()).log(Level.SEVERE, null, ex);
			}
			populateColumnNames(queryString);
			initColumnDataListView();
			System.out.println("***********Here2");
			loadPropertiesData(data);
			 */
		}
		else
		{
			System.out.println("cView3: onViewWrite():Ignoring uninteresting table name '" + table + "'.");
		}
	}

	@Override
	public void onApplicationWrite(String table, Map<String, List<String>> data)
	{

	}

	public void loadMapName(Map<String, List<String>> data)
	{
		Set<String> mapNamesSet = new HashSet<>();
		List<String> list = data.get("mapName");
		mapNamesSet.addAll(list);
		this.mapNamesList.setAll(mapNamesSet);
	}

	//get the map that has the properties in the argument
	public Map<String, List<String>> innerJoin(Map<String, List<String>> data)
	{
		Map<String, List<String>> outMap;

		try
		{
			//Get the mapName of the input data
			return ((cMySQLController) this.controller).innerJoin(this.queryStringTableName, this.inputTableName, "mapName");
		}
		catch (Exception ex)
		{
			Logger.getLogger(cView3.class.getName()).log(Level.SEVERE, null, ex);
		}
		return null;
	}

	public void loadPropertiesData(Map<String, List<String>> data)
	{
		this.tableData.clear();

		Properties2 prop;
		List<Properties2> propData = new LinkedList<>();
		String columnName, fileRef, type, attribute;
		List<String> list;

		Set<String> keys = data.keySet();
		Iterator<String> keyIter = keys.iterator();
		attribute = keyIter.next();
		int size = data.get(attribute).size();
		while (keyIter.hasNext())
		{
			attribute = keyIter.next();
			assert data.get(attribute).size() == size;
		}

		columnName = fileRef = type = "null";
		for (int i = 0; i < size; i++)
		{
			keyIter = keys.iterator();
			while (keyIter.hasNext())
			{
				attribute = keyIter.next();
				list = data.get(attribute);
				assert list != null : "Every key of data must map to a list with at least a single entry in it!";
				switch (attribute)
				{
					case "columnName":
						columnName = list.get(i);
						break;
					case "fileReference":
						fileRef = list.get(i);
						break;
					case "type":
						type = list.get(i);
						break;
					case "mapName":
						this.mapNameTextArea.setText(list.get(i));
						break;
					default:
						System.out.println("Unknown unexpected attribute '" + attribute + "'");
						System.exit(1);
				}
			}
			prop = new Properties2(columnName, fileRef, type);
			propData.add(prop);
		}

		this.tableData.setAll(propData);
	}

	public Pane getPane()
	{
		return this.pane;
	}

	@Override
	public void stop() throws Exception
	{
		this.task.cancel();
		if (this.thread != null)
			this.thread.join();
		System.out.println("****cView3: Stopped thread.");
	}

	public static void main(String[] args)
	{
		System.out.println("Launching...");
		launch(args);
		System.out.println("Launched.");
	}
}
