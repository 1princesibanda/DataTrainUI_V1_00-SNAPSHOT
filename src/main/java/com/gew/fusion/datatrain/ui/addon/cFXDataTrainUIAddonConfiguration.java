/**
 * Copyright (c) 2019 GEW Technologies (GEW). All rights reserved.
 */
package com.gew.fusion.datatrain.ui.addon;

import com.gew.lib.configuration.IConfiguration;
import com.gew.lib.configuration.annotations.AConfiguration;
import com.gew.lib.configuration.annotations.AConfigurationProperty;
import com.gew.lib.ui.fx.securefilechooser.config.cFXSecureFileChooserConfig;

/**
 *
 * @author Philip M. Trenwith
 */
@AConfiguration(name = "data_train_ui_fx_addon_config")
public class cFXDataTrainUIAddonConfiguration implements IConfiguration
{

  private static final long serialVersionUID = 2L;

  @AConfigurationProperty(name = "file_chooser_config", defaultValue = "secure_file_chooser_fx_config", description = "Secure file chooser configuration", unit = "none")
  private cFXSecureFileChooserConfig m_oFileChooserConfig;

  public cFXDataTrainUIAddonConfiguration()
  {
    this.m_oFileChooserConfig = new cFXSecureFileChooserConfig();
  }

  public cFXSecureFileChooserConfig getFileChooserConfig()
  {
    return m_oFileChooserConfig;
  }


  public void setFileChooserConfig(cFXSecureFileChooserConfig oFileChooserConfig)
  {
    this.m_oFileChooserConfig = oFileChooserConfig;
  }
}
