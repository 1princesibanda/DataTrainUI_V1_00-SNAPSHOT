/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gew.fusion.datatrain.ui.addon.view.wizard.Controller.mysqlcontroller;

import com.gew.fusion.datatrain.ui.addon.view.wizard.DataTrain.cDataTrain;
import com.gew.fusion.datatrain.ui.addon.view.wizard.com.gew.datatrain.ui.OutputMap;
import com.gew.fusion.datatrain.ui.addon.view.wizard.com.gew.datatrain.ui.cView2;
import java.io.IOException;
import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.Types;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.InputMismatchException;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;
import java.util.Objects;
import java.util.Scanner;
import java.util.Set;
import javafx.event.Event;
import javafx.event.EventHandler;

/**
 * Accepts connection requests from UI and application and marshalls communication between them Ideally we would just let the user provide a SQL query to write and to read, then for DB by-pass
 * operations we would compare what is being written to what is being read, and if the to be written matches exactly what would be selected, we just make a RS with the to be written and return that as
 * the result of selecting. This will perhaps be provided in another version- it would be time-consuming to parse the SQL and operate the parse trees to do set comparisons on the data of the two
 * trees. It would be nicer to implement the SQL approach described above because SQL is a standardised data description language which an App and UI can quite resonably be expected to know to use to
 * describe the data that they want to write or read- a nicer interface for App and UI that cleanly keeps them decoupled.
 *
 * @author 0400626
 */
public class cMySQLController extends ISQLParser implements IController
{

	private static short ACCEPTABLE_NUMBER_OF_INSTANCES = 1;
	private static int instanceCount;
	private static cMySQLController instance = null;
	public static final int SMALLEST_RS_COLUMN_INDEX = 1;
	private static final int ILLEGAL_COLUMN_AND_VALUES_LIST_SIZE = 0;
	private static final Integer PLACE_HOLDER_TYPE = -1; //A Long because the data is Integer, so this long cannot be confused with a legitimate integer value
	private static final String BIT_ON = "1", BIT_OFF = "0", OPENNING_ROUND_BRACE = "(", CLOSING_ROUND_BRACE = ")", SQL_VALUES_DELIMITER = ",";
	private final String DRIVER = "com.mysql.cj.jdbc.Driver";
	private final String CONNECTER = "jdbc:mysql://";
	private final String HOST_DB_DELIMITER = "/";
	private final String DB_USERNAME_DELIMITER = "?user=";
	private final String USERNAME_PASSWD_DELIMITER = "&password=";
	private final String TABLE_INFORMATION_SQL = "select table_name from INFORMATION_SCHEMA.PARTITIONS WHERE TABLE_SCHEMA = 'testDB';";
	private Connection conn;
	private Statement stmt;
	private ResultSet rs;
	private String host = "localhost"; //can be IP address:port 
	private String databaseName = "testDB";
	private String username = "root";
	private String password = "password";
	private Boolean passThrough;
	private Set<IApplication> applicationSet;
	private Set<IView> viewSet;
	private Iterator<IView> viewIterator;
	private Iterator<IApplication> applicationIterator;

	private cMySQLController() throws IOException
	{
		try
		{
			Class.forName(DRIVER).newInstance();

			this.conn = DriverManager.getConnection(this.DRIVER + this.CONNECTER + this.host + this.HOST_DB_DELIMITER + this.databaseName + this.DB_USERNAME_DELIMITER + this.username + USERNAME_PASSWD_DELIMITER + this.password);
			this.conn.setAutoCommit(false);
			this.stmt = this.conn.createStatement();

		}
		catch (SQLException | ClassNotFoundException | InstantiationException | IllegalAccessException e)
		{
			//throw new Exception("Couldnt create Database Connection");
			//Write to DB an error response instead
		}
		//Write to DB an error response
		this.rs = null;
		this.viewIterator = null;
		this.applicationIterator = null;
		this.applicationSet = new HashSet<>();
		this.viewSet = new HashSet<>();
		this.passThrough = false;
		instanceCount++;
		System.out.println("^^^^Tableset:" + this.tableSet);
		Iterator<Table> iter = this.tableSet.iterator();
		while (iter.hasNext())
		{
			Table t = iter.next();
			System.out.println("table:" + t + " col:" + t.getColumns());
		}
	}

	private cMySQLController(String host, String databaseName, String username, String password, Set<IApplication> applicationSet, Set<IView> viewSet, Boolean passThrough) throws IOException
	{
		this();
		this.host = host;
		this.databaseName = databaseName;
		this.username = username;
		this.password = password;
		this.applicationSet = applicationSet;
		this.viewSet = viewSet;
		this.passThrough = passThrough;
	}

	public static cMySQLController newMySQLController() throws Exception
	{
		if (cMySQLController.instance == null)
		{
			cMySQLController.instance = new cMySQLController();
		}
		assert cMySQLController.instanceCount == cMySQLController.ACCEPTABLE_NUMBER_OF_INSTANCES;
		assert cMySQLController.instance != null;
		return cMySQLController.instance;
	}

	public static cMySQLController newMySQLController(String host, String databaseName, String username, String password, Set<IApplication> app, Set<IView> view, Boolean passThrough) throws Exception
	{
		if (cMySQLController.instance == null)
		{
			cMySQLController.instance = new cMySQLController(host, databaseName, username, password, app, view, passThrough);
		}
		else if (!cMySQLController.instance.equals(new cMySQLController(host, databaseName, username, password, app, view, passThrough)))
		{
			throw new Exception("Only a single instance of class cMySQLController allowed and there is already an existing instance though different from the requested.");
		}
		assert cMySQLController.instanceCount == cMySQLController.ACCEPTABLE_NUMBER_OF_INSTANCES;
		assert cMySQLController.instance != null;
		return cMySQLController.instance;
	}

	//Writes and reads are can be defined non-blocking whereby the controller will spawn new thread to service request and a callback in the caller will be called
	// to provide outcome (success, error)- viewSet must not assume anything about applicationSet, and vice versa.
	//If blocking request(the default), we use the same calling thread to do controller inhouse work, then spawn new thread only when its time to call the other type of applicationSet.
	//We spawn new thread to decouple the two types- the viewSet must not wait for applicationSet/vice versa. The caller waits for exact same dispatched thread to finish and return, or if the
	//the other side makes a r/w request that logically(according to clock partially ordering) more recent than the first call).
	//Thus a r/w request only finishes immediately for the caller, and the controller spawns new thread to go service the request
	//set writeDB if you dont want an actual DB to be created and record all your writes.
	//We need to know the type of the writer. Observe a writer could be both a View and App types if they implements both IApplicaton and IView interfaces.
	//We want to be able to detect one sidedness where an app writes stating the wrong type and throw an exception to prevent app deadlocked.
	//Observe although a class can implement both interface IApplication and IView, there is a logical separation/distinction between these two.
	//We dont want one type to read/write, declaring that it is one type when it is actually of a different type.
	//Such an app could deadlock because it could wait indefinitely for itself to read/write which never happens cause it declared wrong type.
	//@Override
	/*
	public void write(IControllee controlled, String tableName, String sql) throws Exception
	{
		boolean queryExecutionResult = false;
		//assert this.conn.getSchema().equals(databaseName) : "Controller schema " + this.conn.getSchema() + " not same as schema of argument " + databaseName;

		if (controlled instanceof IApplication)
		{
			if (!this.applicationSet.contains((IApplication) controlled))
			{
				throw new Exception("You must call write() IController method with first argument same object as the Application of this IController.");
			}

			try
			{
				if (!this.passThrough)
				{
					System.out.println("stmt:" + stmt + "executing:" + sql);
					queryExecutionResult = !stmt.execute(sql); //returns false for update statements and other statements that shouldnt return a result;
				}
			}
			catch (SQLException e)
			{
				throw new Exception("Could not write:" + e);
			}

			try
			{
				this.viewIterator = this.viewSet.iterator();
				while (viewIterator.hasNext())
				{
					viewIterator.next().onApplicationWrite(tableName);
				}
			}
			catch (Exception e)
			{
				assert queryExecutionResult;
				throw new Exception("Written, but Error in Viewer:" + e);
			}
		}

		if (controlled instanceof IView)
		{
			if (!this.viewSet.contains(controlled))
			{
				System.out.println("argHash:" + controlled.hashCode());
				this.viewIterator = this.viewSet.iterator();
				while (viewIterator.hasNext())
				{
					System.out.println("iter:" + viewIterator.next().hashCode());
				}
				throw new Exception("Controller: Confirmed view: You must call write() IController method with first argument same object as the Application of this IView." + "Size:" + this.viewSet.size());

			}

			try
			{
				if (!this.passThrough)
					queryExecutionResult = !stmt.execute(sql);//returns false for update statements and other statements that shouldnt return a result;
			}
			catch (SQLException e)
			{
				throw new Exception("Could not write:" + e);
			}

			try
			{
				this.applicationIterator = this.applicationSet.iterator();
				while (applicationIterator.hasNext())
				{
					applicationIterator.next().onViewWrite(tableName);
				}
			}
			catch (Exception e)
			{
				throw new Exception("Written, but Error in Viewer:" + e);
			}
		}

		if (!(controlled instanceof IView) && !(controlled instanceof IApplication))
		{
			throw new Exception("Unexpected object in IController cMySQLController write(); first argument must be IControllee.");
		}

		assert queryExecutionResult; //should be true to confirm that no result was returned for a write operation as expected.

	}
	 */
	//Write only, dont do notification ( let notification be handled by notify() )
	private void write(IControllee controlled, String sql) throws Exception
	{
		boolean queryExecutionResult = false;
		//assert this.conn.getSchema().equals(databaseName) : "Controller schema " + this.conn.getSchema() + " not same as schema of argument " + databaseName;

		if (controlled instanceof IApplication)
		{
			if (!this.applicationSet.contains((IApplication) controlled))
			{
				throw new Exception("You must call write() IController method with first argument same object as the Application of this IController.");
			}

			try
			{
				if (!this.passThrough)
				{
					System.out.println("stmt:" + stmt + "executing:" + sql);
					queryExecutionResult = !stmt.execute(sql); //returns false for update statements and other statements that shouldnt return a result;
				}
			}
			catch (SQLException e)
			{
				throw new Exception("Controller: Could not write:" + e);
			}

		}

		if (controlled instanceof IView)
		{
			if (!this.viewSet.contains(controlled))
			{
				System.out.println("argHash:" + controlled.hashCode());
				this.viewIterator = this.viewSet.iterator();
				while (viewIterator.hasNext())
				{
					System.out.println("iter:" + viewIterator.next().hashCode());
				}
				throw new Exception("Controller: Confirmed view: You must call write() IController method with first argument same object as the Application of this IView." + "Size:" + this.viewSet.size());

			}

			try
			{
				if (!this.passThrough)
					queryExecutionResult = !stmt.execute(sql);//returns false for update statements and other statements that shouldnt return a result;
			}
			catch (Exception e)
			{
				throw new Exception("Controller: Could not write:" + e);
			}
		}

		if (!(controlled instanceof IView) && !(controlled instanceof IApplication))
		{
			throw new Exception("Unexpected object in IController cMySQLController write(); first argument must be IControllee.");
		}

		assert queryExecutionResult; //should be true to confirm that no result was returned for a write operation as expected.

	}

	//return of a mapping of column names to their list of values for a given table name tableName and columns columns
	//assumes for every table name in tableNameList there is a corresponding colums list in columnsList with corresponding column values in valuesList
	private Map<String, List<String>> consolidate(List<String> tableNameList, List<List<String>> columnsList, List<List<String>> valuesList, String tableName, List<String> columns)
	{
		int index = tableNameList.indexOf(tableName);
		if (index < 0)
			throw new AssertionError("argument tableNameList must be known to contain the tableName '" + tableName + "' with columns and value at corresponding indexes.", new Exception(tableNameList.toString()));

		List<String> values = valuesList.get(index);
		Map<String, List<String>> outMap = new HashMap<>();
		Iterator<String> iterColumns = columns.iterator(), iterValues = values.iterator();
		List<String> list;
		String columnName;

		while (iterColumns.hasNext())
		{
			assert iterValues.hasNext() : "Should be true because there should be a value in values for each iterColumns.next().";
			columnName = iterColumns.next();
			assert outMap.get(columnName) == null : "The map is initially empty, and as it is being populated here, should not contain any elements, ie outMap.get(columnName) is null";
			list = new LinkedList<>();
			list.add(iterValues.next());
			outMap.put(columnName, list);
		}
		assert outMap.keySet().containsAll(columns) : "Every column should be in set of keys, and simultaneously there is no key that isnt one of the column names";
		assert columns.containsAll(outMap.keySet()) : "Every column should be in set of keys, and simultaneously there is no key that isnt one of the column names";

		//look for other entries that share the same table name tableName and column names with the above and add their values
		for (int i = ++index; i < tableNameList.size(); i++)
		{
			if (tableNameList.get(i).equals(tableName))
			{
				columns = columnsList.get(i);
				if (outMap.keySet().containsAll(columns) && columns.containsAll(outMap.keySet())) //then we can add the values of these columns to the running outMap data
				{
					iterColumns = columns.iterator();
					values = valuesList.get(i);
					iterValues = values.iterator();
					while (iterColumns.hasNext())
					{
						assert iterValues.hasNext() : "Should be true because there should be a value in values for each iterColumns.next().";
						columnName = iterColumns.next();
						list = outMap.get(columnName);
						assert list != null : "should not be null because a value was assigned earlier when tableName was first encountered at index, which is why the for-loop begins at index++";
						list.add(iterValues.next());
					}
				}

			}
		}
		assert outMap.keySet().containsAll(columns) : "Every column should be in set of keys, and simultaneously there is no key that isnt one of the column names";
		assert columns.containsAll(outMap.keySet()) : "Every column should be in set of keys, and simultaneously there is no key that isnt one of the column names";
		assert outMap.values().isEmpty() == values.isEmpty() != false : "Every column in input args must have values, and if so, there is at least one set of values mappable to those columns";
		return outMap;
	}

	private void notify(IControllee controlled, String tableName, Map<String, List<String>> outMap) throws Exception
	{
		if (controlled instanceof IApplication)
		{
			if (!this.applicationSet.contains((IApplication) controlled))
			{
				throw new Exception("You must call write() IController method with first argument same object as the Application of this IController.");
			}

			try
			{
				this.viewIterator = this.viewSet.iterator();
				if (!viewIterator.hasNext())
					System.out.println("Controller: Info: controller has no views no notify");
				while (viewIterator.hasNext())
				{
					System.out.println("Controller: notifying view about app write");
					viewIterator.next().onApplicationWrite(tableName, outMap);
				}
			}
			catch (Exception e)
			{
				throw new Exception("Written, but Error in Viewer:" + e);
			}
		}

		if (controlled instanceof IView)
		{
			if (!this.viewSet.contains((IView) controlled))
			{
				System.out.println("argHash:" + controlled.hashCode());
				this.viewIterator = this.viewSet.iterator();
				while (viewIterator.hasNext())
				{
					System.out.println("iter:" + viewIterator.next().hashCode());
				}
				throw new Exception("Controller: Confirmed view: You must call notify() IController method with first argument same object as the Application of this IView." + "Size:" + this.viewSet.size());

			}

			try
			{
				this.applicationIterator = this.applicationSet.iterator();
				if (!applicationIterator.hasNext())
					System.out.println("Controller: Info: controller has no applications no notify");
				while (applicationIterator.hasNext())
				{
					System.out.println("Controller: notifying app about view write");
					applicationIterator.next().onViewWrite(tableName, outMap);
				}
			}
			catch (Exception e)
			{
				throw new Exception("Written, but Error in Viewer:" + e);
			}
		}

		if (!(controlled instanceof IView) && !(controlled instanceof IApplication))
		{
			throw new Exception("Unexpected object in IController cMySQLController notify(); first argument must be IControllee.");
		}

	}

	@Override
	public void write(IControllee controlled, List<String> tableNameList, List<List<String>> columnsList, List<List<String>> valuesList) throws Exception
	{
		System.out.println("controller: " + tableNameList + columnsList + valuesList);
		//check null arguments and throw
		if (tableNameList == null)
			throw new NullPointerException("write() of controller received null table names list argument tableNameList when non-null expected.");
		if (columnsList == null)
			throw new NullPointerException("write() of controller received null table names list argument columnsList when non-null expected.");
		if ((!((tableNameList.size() == columnsList.size()) && (columnsList.size() == valuesList.size()))) || tableNameList.isEmpty() || columnsList.isEmpty() || valuesList.isEmpty())
			throw new IllegalArgumentException("Number of table names, columns and values must match and correspond, with at least a single entry.");

		String tableName, columnsStr, valuesStr;//, sql = "start transaction read write;\n";
		List<String> columns, values, processedColumn;
		Iterator<String> namesIter, nameSetIter;
		Iterator<List<String>> columnsIter, valuesIter, processedColumnsIter;
		Map<String, List<String>> outMap;
		List<List<String>> processedColumns;
		int index;

		Set<String> tableNamesSet = new HashSet<>(tableNameList);

		namesIter = tableNameList.iterator();
		columnsIter = columnsList.iterator();
		valuesIter = valuesList.iterator();
		nameSetIter = tableNamesSet.iterator();

		try
		{
			if (!this.passThrough)
			{
				this.stmt.addBatch("start transaction read write;");
				while (namesIter.hasNext())
				{
					assert columnsIter.hasNext();
					assert valuesIter.hasNext();

					tableName = namesIter.next();
					columns = columnsIter.next();
					values = valuesIter.next();

					columnsStr = toStringColumns(tableName, columns);
					valuesStr = toStringValues(tableName, columns, values);
					//sql += sql = "insert into " + tableName + " " + columnsStr + " values " + valuesStr + ";\n";
					this.stmt.addBatch("insert into " + tableName + " " + columnsStr + " values " + valuesStr + ";");
				}
				//sql += "commit;";
				this.stmt.addBatch("commit");

				//we have exhausted all elements in all lists
				assert !namesIter.hasNext() : "if one iterator is exhausted, all list iterators should be exhausted since they should all be the same size";
				assert !columnsIter.hasNext() : "if one iterator is exhausted, all list iterators should be exhausted since they should all be the same size";
				assert !valuesIter.hasNext() : "if one iterator is exhausted, all list iterators should be exhausted since they should all be the same size";

				assert nameSetIter.hasNext() : "nameSetIter.hasNext() should be true because it iterates over table names, which should have at least a single table name, and it is the only iterator that hasnt been iterated to end of its collection";
				//notify subscribers
				//if (!this.passThrough) //ie Write to the database too when passThrough disabled
				this.stmt.executeBatch();
				//this.conn.commit(); //only call this when not auto-commit mode. The default when connection is created is auto-commit mode.
			}

			while (nameSetIter.hasNext())
			{
				tableName = nameSetIter.next();
				processedColumns = new LinkedList<>();
				index = tableNameList.indexOf(tableName);
				columns = columnsList.get(index);
				outMap = consolidate(tableNameList, columnsList, valuesList, tableName, columns);
				processedColumns.add(columns);
				writePassThroughTable(tableName, outMap);

				notify(controlled, tableName, outMap);

				//look for other entries that share the same table name and column names and notify
				for (int i = ++index; i < tableNameList.size(); i++)
				{
					if (tableNameList.get(i).equals(tableName))
					{
						columns = columnsList.get(i);
						processedColumnsIter = processedColumns.iterator();
						while (processedColumnsIter.hasNext())
						{
							processedColumn = processedColumnsIter.next();
							if (!(columns.containsAll(processedColumn) && processedColumn.containsAll(columns)))
							{
								outMap = consolidate(tableNameList, columnsList, valuesList, tableName, columns);
								processedColumns.add(columns);
								writePassThroughTable(tableName, outMap);
								notify(controlled, tableName, outMap);
							}
						}

					}
				}
			}
		}
		catch (Exception e)
		{
			System.out.print("Error occurred during controller write. Clearing statement batch...");
			this.stmt.clearBatch();
			System.out.println("Done.");
			throw e;
		}

		if (!this.passThrough)
			this.stmt.clearBatch();
	}

	private void writePassThroughTable(String tableName, List<String> columnsList, List<String> valuesList) throws Exception
	{
		String columnName;
		Column column;
		String value;
		Table table;

		table = getTableByName(tableName);

		Iterator<String> iter = columnsList.iterator();
		Iterator<String> valueListIter = valuesList.iterator();

		while (iter.hasNext())
		{
			assert valueListIter.hasNext() : "There should be a value for each column in argument columsList";
			columnName = iter.next();
			value = valueListIter.next();
			column = table.getColumnByName(columnName);
			column.getData().add(value);
		}
	}

	private void writePassThroughTable(String tableName, Map<String, List<String>> map) throws Exception
	{
		Set<String> columns = map.keySet();
		Iterator<String> iter = columns.iterator();
		Set<Column> autoIncrColumns;
		List<String> columnData;
		int totalRows, size, from, to;

		Table table = getTableByName(tableName);
		Column column;
		String columnName;
		autoIncrColumns = table.getAutoIncrementColumns();
		totalRows = 0;

		while (iter.hasNext())
		{
			columnName = iter.next();
			column = table.getColumnByName(columnName);
			columnData = column.getData();
			columnData.addAll(map.get(columnName));
			size = columnData.size();
			totalRows = size > totalRows ? size : totalRows;
		}

		iter = columns.iterator();
		while (iter.hasNext())
		{
			columnName = iter.next();
			column = table.getColumnByName(columnName);
			columnData = column.getData();

			if (autoIncrColumns.contains(column))
			{
				from = columnData.isEmpty() ? 0 : Integer.valueOf(columnData.get(columnData.size() - 1)); //assuming the string is a int string
				to = totalRows;

				for (int i = from; i < to; i++)
				{
					columnData.add("" + i);
				}
			}
		}

	}

	@Override
	public void write(IControllee controlled, String tableName, List<String> columnsList, List<String> valuesList) throws Exception
	{
		Map<String, List<String>> outMap;
		List<String> tableNameList;
		List<List<String>> columnsListList, valuesListList;

		if (!this.passThrough)
		{
			String columnsStr = toStringColumns(tableName, columnsList);
			String valuesStr = toStringValues(tableName, columnsList, valuesList);
			String sql = "insert into " + tableName + " " + columnsStr + " values " + valuesStr; //need to take out the constants, parse sql from config and sprintf the leaf nodes of the parse tree into this string
			write(controlled, sql);
		}
		else
		{
			if (!super.getTableNames().contains(tableName))
				throw new Exception("cMySQLController:write(): passthrough set to 'true' but attempt to write unknown table name '" + tableName + "'. Please declare table in cMySQLController config.");
			//writePassThroughTable(tableName, columnsList, valuesList);
		}

		columnsListList = new LinkedList<>();
		valuesListList = new LinkedList<>();
		tableNameList = new LinkedList<>();

		columnsListList.add(columnsList);
		valuesListList.add(valuesList);
		tableNameList.add(tableName);

		outMap = consolidate(tableNameList, columnsListList, valuesListList, tableName, columnsList);
		assert outMap != null;
		writePassThroughTable(tableName, outMap);
		notify(controlled, tableName, outMap);
		/*
				Iterator<String> columnsIter, valuesListIter;

		//assert there is a value for each column
		assert columnsList.size() == valuesList.size();
		
		outMap = new HashMap<>();
		columnsIter = columnsList.iterator();
		valuesListIter = valuesList.iterator();
		
		while(columnsIter.hasNext())
		{
			assert valuesListIter.hasNext();
			outMap.put(columnsIter.next(), Arrays.asList( valuesListIter.next() ) );
		}
		notify(controlled, tableName, outMap);
		 */
	}

	private List<Integer> getColumnTypes(String tableName, List<String> columnsList) throws Exception
	{
		Boolean queryExecutionResult = false;
		String sql = "select * from " + tableName + " limit 1;"; //need to take out the constants, parse sql from config and sprintf the leaf nodes of the parse tree into this string
		queryExecutionResult = this.stmt.execute(sql); //true if the first result is a ResultSet object; false if it is an update count or there are no results
		assert queryExecutionResult;
		this.rs = this.stmt.getResultSet();
		ResultSetMetaData rsmd = this.rs.getMetaData();
		int numberOfColumns = rsmd.getColumnCount();

		List<Integer> outputTypesList = new ArrayList<>(columnsList.size());
		for (int i = 0; i < columnsList.size(); i++)
		{
			outputTypesList.add(cMySQLController.PLACE_HOLDER_TYPE);
		}

		assert outputTypesList.size() == columnsList.size() : "outputTypesList.size():" + outputTypesList.size() + ", columnsList.size():" + columnsList.size();

		int counter, columnIndex, typesListIndex;
		Integer tempType;

		ListIterator<String> columnsListIter = columnsList.listIterator();

		while (columnsListIter.hasNext())
		{
			counter = cMySQLController.SMALLEST_RS_COLUMN_INDEX;
			typesListIndex = columnsListIter.nextIndex();
			String columnName = columnsListIter.next();

			while ((counter <= numberOfColumns))
			{
				if (columnName.equals(rsmd.getColumnName(counter)))
				{
					columnIndex = counter;

					assert Math.abs(cMySQLController.SMALLEST_RS_COLUMN_INDEX) == cMySQLController.SMALLEST_RS_COLUMN_INDEX;
					tempType = outputTypesList.get(typesListIndex);
					assert tempType.equals(cMySQLController.PLACE_HOLDER_TYPE); //ie below we overwrite placeholder values only.
					tempType = rsmd.getColumnType(columnIndex);
					break;
				}
				else
					counter++;
			}

		}
		return outputTypesList;
	}

	private String formatString(String str, int typeConstant) throws InputMismatchException, NumberFormatException
	{
		String formattedString = "";
		switch (typeConstant)
		{
			case Types.VARCHAR://String
			case Types.LONGVARCHAR:
			case Types.LONGNVARCHAR:
			case Types.CHAR:
				formattedString = "'" + str + "'";
				break;

			case Types.BOOLEAN:
				if (str.equalsIgnoreCase(Boolean.toString(Boolean.TRUE)) || str.equalsIgnoreCase(Boolean.toString(Boolean.FALSE)))
					formattedString = str;
				else if (str.equalsIgnoreCase(cMySQLController.BIT_ON) || str.equalsIgnoreCase(cMySQLController.BIT_OFF))
					formattedString = str;
				else
					throw new InputMismatchException("Cannot create boolean type with unexpected value '" + str + "'");
				break;

			//Integers
			case Types.INTEGER:
			case Types.BIT:
			case Types.TINYINT:
			case Types.SMALLINT:
				try
				{
					Integer.parseInt(str);
					formattedString = str;
				}
				catch (NumberFormatException e)
				{
					throw e;
				}
				break;

			case Types.BIGINT: //longs
				try
				{
					Long.parseLong(str);
					formattedString = str;
				}
				catch (NumberFormatException e)
				{
					throw e;
				}
				break;

			case Types.FLOAT://floats
				try
				{
					Float.parseFloat(str);
					formattedString = str;
				}
				catch (NumberFormatException e)
				{
					throw e;
				}
				break;

			case Types.DOUBLE://double
				try
				{
					Double.parseDouble(str);
					formattedString = str;
				}
				catch (NumberFormatException e)
				{
					throw e;
				}
				break;

			case Types.NUMERIC://BigDecimal
				try
				{
					new BigDecimal(str);
					formattedString = str;
				}
				catch (NumberFormatException e)
				{
					throw e;
				}
				break;

			case Types.BINARY://byte[]
			case Types.VARBINARY:
			case Types.BLOB:
				//We assume the string is a properly formatted bytes array, ie str is a comma separated value of byte values enclosed in [ ] braces, like the output of Arrays.toString(bytes [] bArr)
				try
				{
					Scanner sc = new Scanner(str).useDelimiter("[\\[,].*[,\\]");
					String token;
					Byte[] byteArr;
					ArrayList<Byte> arrList = new ArrayList<>();
					while (sc.hasNext())
					{
						token = sc.next();
						assert token.length() >= 2; //because a token is between a two brackets, two commas or a combination, the token must comprise of at least these enclosing characters.
						token = token.substring(1, token.length());
						Byte tokenByte = Byte.valueOf(token); //throws NumberFormatException
						arrList.add(tokenByte);
					}
					byteArr = (Byte[]) arrList.toArray();
				}
				catch (NumberFormatException e)
				{
					throw e;
				}
				catch (Exception e)
				{
					System.out.println("cMySQLController: Exception:" + e);
					System.exit(1);
				}
				break;

			case Types.OTHER:
				formattedString = str;
				break;

			default:
				System.out.println("Type unexpected by controller.");
				break;
		}
		return formattedString;
	}

	//values is a string that can be converted to the type that the corrersponding column has.
	public String toStringValues(String tableName, List<String> columnsList, List<String> valuesList) throws Exception
	{
		//if column is a string type in DB, then put the value in quotes. If number, then leave as is. If char, quotes + must be a single character, etc
		if (columnsList == null)
			throw new NullPointerException("toStringValues() of controller received null columns argument when non-null expected.");
		if (valuesList == null)
			throw new NullPointerException("toStringValues() of controller received null column values argument when non-null expected.");
		if ((columnsList.size() != valuesList.size()) || columnsList.isEmpty())
			throw new InputMismatchException("Number of Columns and Values must match and correspond, with at least a single column and its corresponding value.");

		//assert this.conn.getSchema().equals(databaseName) : "Controller schema " + this.conn.getSchema() + " not same as schema of argument " + databaseName;
		List<Integer> typeList = getColumnTypes(tableName, columnsList);

		String formattedOutput = cMySQLController.OPENNING_ROUND_BRACE;
		int nextIndex, typeConstant;

		ListIterator<String> valuesIterator = valuesList.listIterator();
		assert valuesIterator.hasNext() : valuesList.size(); //Because the valuesList has at least a single item in it, as checked by the third if in opening of function.

		nextIndex = valuesIterator.nextIndex();
		typeConstant = typeList.get(nextIndex);
		formattedOutput += formatString(valuesIterator.next(), typeConstant);

		while (valuesIterator.hasNext())
		{
			nextIndex = valuesIterator.nextIndex();
			typeConstant = typeList.get(nextIndex);
			formattedOutput += cMySQLController.SQL_VALUES_DELIMITER;
			formattedOutput += formatString(valuesIterator.next(), typeConstant);
		}
		formattedOutput += cMySQLController.CLOSING_ROUND_BRACE;

		return formattedOutput;
	}

	public String toStringColumns(String tableName, List<String> columnsList) throws Exception
	{
		if (columnsList == null)
			throw new NullPointerException("toStringValues() of controller received null columns argument when non-null expected.");
		if (columnsList.size() <= cMySQLController.ILLEGAL_COLUMN_AND_VALUES_LIST_SIZE)
			throw new InputMismatchException("Number of Columns must be more than " + cMySQLController.ILLEGAL_COLUMN_AND_VALUES_LIST_SIZE);

		getColumnTypes(tableName, columnsList); //assert the columns in columnsList are indeed in the table

		String formattedOutput = cMySQLController.OPENNING_ROUND_BRACE;
		int typeConstant = Types.OTHER;

		ListIterator<String> columnsListIterator = columnsList.listIterator();
		assert columnsListIterator.hasNext() : columnsList.size(); //Because the valuesList has at least a single item in it, as checked by the third if in opening of function.

		formattedOutput += formatString(columnsListIterator.next(), typeConstant);

		while (columnsListIterator.hasNext())
		{
			formattedOutput += cMySQLController.SQL_VALUES_DELIMITER;
			formattedOutput += formatString(columnsListIterator.next(), typeConstant);
		}
		formattedOutput += cMySQLController.CLOSING_ROUND_BRACE;

		return formattedOutput;
	}

	//We take a shortcut here and expose the RS to the View, because we'd have to do SQL parsing
	//Dont return RS, return an a List of column names in the order of the RS, and a set with a List of data for each column
	private ResultSet read(String sql) throws Exception
	{
		boolean queryExecutionResult;
		try
		{
			queryExecutionResult = stmt.execute(sql); //true if the first result is a ResultSet object; false if it is an update count or there are no results
			this.rs = stmt.getResultSet();
			if (queryExecutionResult)
				return this.rs;
			else
				throw new Exception("Controller: Could not retrieve query result for read query \"" + sql + "\"");
		}
		catch (SQLException e)
		{
			throw new Exception("Could not read:" + e);
		}
	}

	//Like read(), except if a list is not empty, the data of that list must be exhaustive, and this function will error if there is data not present in a non-empty list;
	//The first argument is a sql statement and may not be empty.
	public Map<String, List<String>> read(String sql, List<String> tableName, List<String> schemaList, List<String> columnJavaTypes, List<String> columnsList) throws Exception
	{
		if (sql == null)
			throw new InputMismatchException("The first argument to read() must be a SQL statement and may not be null.");
		tableName = tableName == null ? new LinkedList<>() : tableName;
		schemaList = schemaList == null ? new LinkedList<>() : schemaList;
		columnsList = columnsList == null ? new LinkedList<>() : columnsList;
		columnJavaTypes = columnJavaTypes == null ? new LinkedList<>() : columnJavaTypes;

		//declare 
		boolean failedAssert;
		int numberOfColumns;
		ResultSetMetaData rsmd;
		Map<String, List<String>> outputMap;
		List<String> tableName_ = new LinkedList<>(), schemaList_ = new LinkedList<>(), columnsList_ = new LinkedList<>(), columnJavaTypes_ = new LinkedList<>(), tmpList;

		//init
		failedAssert = false;
		this.rs = read(sql);
		outputMap = new HashMap<>();
		rsmd = this.rs.getMetaData();
		numberOfColumns = rsmd.getColumnCount();

		for (int i = cMySQLController.SMALLEST_RS_COLUMN_INDEX; i <= numberOfColumns; i++)
		{
			columnsList_.add(rsmd.getColumnName(i));
			outputMap.put(rsmd.getColumnName(i), new LinkedList<>());
			tableName_.add(rsmd.getTableName(i));
			schemaList_.add(rsmd.getSchemaName(i));
			columnJavaTypes_.add(rsmd.getColumnClassName(i));
		}

		//check asserts
		if (!columnsList.isEmpty())
		{
			failedAssert = columnsList_.equals(columnsList);
			if (failedAssert)
				throw new AssertionError("Column names list provided but missing, superfluous or different order of column names to provided SQL statement.", new Exception(columnsList_.toString()));
		}
		else
			columnsList.addAll(columnsList_);

		if (!tableName.isEmpty())
		{
			failedAssert = tableName_.equals(tableName);
			if (failedAssert)
				throw new AssertionError("Table names list provided but missing, superfluous or different order of table names to provided SQL statement.", new Exception(tableName_.toString()));
		}
		else
			tableName.addAll(tableName_);

		if (!columnJavaTypes.isEmpty())
		{
			failedAssert = columnJavaTypes_.equals(columnJavaTypes);
			if (failedAssert)
				throw new AssertionError("Column Java Types names list provided but missing, superfluous or different order of schema names to provided SQL statement.", new Exception(columnJavaTypes_.toString()));
		}
		else
			columnJavaTypes.addAll(columnJavaTypes_);

		if (!schemaList.isEmpty())
		{
			failedAssert = schemaList_.equals(schemaList);
			if (failedAssert)
				throw new AssertionError("Schema names list provided but missing, superfluous or different order of schema names to provided SQL statement.", new Exception(schemaList_.toString()));
		}
		else
			schemaList.addAll(schemaList_);

		assert numberOfColumns == columnsList.size(); // because columnsList doesnt dictate columns to use, instead it must be exhaustive and in correct order!

		//populate data
		while (rs.next())
		{
			for (int i = cMySQLController.SMALLEST_RS_COLUMN_INDEX; i <= numberOfColumns; i++)
			{
				tmpList = outputMap.get(rsmd.getColumnName(i));
				tmpList.add(rs.getObject(i, String.class));
			}
		}

		assert outputMap.keySet().containsAll(columnsList) : outputMap.keySet().toString() + " vs " + columnsList.toString();
		return outputMap;
	}

	//if columnsList has at least one item, this function returns a Map with the column names in argument columnsList as keys that map to that column's data.
	//Otherwise if the list is null or has no items, the function returns a map with data from all the columns of the table, and the corresponding columns populated into columnsList.
	@Override
	public Map<String, List<String>> read(String tableName, List<String> columnsList) throws Exception
	{
		String columnsStr, sql;
		columnsStr = ((columnsList == null) || columnsList.isEmpty()) ? "*" : toStringColumns(tableName, columnsList);
		sql = "select " + columnsStr + " from " + tableName + ";"; //need to take out the constants, parse sql from config and sprintf the leaf nodes of the parse tree into this string
		this.rs = read(sql);
		ResultSetMetaData rsmd = this.rs.getMetaData();
		int numberOfColumns = rsmd.getColumnCount();
		columnsList = (columnsList == null) ? new LinkedList<>() : columnsList;
		if (columnsList.isEmpty())
			for (int i = cMySQLController.SMALLEST_RS_COLUMN_INDEX; i < numberOfColumns; i++)
			{
				columnsList.add(rsmd.getColumnName(i));
			}

		//assert numberOfColumns == columnsList.size();
		Map<String, List<String>> outputMap = new HashMap<>();
		Iterator<String> iter = columnsList.iterator();
		while (iter.hasNext())
		{
			outputMap.put(iter.next(), new LinkedList<>());
		}

		String columnName;
		List<String> columnDataList;
		while (rs.next())
		{
			for (int i = 0; i < columnsList.size(); i++)
			{
				columnName = columnsList.get(i);
				columnDataList = outputMap.get(columnName);
				columnDataList.add(rs.getObject(columnName, String.class)); //here we assume we can convert all data comming from rs object into a string
			}
		}
		assert outputMap.keySet().containsAll(columnsList);
		return outputMap;
	}

	@Override
	public boolean equals(Object c)
	{
		return (c instanceof cMySQLController) && (this.host.equals(((cMySQLController) c).getHost())) && (this.databaseName.equals(((cMySQLController) c).getDatabaseName())) && (this.username.equals(((cMySQLController) c).getUsername())) && (this.password.equals(((cMySQLController) c).getPassword())) && (this.applicationSet == ((cMySQLController) c).getApplications()) && (this.viewSet == ((cMySQLController) c).getViews()) && (this.passThrough == ((cMySQLController) c).getPassThrough());
	}

	@Override
	public int hashCode()
	{
		int hash = 3;
		hash = 67 * hash + Objects.hashCode(this.host);
		hash = 67 * hash + Objects.hashCode(this.databaseName);
		hash = 67 * hash + Objects.hashCode(this.username);
		hash = 67 * hash + Objects.hashCode(this.password);
		hash = 67 * hash + Objects.hashCode(this.applicationSet);
		hash = 67 * hash + Objects.hashCode(this.viewSet);
		return hash;
	}

	@Override
	public void setMySQLCredentials(String host, String databaseName, String username, String password) throws Exception
	{
		if (this.conn != null)
		{
			this.conn.close();
		}
		if (this.stmt != null)
		{
			this.stmt.close();
		}
		this.host = host;
		this.databaseName = databaseName;
		this.username = username;
		this.password = password;
		Class.forName(this.DRIVER).newInstance();
		this.conn = DriverManager.getConnection(this.CONNECTER + this.host + this.HOST_DB_DELIMITER + this.databaseName + this.DB_USERNAME_DELIMITER + this.username + USERNAME_PASSWD_DELIMITER + this.password);
		this.conn.setAutoCommit(false);
		this.stmt = this.conn.createStatement();
	}

	@Override
	public String getHost()
	{
		return this.host;
	}

	@Override
	public void setHost(String host)
	{
		this.host = host;
	}

	@Override
	public String getDatabaseName()
	{
		return this.databaseName;
	}

	@Override
	public void setDatabaseName(String databaseName)
	{
		this.databaseName = databaseName;
	}

	@Override
	public String getUsername()
	{
		return this.username;
	}

	@Override
	public void setUsername(String username)
	{
		this.username = username;
	}

	@Override
	public String getPassword()
	{
		return this.password;
	}

	@Override
	public void setPassword(String password)
	{
		this.password = password;
	}

	@Override
	public Collection<IApplication> getApplications()
	{
		return this.applicationSet;
	}

	public boolean getPassThrough()
	{
		return this.passThrough;
	}

	public void setPassThrough(boolean passThrough)
	{
		this.passThrough = passThrough;
	}

	@Override
	public void setApplications(Collection<IApplication> applications)
	{
		if (this.applicationSet != null)
		{
			this.applicationSet.addAll(applications);
		}
		else
		{
			this.applicationSet = new HashSet<>(applications);
		}
	}

	@Override
	public Collection<IView> getViews()
	{
		return this.viewSet;
	}

	@Override
	public void setViews(Collection<IView> views)
	{
		if (this.viewSet != null)
		{
			this.viewSet.clear();
			this.viewSet.addAll(views);
		}
		else
		{
			this.viewSet = new HashSet<>(views);
		}
	}

	//returns the columns of the left table only
	public Map<String, List<String>> innerJoin(String leftTableName, String rightTableName, String onColumn) throws Exception
	{
		Table leftTable = this.getTableByName(leftTableName);
		Table rightTable = this.getTableByName(rightTableName);
		Set<Integer> rowIndexSet = new HashSet<>();
		int leftSize, rightSize, maxEntries, index;
		List<String> leftColumnNames, columnData;
		Iterator<String> iter;
		Iterator<Integer> iter2;
		String columnName;
		Map<String, List<String>> outMap;
		List<String> outList;

		Column leftColumn = leftTable.getColumnByName(onColumn);
		Column rightColumn = rightTable.getColumnByName(onColumn);
		System.out.println("********leftDta" + leftColumn.getData() + " rigt:" + rightColumn.getData());
		leftSize = leftColumn.getData().size();
		rightSize = rightColumn.getData().size();

		maxEntries = leftSize < rightSize ? leftSize : rightSize;

		for (int i = 0; i < maxEntries; i++)
		{
			if (leftColumn.getData().get(i).equals(rightColumn.getData().get(i)))
				rowIndexSet.add(i);
			System.out.println("********Adding" + i);
		}

		leftColumnNames = new LinkedList<>(leftTable.getColumnNames());
		iter = leftColumnNames.iterator();
		outMap = new HashMap<>();
		while (iter.hasNext())
		{
			columnName = iter.next();
			columnData = leftTable.getColumnByName(columnName).getData();
			System.out.println("********left tbl column data:" + columnData);
			iter2 = rowIndexSet.iterator();
			outList = new LinkedList<>();
			while (iter2.hasNext())
			{
				index = iter2.next();
				if (!columnData.isEmpty()) //should not happen- handle the auto increments columns to avoid empty data here
					outList.add(columnData.get(index));
			}
			outMap.put(columnName, outList);

		}
		return outMap;
	}

	public Map<String, List<String>> readValue(String tableName) throws Exception
	{
		Table table = getTableByName(tableName);
		List<String> columnsList = new LinkedList<>(table.getColumnNames());
		return readValue(tableName, columnsList);
	}

	//read data from argument table tableName the columns in columnNameList
	public Map<String, List<String>> readValue(String tableName, List<String> columnNamesList) throws Exception
	{
		Map<String, List<String>> outMap;
		String colName;
		Column col;

		Table table = getTableByName(tableName);

		Iterator<String> colNamesIter = columnNamesList.iterator();

		outMap = new HashMap<>();
		while (colNamesIter.hasNext())
		{
			colName = colNamesIter.next();
			col = table.getColumnByName(colName);
			outMap.put(colName, col.getData());
		}

		return outMap;
	}

	public void write(Set<OutputMap> mapSet, String filename)
	{
		this.applicationIterator = this.applicationSet.iterator();

		while (this.applicationIterator.hasNext())
		{
			IApplication app = this.applicationIterator.next();
			if (app instanceof cDataTrain)
			{
				((cDataTrain) app).write(mapSet, filename);
				break;
			}
		}
	}

	public Set<OutputMap> readFile(String filename)
	{
		this.applicationIterator = this.applicationSet.iterator();

		while (this.applicationIterator.hasNext())
		{
			IApplication app = this.applicationIterator.next();
			if (app instanceof cDataTrain)
			{
				return ((cDataTrain) app).readMapsFromFile( filename, this.getDatabaseName(), this.host, this.password, this.username);
			}
		}
		return null;
	}
}
