/**
 * Copyright (c) 2019 GEW Technologies (GEW). All rights reserved.
 */
package com.gew.fusion.datatrain.ui.addon.view.wizard;

import com.gew.fusion.datatrain.ui.addon.cFXDataTrainUIManager;
import com.gew.fusion.datatrain.ui.addon.cFXDataTrainUIManager.EViews;
import com.gew.fusion.datatrain.ui.addon.view.cAbstractView;
import com.gew.fusion.datatrain.ui.addon.view.cStandaloneUI;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import com.gew.fusion.datatrain.ui.addon.view.wizard.cMain;
import com.gew.fusion.datatrain.ui.addon.view.wizard.com.gew.datatrain.ui.IDataTrainView;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Philip M. Trenwith
 */
public class cWizardView extends cAbstractView
{

	private Label m_oTitle;

	public cWizardView(cFXDataTrainUIManager oManager, int iDefaultWidth, int iDefaultHeight)
	{
		super(oManager, iDefaultWidth, iDefaultHeight);
	}

	@Override
	public void reload()
	{
		if (m_oResourceBundle != null)
		{
			m_oTitle.setText(m_oResourceBundle.getString("title_wizard"));
		}
	}

	@Override
	protected BorderPane setupPane(Scene oScene)
	{
		m_oParent.prefHeightProperty().bind(oScene.heightProperty());
		m_oParent.prefWidthProperty().bind(oScene.widthProperty());

		HBox oTitlePane = new HBox(20);
		m_oTitle = new Label("Wizard");
		m_oTitle.setPadding(new Insets(10));
		oTitlePane.getChildren().addAll(m_oTitle);
		oTitlePane.setAlignment(Pos.CENTER);
		//m_oParent.setTop(oTitlePane);
		cStandaloneUI.cmain.setPaneManager(m_oParent);
		m_oParent.setCenter(cStandaloneUI.cmain.getFirstViewPane());

		return m_oParent;
	}

	@Override
	public Node getNode()
	{
		return m_oParent;
	}

	@Override
	public String getViewName()
	{
		return EViews.WIZARD_VIEW.name();
	}

}
