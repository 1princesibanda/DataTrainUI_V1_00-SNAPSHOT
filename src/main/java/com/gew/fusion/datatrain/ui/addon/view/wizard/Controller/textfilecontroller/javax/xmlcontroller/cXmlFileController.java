/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gew.fusion.datatrain.ui.addon.view.wizard.Controller.textfilecontroller.javax.xmlcontroller;

/**
 *
 * @author 0400626
 */
import com.gew.fusion.datatrain.ui.addon.view.wizard.Controller.textfilecontroller.ITextFileController;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

//Config should allow user to enter config about location of a Map in a file and the file to parse
//Config should allow type binding from the possible data types of xpath (i think its only three: boolean, string, number)
//Idea here is for the user to supply an xpath  to get data(in one of the supported xpath return types) and to use xpath expressions to point at xml elements(which should not happen- 
//we mean for the user to be agnostic to the underlying file storage structure details beyond simply that the storage is a file, an xml file, and that it contains data. For instance,
//the user should not be returned document elements to manipulate themselves. All xml file manipulations should be done in this controller.
//use xpath to query xml file contents
public class cXmlFileController implements ITextFileController
{

	public static int SMALLEST_ELEMENT_INDEX = 1;

	XPathFactory xpathFactory;
	XPath xpath;
	DocumentBuilderFactory factory;
	DocumentBuilder builder;
	String filename;

	public cXmlFileController(String filename)
	{
		try
		{
			this.factory = DocumentBuilderFactory.newInstance();
			this.factory.setNamespaceAware(true);
			this.builder = this.factory.newDocumentBuilder();
			this.xpathFactory = XPathFactory.newInstance();
			this.xpath = this.xpathFactory.newXPath();
			this.filename = filename;
		}
		catch (ParserConfigurationException ex)
		{
			Logger.getLogger(cXmlFileController.class.getName()).log(Level.SEVERE, null, ex);
		}
	}

	//Returns a data item at the specified xpath, as a string
	//The xpath must be one that selects text, not elements, always.
	@Override
	public String getDataItem(String path)
	{
		Document doc;
		XPathExpression expr;
		String out;

		out = null;

		try
		{
			doc = this.builder.parse(this.filename);
			expr = this.xpath.compile(path); //throws XPathExpressionException
			out = (String) expr.evaluate(doc, XPathConstants.STRING);
		}
		catch (SAXException | IOException | XPathExpressionException ex)
		{
			Logger.getLogger(cXmlFileController.class.getName()).log(Level.SEVERE, null, ex);
		}

		return out;
	}

	@Override
	public List<String> getData(String path)
	{

		LinkedList<String> outList;

		outList = new LinkedList<>();

		return outList;//need to return a List rather- order can matter
	}

	@Override
	public void insertData(String path, String data)
	{

	}

	@Override
	public void updateData(String path)
	{

	}

	public static void main(String[] args)
	{
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		factory.setNamespaceAware(true);
		DocumentBuilder builder;
		Document doc = null;
		try
		{
			builder = factory.newDocumentBuilder();
			doc = builder.parse("/Users/0400626/Downloads/maps.xml");
			//doc = builder.parse("/cygwin64/home/0400626/workspace/ETL_Wizard/V1_00/DatatrainUI/src/main/resources/exampleMapInput/Employees.xml");
			//doc = builder.parse("/Users/0400626/Downloads/Employees.xml");
			//doc = builder.parse("C:\\Users\\0400626\\Downloads\\Employees.xml");
			//doc = builder.parse("/Users/0400626/Downloads/Employees.xml");

			// Create XPathFactory object
			XPathFactory xpathFactory = XPathFactory.newInstance();

			// Create XPath object
			XPath xpath = xpathFactory.newXPath();

			String name = getEmployeeNameById(doc, xpath, 4);
			System.out.println("Employee Name with ID 4: " + name);

			List<String> names = getEmployeeNameWithAge(doc, xpath, 30);
			System.out.println("Employees with 'age>30' are:" + Arrays.toString(names.toArray()));

			List<String> femaleEmps = getFemaleEmployeesName(doc, xpath);
			System.out.println("Female Employees names are:"
					   + Arrays.toString(femaleEmps.toArray()));

		}
		catch (ParserConfigurationException | SAXException | IOException e)
		{
			e.printStackTrace();
		}

	}

	private static List<String> getFemaleEmployeesName(Document doc, XPath xpath)
	{
		List<String> list = new ArrayList<>();
		try
		{
			//create XPathExpression object
			XPathExpression expr
					= xpath.compile("/Employees/Employee[gender='Female']/name/text()");
			//evaluate expression result on XML document
			NodeList nodes = (NodeList) expr.evaluate(doc, XPathConstants.NODESET);
			for (int i = 0; i < nodes.getLength(); i++)
			{
				list.add(nodes.item(i).getNodeValue());
			}
		}
		catch (XPathExpressionException e)
		{
			e.printStackTrace();
		}
		return list;
	}

	private static List<String> getEmployeeNameWithAge(Document doc, XPath xpath, int age)
	{
		List<String> list = new ArrayList<>();
		try
		{
			XPathExpression expr
					= xpath.compile("/Employees/Employee[age>" + age + "]/name/text()");
			NodeList nodes = (NodeList) expr.evaluate(doc, XPathConstants.NODESET);
			for (int i = 0; i < nodes.getLength(); i++)
			{
				list.add(nodes.item(i).getNodeValue());
			}
		}
		catch (XPathExpressionException e)
		{
			e.printStackTrace();
		}
		return list;
	}

	private static String getEmployeeNameById(Document doc, XPath xpath, int id)
	{
		String name = null;
		try
		{
			XPathExpression expr = xpath.compile("DataTrain/Maps/Map[1]/Input/Sql[1]/text()");
			//XPathExpression expr = xpath.compile("/Employees/Employee[@id='" + id + "']/name/text()");
			name = (String) expr.evaluate(doc, XPathConstants.STRING);
		}
		catch (XPathExpressionException e)
		{
			e.printStackTrace();
		}

		return name;
	}

}
