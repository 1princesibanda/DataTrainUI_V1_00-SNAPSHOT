/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gew.fusion.datatrain.ui.addon.view.wizard.Controller.mysqlcontroller;

import java.util.List;
import java.util.Map;

/**
 *
 * @author 0400626
 */
public interface IApplication extends IControllee
{
    //Will be called when a view writes data to a database and table specified
    //public void onViewWrite(String database, String table);
    
    public void onViewWrite(String table, Map<String,List<String>> data);
    
    //public void onViewRead(String database, String table);
    
}
